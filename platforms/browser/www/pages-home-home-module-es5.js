(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/accept/accept.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/accept/accept.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-content class=\"scroll\">\n  <div class=\"whiteFlap\">\n    <h4 *ngIf=\"status\">\n      <ion-icon name=\"checkmark-circle\"></ion-icon>\n      <strong style=\"color: green\">{{info}}</strong>Verified\n    </h4>\n\n    <h4 *ngIf=\"!status\">\n      <ion-icon color=\"danger\" name=\"close\"></ion-icon>\n      <strong>Rider</strong>Unverified\n    </h4>\n\n    <div class=\"bars\">\n      <h2>\n        <ion-icon name=\"cash\"></ion-icon>\n        {{settings.appcurrency}}{{charge}}\n      </h2>\n    </div>\n\n    <h4><ion-icon name=\"star\"></ion-icon> {{up}}/{{down}}</h4>\n\n    <p><ion-icon name=\"pin\"></ion-icon> <strong> </strong>{{loc}}</p>\n\n    <p><ion-icon name=\"navigate\"></ion-icon> <strong> </strong>{{des}}</p>\n\n    <div style=\"border: 1px solid lightgreen\">\n      <div id=\"dvMap\" style=\"width: auto; height: 120px\"></div>\n    </div>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col>\n          <button\n            [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n            color=\"danger\"\n            expand=\"block\"\n            shape=\"round\"\n            (click)=\"closeModal()\"\n          >\n            IGNORE\n          </button>\n        </ion-col>\n\n        <ion-col>\n          <button\n            [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n            color=\"green\"\n            expand=\"block\"\n            shape=\"round\"\n            (click)=\"acceptModal()\"\n          >\n            ACCEPT\n          </button>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n</ion-content> -->\n\n<ion-content class=\"backgroundColor\">\n  <div class=\"driverFound\">\n    <div class=\"ion-padding\" class=\"request-for-ride2\">\n      <div class=\"headSection\">\n        <div class=\"moveHeader\">\n          <span style=\"font-size: 25px; font-weight: 800\">\n            <strong>Incoming Request!</strong></span\n          >\n        </div>\n      </div>\n\n      <div class=\"resultContainer\">\n        <span style=\"font-size: 30px; font-weight: 800\">\n          <ion-icon\n            name=\"checkmark-circle\"\n            style=\"color: #fbb91d; margin-left: 10%\"\n          ></ion-icon>\n          <strong style=\"color: green\">{{info}} </strong></span\n        >\n        <div style=\"display: inline\">\n          <div class=\"resultContainer2\">\n            <div style=\"display: inline\">\n              <div class=\"img-wrapper\">\n                <ion-icon name=\"pin\"></ion-icon>\n              </div>\n              <div class=\"content-wrap2\">\n                <span\n                  style=\"font-size: 16px; font-weight: 500\"\n                  class=\"bookPrice\"\n                  >{{loc}}\n                </span>\n              </div>\n            </div>\n            <br />\n\n            <div style=\"display: inline\">\n              <div class=\"img-wrapper\">\n                <ion-icon name=\"navigate\"></ion-icon>\n              </div>\n\n              <div class=\"content-wrap2\">\n                <span\n                  style=\"font-size: 16px; font-weight: 500; color: #fbb91d\"\n                  class=\"bookPrice\"\n                  >{{des}}\n                </span>\n              </div>\n            </div>\n          </div>\n\n          <!-- <div class=\"content-wrap\">\n            <h4 class=\"butt\" class=\"ion-text-center\">\n              {{settings.appcurrency}}\n              <span style=\"font-size: 40px; font-weight: 800\">\n                <strong>{{charge}}</strong></span\n              >\n            </h4>\n          </div> -->\n        </div>\n      </div>\n\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-button\n              [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n              color=\"dark\"\n              expand=\"block\"\n              shape=\"round\"\n              (click)=\"acceptModal()\"\n            >\n              ACCEPT\n            </ion-button>\n          </ion-col>\n          <ion-col>\n            <ion-button\n              [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n              color=\"danger\"\n              expand=\"block\"\n              shape=\"round\"\n              (click)=\"closeModal()\"\n            >\n              IGNORE\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/chat/chat.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/chat/chat.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-title>Chat with Rider</ion-title>\n    <ion-button icon-only (click)=\"closeChat()\" slot=\"end\">\n        <ion-icon name=\"close\"></ion-icon>\n      </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding\" class=\"ion-text-center\" class=\"yes-scroll\">\n  <div class=\"followed-items\">\n    <ion-list class=\"ion-text-center\" lines=\"none\">\n      <div class=\"ion-text-center\" *ngFor=\"let event of eventList\">\n        <ion-item text-wrap color=\"primary\" class=\"driver\" *ngIf=\"!event.user\">\n          <p>{{event.driver}}</p>\n        </ion-item>\n        <ion-item text-wrap color=\"primary\" class=\"user\" *ngIf=\"!event.driver\">\n          <p>{{event.user}}</p>\n        </ion-item>\n      </div>\n    </ion-list>\n  </div>\n</ion-content>\n<!-- \n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"6\">\n        <ion-button\n          id=\"container_1\"\n          color=\"primary\"\n          (click)=\"closeChat()\"\n          shape=\"round\"\n        >\n          Close\n        </ion-button>\n      </ion-col>\n      <ion-col size=\"6\">\n        <ion-button\n          id=\"container_1\"\n          color=\"primary\"\n          (click)=\"Send()\"\n          shape=\"round\"\n        >\n          New Message\n        </ion-button>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer> -->\n\n<ion-footer>\n  <ion-grid>\n    <ion-row>\n      <ion-col size=\"10\">\n        <ion-input\n          type=\"text\"\n          placeholder=\"Type a message\"\n          [(ngModel)]=\"data.message\"\n          name=\"message\"\n        ></ion-input>\n      </ion-col>\n      <ion-col size=\"2\" (click)=\"Send()\">\n        <ion-icon name=\"paper-plane\"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"no-scroll\">\n  <div id=\"map-canvas\">\n    <div class=\"topBar\">\n      <ion-menu-toggle (click)=\"toggle()\" autoHide=\"false\">\n        <!-- *ngIf=\"hasNotPicked && hasEnded\" -->\n        <ion-button\n       \n  \n          style=\"\n            --border-radius: 100%;\n            width: 60px;\n            height: 60px;\n            --vertical-align: middle;\n            --padding-start: -5px;\n            --padding-end: -5px;\n          \"\n        >\n          <ion-icon name=\"ios-menu\" slot=\"icon-only\"></ion-icon>\n        </ion-button>\n      </ion-menu-toggle>\n\n      <!-- chooseSubscriptiontype() -->\n\n       <!-- <div id=\"buttonContainer\" *ngIf=\"subscribed == false && has_subscribed == undefined \" >\n        <ion-button\n        \n          size=\"large\"\n          shape=\"round\"\n          color=\"primary\"\n          class=\"button\"\n          (click)=\"chooseSubscriptiontype()\"\n        >\n          <ion-grid style=\"color: #000000\">\n            <ion-row>\n              <ion-col>\n                SUBSCRIBE\n                <ion-title style=\"color: #000000\" id=\"demo\"></ion-title>\n              </ion-col>\n            </ion-row>\n            \n          </ion-grid>\n        </ion-button>\n\n\n\n      </div> -->\n      <!-- *ngIf=\"subscribed == true || has_subscribed != undefined \" -->\n\n      <div id=\"buttonContainer\" >\n        <ion-button\n          *ngIf=\"!this.pop.loggedOff\"\n          size=\"large\"\n          shape=\"round\"\n          color=\"primary\"\n          class=\"button\"\n          (click)=\"goOffline()\"\n        >\n          <ion-grid style=\"color: #228b22\">\n            <ion-row>\n              <ion-col>\n               GO OFFLINE\n                <ion-title style=\"color: #000000;\n    font-size: 12px;\" id=\"demo\"></ion-title>\n              </ion-col>\n            </ion-row>\n            \n          </ion-grid>\n        </ion-button>\n\n        <ion-button\n          class=\"button\"\n          size=\"large\"\n          color=\"primary\"\n          shape=\"round\"\n          *ngIf=\"this.pop.loggedOff && hasEnded && !hasNotFoundMap\"\n          (click)=\"goOnline()\"\n        >\n          <div style=\"color: #f00a0a\">GO ONLINE</div>\n        </ion-button>\n\n        <ion-button *ngIf=\"this.pop.loggedOff && hasNotFoundMap\">\n          <ion-spinner color=\"#ffffff\" name=\"bubbles\"></ion-spinner>\n        </ion-button>\n      </div>\n\n      <div class=\"mid-right\">\n        <ion-fab slot=\"fixed\">\n          <ion-fab-button color=\"primary\"> SOS </ion-fab-button>\n          <ion-fab-list side=\"top\">\n            <ion-fab-button (click)=\"call_phone()\"\n              ><ion-icon name=\"call\"> </ion-icon\n            ></ion-fab-button>\n          </ion-fab-list>\n\n          <ion-fab-list side=\"start\">\n            <ion-fab-button (click)=\"call_phone_other()\"\n              ><ion-icon name=\"body\"> </ion-icon\n            ></ion-fab-button>\n          </ion-fab-list>\n\n          <ion-fab-list side=\"bottom\">\n            <ion-fab-button (click)=\"call_phone_other()\"\n              ><ion-icon name=\"bonfire\"> </ion-icon\n            ></ion-fab-button>\n          </ion-fab-list>\n        </ion-fab>\n      </div>\n\n      <div [hidden]=\"isPiked == true\">\n        <div class=\"mid-right2\">\n          <ion-fab slot=\"fixed\">\n            <ion-fab-button (click)=\"gotoDestination()\">\n              <ion-icon name=\"navigate\"></ion-icon>\n            </ion-fab-button>\n          </ion-fab>\n        </div>\n      </div>\n\n      <div *ngIf=\"isArrived\">\n        <div class=\"mid-right2\">\n          <ion-fab slot=\"fixed\">\n            <ion-fab-button (click)=\"gotoMap()\">\n              <ion-icon name=\"navigate\"></ion-icon>\n            </ion-fab-button>\n          </ion-fab>\n        </div>\n      </div>\n    </div>\n\n    <div id=\"map_layer\">\n      \n   \n      <label *ngIf=\"!hasEnded\">\n        <input type=\"checkbox\" name=\"run\" value=\"click\" />\n        <div id=\"button1\" class=\"button\" style=\"z-index: 999999999999\">\n          <ion-icon\n            name=\"ios-more\"\n            style=\"font-size: 34px; margin-top: 13px\"\n          ></ion-icon>\n\n          <button class=\"main-con\">\n            <div class=\"driverFoundNew\" *ngIf=\"!hasEnded\">\n              <div class=\"ion-padding\" class=\"request-for-ride2\">\n                <div\n                  class=\"headSection\"\n                  *ngIf=\"hasNotPicked && pop.hasPicked == false\"\n                >\n                  <div class=\"moveHeader\">\n                    <span\n                      style=\"\n                        font-size: 25px;\n                        font-weight: 800;\n                        margin-left: 10%;\n                      \"\n                    >\n                      <strong>Pickup Rider</strong></span\n                    >\n                    <br />\n                    <span class=\"centerText\">ETA {{dProvider.time}}</span>\n                  </div>\n                </div>\n\n                <div\n                  class=\"headSection\"\n                  *ngIf=\"!hasNotPicked && isPiked == true\"\n                >\n                  <div class=\"moveHeader\">\n                    <span\n                      style=\"\n                        font-size: 25px;\n                        font-weight: 800;\n                        margin-left: 10%;\n                      \"\n                    >\n                      <strong>Start Your Ride</strong></span\n                    >\n                    <br />\n                    <span class=\"centerText\">ETA {{dProvider.time2}}</span>\n                  </div>\n                </div>\n\n                <div class=\"headSection\" [hidden]=\"!pop.hasPicked\">\n                  <div class=\"moveHeader\">\n                    <span\n                      style=\"\n                        font-size: 25px;\n                        font-weight: 800;\n                        margin-left: 10%;\n                      \"\n                    >\n                      <strong>Enroute to destination</strong></span\n                    >\n                    <br />\n                    <span class=\"centerText\">ETA {{dProvider.time2}}</span>\n                  </div>\n                </div>\n\n                <div class=\"resultContainer\">\n                  <ion-grid>\n                    <ion-row>\n                      <ion-col size=\"3\">\n                        <div class=\"img-wrapper\">\n                          <ion-icon\n                            name=\"contact\"\n                            id=\"drivericonSize\"\n                            (click)=\"OpenInfo()\"\n                          ></ion-icon>\n                        </div>\n                      </ion-col>\n                      <ion-col size=\"5\">\n                        <div class=\"content-wrap\">\n                          <span class=\"bookTitle\">Rider</span>\n                          <br />\n                          <span\n                            style=\"font-size: 30px; font-weight: 800\"\n                            class=\"bookPrice\"\n                            >{{name}}</span\n                          >\n                        </div>\n                      </ion-col>\n\n                      <ion-col size=\"4\">\n                        <div class=\"content-wrap\">\n                          <span class=\"bookTitle\">\n                            <img\n                              src=\"assets/icon/chat.svg\"\n                              class=\"chatIcon\"\n                              (click)=\"SendMessage()\"\n                          /></span>\n                          <br />\n                        </div>\n\n                        <div class=\"content-wrap\">\n                          <span class=\"bookTitle\">\n                            <ion-icon\n                              name=\"call\"\n                              class=\"callIcon\"\n                              (click)=\"call()\"\n                            ></ion-icon>\n                          </span>\n                          <br />\n                        </div>\n                      </ion-col>\n                    </ion-row>\n                  </ion-grid>\n                </div>\n\n                <div class=\"resultContainer2\" [hidden]=\"timeOver && !isArrived\">\n                  <ion-grid>\n                    <ion-row>\n                      <ion-col size=\"2\">\n                        <div class=\"img-wrapper\">\n                          <img src=\"assets/icon/pin.png\" class=\"dott\" />\n                        </div>\n                      </ion-col>\n                      <ion-col size=\"10\">\n                        <div class=\"content-wrap2\">\n                          <span\n                            style=\"font-size: 16px; font-weight: 500\"\n                            class=\"bookPrice\"\n                            >{{destination}}\n                          </span>\n                        </div>\n                      </ion-col>\n                    </ion-row>\n                  </ion-grid>\n                </div>\n\n                <div [hidden]=\"!pop.hasPicked\">\n                  <div id=\"btn-center\">\n                    <div>\n                      <ion-button\n                        class=\"button\"\n                        size=\"large\"\n                        color=\"danger\"\n                        shape=\"round\"\n                        (click)=\"ConfirmDrop()\"\n                      >\n                        <div>END TRIP</div>\n                      </ion-button>\n                    </div>\n                  </div>\n                </div>\n\n                <div [hidden]=\"isPiked == true\">\n                  <div id=\"btn-center\">\n                    <div>\n                      <ion-button\n                        class=\"button\"\n                        size=\"large\"\n                        color=\"primary\"\n                        shape=\"round\"\n                        (click)=\"ConfirmPickup()\"\n                      >\n                        <div>START TRIP</div>\n                      </ion-button>\n                    </div>\n                  </div>\n                </div>\n\n                <div [hidden]=\"isArrived\">\n                  <div id=\"btn-center\">\n                    <div>\n                      <ion-button\n                        class=\"button\"\n                        size=\"large\"\n                        color=\"dark\"\n                        shape=\"round\"\n                        (click)=\"HasArrived()\"\n                      >\n                        <div>ARRIVED</div>\n                      </ion-button>\n                    </div>\n                  </div>\n                </div>\n              </div>\n            </div>\n          </button>\n        </div>\n      </label>\n      <!-- END DRIVER MODE-->\n    </div>\n  </div>\n</ion-content>\n\n<ion-footer no-border id=\"lower_items\" *ngIf=\"hasEnded\">\n  <ion-button expand=\"block\" ion-text-center color=\"primary\" (click)=\"hoseMe()\">\n    <ion-icon *ngIf=\"!hideMe\" name=\"arrow-round-up\" color=\"dark\"></ion-icon>\n    <ion-icon *ngIf=\"hideMe\" name=\"arrow-round-down\" color=\"dark\"></ion-icon>\n  </ion-button>\n  <div [hidden]=\"!isNotDestinyOption\" *ngIf=\"hideMe\">\n    <h3 class=\"centerEarnings\">Earnings</h3>\n    <div class=\"topped\">\n      <ion-badge id=\"myBadge\" color=\"primary\" slot=\"end\"\n        >{{settings.appcurrency}} {{mr}}\n      </ion-badge>\n    </div>\n  </div>\n</ion-footer>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/paymentpage/paymentpage.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/paymentpage/paymentpage.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content class=\"backgroundColor\">\n  <div class=\"driverFound\">\n    <div class=\"ion-padding\" class=\"request-for-ride2\">\n      <div class=\"headSection\">\n        <div class=\"moveHeader\">\n          <span style=\"font-size: 25px; font-weight: 800\">\n            <strong>Trip Fare</strong></span\n          >\n        </div>\n      </div>\n\n      <div class=\"resultContainer\">\n        <span\n          style=\"\n            font-size: 30px;\n            font-weight: 200;\n            text-align: center;\n            margin-left: 20%;\n          \"\n        >\n          Rider will pay</span\n        >\n        <div style=\"display: inline\">\n          <div class=\"content-wrap\">\n            <h4 class=\"butt\" class=\"ion-text-center\">\n              {{settings.appcurrency}}\n              <span\n                style=\"font-size: 60px; font-weight: 800; text-align: center\"\n              >\n                <strong>{{amount}}</strong></span\n              >\n            </h4>\n          </div>\n        </div>\n      </div>\n\n      <ion-grid>\n        <ion-row>\n          <ion-col>\n            <ion-button\n              [ngStyle]=\"{'margin-top': 20 + 'px', 'font-size': 1.1 + 'em'}\"\n              color=\"dark\"\n              expand=\"block\"\n              shape=\"round\"\n              (click)=\"closeModal()\"\n            >\n              ACCEPT\n            </ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n    </div>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/tripinfo/tripinfo.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/tripinfo/tripinfo.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-button (click)=\"closeModal()\" ion-button color=\"primary\" fill=\"clear\">\n    <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n    <span style=\"margin-left: 30px; font-size: 1.4em\">CLOSE</span>\n  </ion-button>\n</ion-header>\n\n<ion-content class=\"scroll\" padding>\n\n\n  <div text-center class=\"whiteFlap\">\n    <ion-title>Trip Information</ion-title>\n  </div>\n  <div text-center class=\"whiteFlap\">\n    <ion-item>\n      <h4 text-center>Pickup {{info.Client_locationName}}</h4></ion-item\n    >\n    <ion-item>Drop off {{info.Client_destinationName}}</ion-item>\n   \n    <ion-item> <h4 text-center>Time Arrived {{arrived}}</h4></ion-item>\n  </div>\n\n  <div padding text-center id=\"buttonContainer2\">\n    <ion-item no-lines detail-none ion-item class=\"guttonz\">\n      <ion-label class=\"stack\" (click)=\"onChange($event)\">\n        <h1 style=\"text-align: center\">CANCEL THIS TRIP</h1>\n      </ion-label>\n     \n    </ion-item>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/accept/accept.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/accept/accept.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inner-scroll {\n  left: 0px;\n  right: 0px;\n  top: calc(var(--offset-top) * -1);\n  bottom: calc(var(--offset-bottom) * -1);\n  padding-left: var(--padding-start);\n  padding-right: var(--padding-end);\n  padding-top: calc(var(--padding-top) + var(--offset-top));\n  padding-bottom: calc( var(--padding-bottom) + var(--keyboard-offset) + var(--offset-bottom) );\n  position: absolute;\n  background: #cf0d0dc7 !important;\n  color: var(--color);\n  box-sizing: border-box;\n  overflow: hidden;\n}\n\n.driverFound {\n  height: 47%;\n  width: 90%;\n  margin-left: 5%;\n  background: white;\n  position: absolute;\n  bottom: 30%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\n.request-for-ride2 {\n  height: 200px;\n}\n\n.headSection {\n  background-color: #000000;\n  color: #fbb91d;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 35%;\n  text-align: center;\n}\n\n.moveHeader {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  align-items: center;\n  margin-top: 20px;\n  margin-left: 0%;\n  margin-right: 0%;\n  width: 100%;\n}\n\n.centerText {\n  text-align: center;\n  font-size: 16px !important;\n}\n\n.resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n  border-bottom: #bababa solid 1px;\n}\n\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n  margin-left: 20px;\n}\n\n#drivericonSize {\n  font-size: 70px !important;\n}\n\n.bookImage,\n.bookTitle,\n.bookPrice {\n  margin-left: 20px;\n}\n\n.resultContainer2 {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n}\n\n.content-wrap2 {\n  display: inline-block;\n}\n\n.dott {\n  width: 20px;\n  height: 20px;\n}\n\n.centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\n.whiteFlap {\n  text-align: center;\n}\n\n.button {\n  height: 70px;\n}\n\n.butt {\n  display: inline-table;\n  height: auto;\n  overflow: hidden;\n}\n\n.price {\n  color: #248cd2;\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n}\n\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n\n#envelope {\n  height: auto;\n  width: 6em;\n}\n\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: white;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n}\n\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: white;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: blue;\n  padding: 5px;\n}\n\n.bars .bars-destinate {\n  height: 100px;\n  width: 100%;\n  background: white;\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  overflow: hidden;\n  border-radius: 50px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: blue;\n  padding: 5px;\n}\n\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#stuff {\n  color: #248cd2;\n  width: 100%;\n  height: 60% !important;\n  border: 1px solid #248cd2;\n}\n\n.no-scroll {\n  background: blue;\n}\n\n.sc-ion-modal-md-h {\n  --width: 86%;\n  --min-width: auto;\n  --max-width: auto;\n  --height: 70% !important;\n  --min-height: auto;\n  --max-height: auto;\n  --overflow: hidden;\n  --border-radius: 0;\n  --border-width: 0;\n  --border-style: none;\n  --border-color: transparent;\n  --background: var(--ion-background-color, #fff);\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy9hY2NlcHQvYWNjZXB0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvYWNjZXB0L2FjY2VwdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDRSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlDQUFBO0VBQ0EsdUNBQUE7RUFDQSxrQ0FBQTtFQUNBLGlDQUFBO0VBQ0EseURBQUE7RUFDQSw2RkFBQTtFQUdBLGtCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUNIRjs7QURVQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUNQRjs7QURVQTtFQUNFLGFBQUE7QUNQRjs7QURVQTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDUEY7O0FEU0E7RUFDRSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0FDTkY7O0FEU0E7RUFDRSxrQkFBQTtFQUNBLDBCQUFBO0FDTkY7O0FEU0E7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGdDQUFBO0FDTkY7O0FEU0E7O0VBRUUscUJBQUE7RUFDQSxpQkFBQTtBQ05GOztBRFNBO0VBQ0UsMEJBQUE7QUNORjs7QURTQTs7O0VBR0UsaUJBQUE7QUNORjs7QURTQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FDTkY7O0FEU0E7RUFDRSxxQkFBQTtBQ05GOztBRFNBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUNORjs7QURTQTtFQUNFLG9CQUFBO0VBQUEsYUFBQTtFQUNBLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0FDTkY7O0FEV0E7RUFDRSxrQkFBQTtBQ1JGOztBRFdBO0VBQ0UsWUFBQTtBQ1JGOztBRFVBO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBRUEsZ0JBQUE7QUNSRjs7QURXQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFFQSxtQkFBQTtBQ1RGOztBRFVFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ1JKOztBRFlBO0VBQ0UsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7QUNWRjs7QURXRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtBQ1RKOztBRFlFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQ1ZKOztBRGNBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtEQUFBO0FDWEY7O0FEZUU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FDYko7O0FEaUJBO0VBQ0UsV0FBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7QUNmRjs7QURnQkU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQ2RKOztBRGtCQTtFQUNFLFlBQUE7RUFDQSxVQUFBO0FDZkY7O0FEa0JBO0VBQ0UsY0FBQTtFQUNBLGFBQUE7QUNmRjs7QURpQkU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0RBQUE7RUFDQSxtREFBQTtFQUNBLCtDQUFBO0VBQ0Esa0RBQUE7QUNmSjs7QURrQkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0RBQUE7RUFDQSxtREFBQTtFQUNBLCtDQUFBO0VBQ0Esa0RBQUE7RUFFQSxlQUFBO0VBRUEsVUFBQTtFQUVBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQko7O0FEcUJJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FDbkJOOztBRHVCRTtFQUNFLGFBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0VBQ0Esa0RBQUE7RUFDQSxtREFBQTtFQUNBLCtDQUFBO0VBQ0Esa0RBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDckJKOztBRHVCSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ3JCTjs7QUR5QkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0RBQUE7RUFDQSxtREFBQTtFQUNBLCtDQUFBO0VBQ0Esa0RBQUE7RUFJQSxVQUFBO0VBRUEsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQzNCSjs7QUQ2Qkk7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUMzQk47O0FEZ0NBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQzdCRjs7QURnQ0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0FDN0JGOztBRGdDQTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtBQzdCRjs7QURnQ0E7RUFDRSxnQkFBQTtBQzdCRjs7QURnQ0E7RUFDRSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsMkJBQUE7RUFDQSwrQ0FBQTtFQUNBLGtCQUFBO0FDN0JGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWNjZXB0L2FjY2VwdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL05FVyBERVNJR04gQ1NTXHJcblxyXG4uaW5uZXItc2Nyb2xsIHtcclxuICBsZWZ0OiAwcHg7XHJcbiAgcmlnaHQ6IDBweDtcclxuICB0b3A6IGNhbGModmFyKC0tb2Zmc2V0LXRvcCkgKiAtMSk7XHJcbiAgYm90dG9tOiBjYWxjKHZhcigtLW9mZnNldC1ib3R0b20pICogLTEpO1xyXG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFkZGluZy1zdGFydCk7XHJcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFkZGluZy1lbmQpO1xyXG4gIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZGRpbmctdG9wKSArIHZhcigtLW9mZnNldC10b3ApKTtcclxuICBwYWRkaW5nLWJvdHRvbTogY2FsYyhcclxuICAgIHZhcigtLXBhZGRpbmctYm90dG9tKSArIHZhcigtLWtleWJvYXJkLW9mZnNldCkgKyB2YXIoLS1vZmZzZXQtYm90dG9tKVxyXG4gICk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJhY2tncm91bmQ6ICNjZjBkMGRjNyAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB2YXIoLS1jb2xvcik7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4vLyAuYmFja2dyb3VuZENvbG9yIHtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhO1xyXG4vLyAgIGhlaWdodDogOTBweDtcclxuLy8gfVxyXG4uZHJpdmVyRm91bmQge1xyXG4gIGhlaWdodDogNDclO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDMwJTtcclxuICB6LWluZGV4OiAxO1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XHJcbn1cclxuXHJcbi5yZXF1ZXN0LWZvci1yaWRlMiB7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxufVxyXG5cclxuLmhlYWRTZWN0aW9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xyXG4gIGNvbG9yOiAjZmJiOTFkO1xyXG4gIG1hcmdpbi10b3A6IC02JTtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDM1JTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1vdmVIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAwJTtcclxuICBtYXJnaW4tcmlnaHQ6IDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uY2VudGVyVGV4dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmVzdWx0Q29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogMDtcclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogI2JhYmFiYSBzb2xpZCAxcHg7XHJcbn1cclxuXHJcbi5jb250ZW50LXdyYXAsXHJcbi5pbWctd3JhcHBlciB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG59XHJcblxyXG4jZHJpdmVyaWNvblNpemUge1xyXG4gIGZvbnQtc2l6ZTogNzBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYm9va0ltYWdlLFxyXG4uYm9va1RpdGxlLFxyXG4uYm9va1ByaWNlIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuLnJlc3VsdENvbnRhaW5lcjIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAwO1xyXG4gIG1hcmdpbi10b3A6IDBweDtcclxuICBtYXJnaW4tYm90dG9tOiAwMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5jb250ZW50LXdyYXAyIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5kb3R0IHtcclxuICB3aWR0aDogMjBweDtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5jZW50ZXJCdG4ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLy8gRU5EIE5FVyBERVNJR04gQ1NTXHJcblxyXG4ud2hpdGVGbGFwIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbi5idXR0b24ge1xyXG4gIGhlaWdodDogNzBweDtcclxufVxyXG4uYnV0dCB7XHJcbiAgZGlzcGxheTogaW5saW5lLXRhYmxlO1xyXG4gIGhlaWdodDogYXV0bztcclxuXHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuLnByaWNlIHtcclxuICBjb2xvcjogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgZm9udC1zaXplOiAxLjY3ZW07XHJcbiAgcGFkZGluZy10b3A6IDE0cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDE0cHg7XHJcblxyXG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2IoMzYsIDE0MCwgMjEwKTtcclxuICB9XHJcbn1cclxuXHJcbi5sb2NhdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGxpZ2h0LCBiYXNlKTtcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgcCB7XHJcbiAgICBmb250LXNpemU6IDEuM2VtO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gIH1cclxuXHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBvcmFuZ2U7XHJcbiAgfVxyXG59XHJcblxyXG4uZGF0ZSB7XHJcbiAgY29sb3I6IG9yYW5nZTtcclxuICBmb250LXNpemU6IDEuNDdlbTtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcclxuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAvLyBib3JkZXI6IDFweCBzb2xpZCBjb2xvcigkY29sb3JzLCBsaWdodCwgYmFzZSk7XHJcbiAgLy9ib3JkZXItYm90dG9tOiAxcHggc29saWQgY29sb3IoJGNvbG9ycywgbGlnaHQsIGJhc2UpO1xyXG4gIC8vIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiByZ2IoMzYsIDE0MCwgMjEwKTtcclxuICB9XHJcbn1cclxuXHJcbi5kZXN0aW5hdGlvbiB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgLy8gYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGNvbG9yKCRjb2xvcnMsIGxpZ2h0LCBiYXNlKTtcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBkYXJrc2xhdGVibHVlO1xyXG4gIH1cclxufVxyXG5cclxuI2VudmVsb3BlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDZlbTtcclxufVxyXG5cclxuLmJhcnMge1xyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcblxyXG4gIC5wb2l0ZXIge1xyXG4gICAgei1pbmRleDogNTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICB9XHJcblxyXG4gIC5iYXJzLWxvY2F0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIC8vICBtYXJnaW46IC0xMiUgMCAwIC01MHB4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDAlO1xyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IGJsdWU7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLWRlc3RpbmF0ZSB7XHJcbiAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAyNTUsIDI1NSk7XHJcbiAgICBtYXJnaW46IDMlIDAgMCAtNTBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxLjJlbTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgZm9udC1zaXplOiAxZW07XHJcbiAgICAgIGxlZnQ6IDIlO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICAgIGNvbG9yOiByZ2IoMzYsIDE0MCwgMjEwKTtcclxuICAgIH1cclxuICB9XHJcblxyXG4gIC5iYXJzLXByaWNlIHtcclxuICAgIGhlaWdodDogNTBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZmZmZjtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIC8vICBtYXJnaW46IC0xMiUgMCAwIC01MHB4O1xyXG4gICAgLy8gbWFyZ2luLWxlZnQ6IDIyJSBhdXRvO1xyXG5cclxuICAgIHotaW5kZXg6IDM7XHJcbiAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IGJsdWU7XHJcbiAgICAgIHBhZGRpbmc6IDVweDtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbiNwb3NpdGlvbiB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI3doZXJldG8ge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XHJcbn1cclxuXHJcbiNzdHVmZiB7XHJcbiAgY29sb3I6IHJnYigzNiwgMTQwLCAyMTApO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogNjAlICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiKDM2LCAxNDAsIDIxMCk7XHJcbn1cclxuXHJcbi5uby1zY3JvbGwge1xyXG4gIGJhY2tncm91bmQ6IGJsdWU7XHJcbn1cclxuXHJcbi5zYy1pb24tbW9kYWwtbWQtaCB7XHJcbiAgLS13aWR0aDogODYlO1xyXG4gIC0tbWluLXdpZHRoOiBhdXRvO1xyXG4gIC0tbWF4LXdpZHRoOiBhdXRvO1xyXG4gIC0taGVpZ2h0OiA3MCUgIWltcG9ydGFudDtcclxuICAtLW1pbi1oZWlnaHQ6IGF1dG87XHJcbiAgLS1tYXgtaGVpZ2h0OiBhdXRvO1xyXG4gIC0tb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAtLWJvcmRlci1yYWRpdXM6IDA7XHJcbiAgLS1ib3JkZXItd2lkdGg6IDA7XHJcbiAgLS1ib3JkZXItc3R5bGU6IG5vbmU7XHJcbiAgLS1ib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xyXG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsICNmZmYpO1xyXG4gIC0tYm94LXNoYWRvdzogbm9uZTtcclxuICAvLyBsZWZ0OiAwO1xyXG4gIC8vIHJpZ2h0OiAwO1xyXG4gIC8vIHRvcDogMDtcclxuICAvLyBib3R0b206IDA7XHJcbiAgLy8gZGlzcGxheTogLW1zLWZsZXhib3g7XHJcbiAgLy8gZGlzcGxheTogZmxleDtcclxuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgLy8gLW1zLWZsZXgtYWxpZ246IGNlbnRlcjtcclxuICAvLyBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gIC8vIC1tcy1mbGV4LXBhY2s6IGNlbnRlcjtcclxuICAvLyBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAvLyBjb250YWluOiBzdHJpY3Q7XHJcbn1cclxuIiwiLmlubmVyLXNjcm9sbCB7XG4gIGxlZnQ6IDBweDtcbiAgcmlnaHQ6IDBweDtcbiAgdG9wOiBjYWxjKHZhcigtLW9mZnNldC10b3ApICogLTEpO1xuICBib3R0b206IGNhbGModmFyKC0tb2Zmc2V0LWJvdHRvbSkgKiAtMSk7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFkZGluZy1zdGFydCk7XG4gIHBhZGRpbmctcmlnaHQ6IHZhcigtLXBhZGRpbmctZW5kKTtcbiAgcGFkZGluZy10b3A6IGNhbGModmFyKC0tcGFkZGluZy10b3ApICsgdmFyKC0tb2Zmc2V0LXRvcCkpO1xuICBwYWRkaW5nLWJvdHRvbTogY2FsYyggdmFyKC0tcGFkZGluZy1ib3R0b20pICsgdmFyKC0ta2V5Ym9hcmQtb2Zmc2V0KSArIHZhcigtLW9mZnNldC1ib3R0b20pICk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogI2NmMGQwZGM3ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB2YXIoLS1jb2xvcik7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5kcml2ZXJGb3VuZCB7XG4gIGhlaWdodDogNDclO1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMzAlO1xuICB6LWluZGV4OiAxO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAzMHB4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbn1cblxuLnJlcXVlc3QtZm9yLXJpZGUyIHtcbiAgaGVpZ2h0OiAyMDBweDtcbn1cblxuLmhlYWRTZWN0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgY29sb3I6ICNmYmI5MWQ7XG4gIG1hcmdpbi10b3A6IC02JTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tb3ZlSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgbWFyZ2luLXJpZ2h0OiAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jZW50ZXJUZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cblxuLnJlc3VsdENvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGxlZnQ6IDA7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItYm90dG9tOiAjYmFiYWJhIHNvbGlkIDFweDtcbn1cblxuLmNvbnRlbnQtd3JhcCxcbi5pbWctd3JhcHBlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XG59XG5cbiNkcml2ZXJpY29uU2l6ZSB7XG4gIGZvbnQtc2l6ZTogNzBweCAhaW1wb3J0YW50O1xufVxuXG4uYm9va0ltYWdlLFxuLmJvb2tUaXRsZSxcbi5ib29rUHJpY2Uge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuLnJlc3VsdENvbnRhaW5lcjIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNvbnRlbnQtd3JhcDIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5kb3R0IHtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbn1cblxuLmNlbnRlckJ0biB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4ud2hpdGVGbGFwIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uYnV0dG9uIHtcbiAgaGVpZ2h0OiA3MHB4O1xufVxuXG4uYnV0dCB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogIzI0OGNkMjtcbiAgZm9udC1zaXplOiAxLjY3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5wcmljZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6ICMyNDhjZDI7XG59XG5cbi5sb2NhdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmxvY2F0aW9uIHAge1xuICBmb250LXNpemU6IDEuM2VtO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ubG9jYXRpb24gaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG5cbi5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZTtcbiAgZm9udC1zaXplOiAxLjQ3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG59XG4uZGF0ZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6ICMyNDhjZDI7XG59XG5cbi5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmRlc3RpbmF0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNmVtO1xufVxuXG4uYmFycyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBwYWRkaW5nOiAxMnB4O1xufVxuLmJhcnMgLnBvaXRlciB7XG4gIHotaW5kZXg6IDU7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBjb2xvcjogYmx1ZTtcbiAgcGFkZGluZzogNXB4O1xufVxuLmJhcnMgLmJhcnMtZGVzdGluYXRlIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW46IDMlIDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1kZXN0aW5hdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogIzI0OGNkMjtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtcHJpY2UgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6IGJsdWU7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuI3Bvc2l0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiN3aGVyZXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiNzdHVmZiB7XG4gIGNvbG9yOiAjMjQ4Y2QyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA2MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzI0OGNkMjtcbn1cblxuLm5vLXNjcm9sbCB7XG4gIGJhY2tncm91bmQ6IGJsdWU7XG59XG5cbi5zYy1pb24tbW9kYWwtbWQtaCB7XG4gIC0td2lkdGg6IDg2JTtcbiAgLS1taW4td2lkdGg6IGF1dG87XG4gIC0tbWF4LXdpZHRoOiBhdXRvO1xuICAtLWhlaWdodDogNzAlICFpbXBvcnRhbnQ7XG4gIC0tbWluLWhlaWdodDogYXV0bztcbiAgLS1tYXgtaGVpZ2h0OiBhdXRvO1xuICAtLW92ZXJmbG93OiBoaWRkZW47XG4gIC0tYm9yZGVyLXJhZGl1czogMDtcbiAgLS1ib3JkZXItd2lkdGg6IDA7XG4gIC0tYm9yZGVyLXN0eWxlOiBub25lO1xuICAtLWJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIC0tYmFja2dyb3VuZDogdmFyKC0taW9uLWJhY2tncm91bmQtY29sb3IsICNmZmYpO1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/accept/accept.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/accept/accept.page.ts ***!
  \*********************************************/
/*! exports provided: AcceptPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AcceptPage", function() { return AcceptPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var AcceptPage = /** @class */ (function () {
    function AcceptPage(navCtrl, lp, settings, pop, modal, activatedRoute) {
        // this.distance = Math.round(this.distance1);
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.modal = modal;
        this.activatedRoute = activatedRoute;
        this.draco = true;
        this.cost = this.activatedRoute.snapshot.paramMap.get("charge");
        // console.log("CHARGES IN ACCEPT PAGE", this.charge);
        // this.cost = this.charge.toFixed(2);
        // console.log("CHARGES IN ACCEPT PAGE", this.cost);
    }
    AcceptPage.prototype.ionViewDidEnter = function () {
        this.pop.showLoader("");
    };
    AcceptPage.prototype.closeModal = function () {
        this.modal.dismiss(2);
    };
    AcceptPage.prototype.acceptModal = function () {
        console.log("ACCEPTED REQUEST");
        this.modal.dismiss(1);
    };
    AcceptPage.prototype.ngOnInit = function () { };
    AcceptPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "loc", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "des", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "charge", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "info", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "up", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "down", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "status", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "time", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], AcceptPage.prototype, "distance1", void 0);
    AcceptPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-accept",
            template: __webpack_require__(/*! raw-loader!./accept.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/accept/accept.page.html"),
            styles: [__webpack_require__(/*! ./accept.page.scss */ "./src/app/pages/accept/accept.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], AcceptPage);
    return AcceptPage;
}());



/***/ }),

/***/ "./src/app/pages/chat/chat.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/chat/chat.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content h1 {\n  font-size: 1.67em;\n  padding-top: 7px;\n  width: auto;\n  padding-bottom: 7px;\n}\nion-content .followed-items .driver {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 30px;\n  background-color: #fbb91d;\n  padding: 10px;\n  float: right;\n  width: 80%;\n  color: black;\n}\nion-content .followed-items .user {\n  margin-top: 5px;\n  margin-bottom: 5px;\n  border-radius: 30px;\n  background-color: #101110;\n  padding: 10px;\n  float: left;\n  width: 80%;\n  color: #fbb91d;\n}\nion-footer #container_1 {\n  margin-top: 2%;\n  height: 70px;\n  width: 148px;\n  color: white;\n  text-align: center;\n  background-position: left;\n  overflow: hidden;\n}\nion-footer #container_1 ion-icon {\n  color: #ffffff;\n}\nion-footer #container_1 h2 {\n  font-size: 1em;\n  height: auto;\n}\nion-footer #container_1 ion-icon {\n  margin: 5px;\n}\nion-footer #container_1 .profile-pic {\n  width: 15%;\n  height: 18%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy9jaGF0L2NoYXQucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9jaGF0L2NoYXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBRUUsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQ0RKO0FES0k7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBQ0pOO0FEYUk7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUVBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtBQ1pOO0FEMENFO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBRUEsWUFBQTtFQUNBLGtCQUFBO0VBRUEseUJBQUE7RUFDQSxnQkFBQTtBQ3pDSjtBRDBDSTtFQUNFLGNBQUE7QUN4Q047QUQwQ0k7RUFDRSxjQUFBO0VBQ0EsWUFBQTtBQ3hDTjtBRDJDSTtFQUNFLFdBQUE7QUN6Q047QUQ0Q0k7RUFDRSxVQUFBO0VBQ0EsV0FBQTtBQzFDTiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NoYXQvY2hhdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XHJcbiAgaDEge1xyXG4gICAgLy8gYmFja2dyb3VuZDogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgICBmb250LXNpemU6IDEuNjdlbTtcclxuICAgIHBhZGRpbmctdG9wOiA3cHg7XHJcbiAgICB3aWR0aDogYXV0bztcclxuICAgIHBhZGRpbmctYm90dG9tOiA3cHg7XHJcbiAgfVxyXG5cclxuICAuZm9sbG93ZWQtaXRlbXMge1xyXG4gICAgLmRyaXZlciB7XHJcbiAgICAgIG1hcmdpbi10b3A6IDVweDtcclxuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xyXG4gICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcblxyXG4gICAgICAvLyBwIHtcclxuICAgICAgLy8gICBmb250LXNpemU6IDE2cHg7XHJcbiAgICAgIC8vICAgZm9udC1zaXplOiA0dnc7XHJcbiAgICAgIC8vICAgY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgICAgLy8gfVxyXG4gICAgfVxyXG5cclxuICAgIC51c2VyIHtcclxuICAgICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICAgIC8vIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICAgIGJhY2tncm91bmQtY29sb3I6ICMxMDExMTA7XHJcbiAgICAgIHBhZGRpbmc6IDEwcHg7XHJcbiAgICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgICB3aWR0aDogODAlO1xyXG4gICAgICBjb2xvcjogI2ZiYjkxZFxyXG5cclxuICAgICAgLy8gcCB7XHJcbiAgICAgIC8vICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAvLyAgIGZvbnQtc2l6ZTogNHZ3O1xyXG4gICAgICAvLyAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgICAgIC8vIH1cclxuICAgIH1cclxuICB9XHJcblxyXG4gIC8vIC50b3BwZWQtaXRlbXMge1xyXG4gIC8vICAgaW9uLWl0ZW0ge1xyXG4gIC8vICAgICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgLy8gICAgIG1hcmdpbi1ib3R0b206IDZweDtcclxuICAvLyAgIH1cclxuICAvLyAgIGgyIHtcclxuICAvLyAgICAgY29sb3I6IG9yYW5nZTtcclxuICAvLyAgICAgZm9udC1zaXplOiAxLjI3ZW07XHJcbiAgLy8gICAgIHBhZGRpbmc6IDJweDtcclxuICAvLyAgIH1cclxuXHJcbiAgLy8gICBpb24tbGFiZWwge1xyXG4gIC8vICAgICBjb2xvcjogcmdiKDAsIDE1MywgMjU1KSAhaW1wb3J0YW50O1xyXG4gIC8vICAgICBmb250LXNpemU6IDFlbTtcclxuICAvLyAgICAgcGFkZGluZzogMnB4O1xyXG4gIC8vICAgfVxyXG4gIC8vIH1cclxufVxyXG5cclxuaW9uLWZvb3RlciB7XHJcbiAgI2NvbnRhaW5lcl8xIHtcclxuICAgIG1hcmdpbi10b3A6IDIlO1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgd2lkdGg6IDE0OHB4O1xyXG5cclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICB9XHJcbiAgICBoMiB7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBtYXJnaW46IDVweDtcclxuICAgIH1cclxuXHJcbiAgICAucHJvZmlsZS1waWMge1xyXG4gICAgICB3aWR0aDogMTUlO1xyXG4gICAgICBoZWlnaHQ6IDE4JTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuIiwiaW9uLWNvbnRlbnQgaDEge1xuICBmb250LXNpemU6IDEuNjdlbTtcbiAgcGFkZGluZy10b3A6IDdweDtcbiAgd2lkdGg6IGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiA3cHg7XG59XG5pb24tY29udGVudCAuZm9sbG93ZWQtaXRlbXMgLmRyaXZlciB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkO1xuICBwYWRkaW5nOiAxMHB4O1xuICBmbG9hdDogcmlnaHQ7XG4gIHdpZHRoOiA4MCU7XG4gIGNvbG9yOiBibGFjaztcbn1cbmlvbi1jb250ZW50IC5mb2xsb3dlZC1pdGVtcyAudXNlciB7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMTAxMTEwO1xuICBwYWRkaW5nOiAxMHB4O1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDgwJTtcbiAgY29sb3I6ICNmYmI5MWQ7XG59XG5cbmlvbi1mb290ZXIgI2NvbnRhaW5lcl8xIHtcbiAgbWFyZ2luLXRvcDogMiU7XG4gIGhlaWdodDogNzBweDtcbiAgd2lkdGg6IDE0OHB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cbmlvbi1mb290ZXIgI2NvbnRhaW5lcl8xIGlvbi1pY29uIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5pb24tZm9vdGVyICNjb250YWluZXJfMSBoMiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBoZWlnaHQ6IGF1dG87XG59XG5pb24tZm9vdGVyICNjb250YWluZXJfMSBpb24taWNvbiB7XG4gIG1hcmdpbjogNXB4O1xufVxuaW9uLWZvb3RlciAjY29udGFpbmVyXzEgLnByb2ZpbGUtcGljIHtcbiAgd2lkdGg6IDE1JTtcbiAgaGVpZ2h0OiAxOCU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/chat/chat.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/chat/chat.page.ts ***!
  \*****************************************/
/*! exports provided: ChatPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChatPage", function() { return ChatPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");






var ChatPage = /** @class */ (function () {
    function ChatPage(navCtrl, plat, alert, modal, eventProvider, ph, lp) {
        this.navCtrl = navCtrl;
        this.plat = plat;
        this.alert = alert;
        this.modal = modal;
        this.eventProvider = eventProvider;
        this.ph = ph;
        this.lp = lp;
        this.data = { type: '', nickname: '', message: '' };
    }
    ChatPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        console.log('inregf');
        this.eventProvider.getChatList(this.id).on('value', function (snapshot) {
            _this.eventList = [];
            console.log('sjiy');
            snapshot.forEach(function (snap) {
                _this.eventList.push({
                    id: snap.key,
                    driver: snap.val().Driver_Message,
                    user: snap.val().Client_Message,
                });
                console.log(_this.eventList);
                return false;
            });
        });
    };
    ChatPage.prototype.closeChat = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ChatPage.prototype.Send = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                console.log('MESSAGE', this.data.message);
                this.eventProvider.SendMessage(this.data.message, this.id);
                return [2 /*return*/];
            });
        });
    };
    ChatPage.prototype.ngOnInit = function () {
    };
    ChatPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"] }
    ]; };
    ChatPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-chat',
            template: __webpack_require__(/*! raw-loader!./chat.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/chat/chat.page.html"),
            styles: [__webpack_require__(/*! ./chat.page.scss */ "./src/app/pages/chat/chat.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"], src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_5__["LanguageService"]])
    ], ChatPage);
    return ChatPage;
}());



/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _accept_accept_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../accept/accept.page */ "./src/app/pages/accept/accept.page.ts");
/* harmony import */ var _chat_chat_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../chat/chat.page */ "./src/app/pages/chat/chat.page.ts");
/* harmony import */ var _tripinfo_tripinfo_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../tripinfo/tripinfo.page */ "./src/app/pages/tripinfo/tripinfo.page.ts");
/* harmony import */ var _paymentpage_paymentpage_page__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../paymentpage/paymentpage.page */ "./src/app/pages/paymentpage/paymentpage.page.ts");











var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: "",
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"],
                    },
                ]),
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"], _accept_accept_page__WEBPACK_IMPORTED_MODULE_7__["AcceptPage"], _chat_chat_page__WEBPACK_IMPORTED_MODULE_8__["ChatPage"], _tripinfo_tripinfo_page__WEBPACK_IMPORTED_MODULE_9__["TripinfoPage"], _paymentpage_paymentpage_page__WEBPACK_IMPORTED_MODULE_10__["PaymentpagePage"]],
            entryComponents: [_accept_accept_page__WEBPACK_IMPORTED_MODULE_7__["AcceptPage"], _chat_chat_page__WEBPACK_IMPORTED_MODULE_8__["ChatPage"], _tripinfo_tripinfo_page__WEBPACK_IMPORTED_MODULE_9__["TripinfoPage"], _paymentpage_paymentpage_page__WEBPACK_IMPORTED_MODULE_10__["PaymentpagePage"]],
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  overflow: visible !important;\n  margin-top: 0% !important;\n}\nion-content .drive {\n  color: #f7f7f7;\n  font-family: \"Franklin Gothic Medium\", \"Arial Narrow\", Arial, sans-serif;\n}\nion-content .price {\n  color: lime;\n}\nion-content .date {\n  color: #ffcc3e;\n}\nion-content .destination {\n  color: #f6fcff;\n}\nion-content .location {\n  color: whitesmoke;\n}\nion-content #map-canvas {\n  width: 100%;\n  height: 100%;\n  position: fixed;\n  z-index: 0;\n}\nlabel > input {\n  display: none;\n}\n#button0 {\n  position: absolute;\n  line-height: 45px;\n  font-weight: bold;\n  text-align: center;\n  display: inline-block;\n  height: 50px;\n  width: 50px;\n  border: 2px dashed black;\n  box-sizing: border-box;\n  left: 0;\n  right: 0;\n  margin: auto;\n  top: 50%;\n  -webkit-transform: translateY(-50%);\n  transform: translateY(-50%);\n  opacity: 0.5;\n}\nlabel > input ~ .button {\n  cursor: pointer;\n  position: absolute;\n  line-height: 45px;\n  font-weight: bold;\n  text-align: center;\n  display: inline-block;\n  height: 50px;\n  width: 100%;\n  box-sizing: border-box;\n  -webkit-transition: all 1s ease-in-out;\n  transition: all 1s ease-in-out;\n  -moz-user-select: none;\n   -ms-user-select: none;\n       user-select: none;\n  -webkit-user-select: none;\n}\nlabel > input ~ #button1 {\n  left: 0;\n  top: 0;\n  top: 45%;\n  background-color: #f2f2f2;\n  color: black;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  text-align: center;\n}\nlabel > input:checked ~ #button1 {\n  left: calc(0% - 0px);\n  top: calc(75% - 0%);\n  -webkit-transition: all 1s ease-in-out;\n  transition: all 1s ease-in-out;\n}\n.main-con {\n  background: transparent;\n}\n.driverFoundNew {\n  left: 0%;\n  top: 147%;\n  height: 50%;\n  width: 95%;\n  margin-left: 1.5%;\n  background: white;\n  position: absolute;\n  bottom: 0%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n@-webkit-keyframes wiki {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n@keyframes wiki {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\nion-footer .bars {\n  width: 100%;\n  position: absolute;\n  background: #f7f7f7;\n  height: auto;\n  z-index: 4;\n  line-height: 50px;\n  text-align: center;\n}\nion-footer .bars .name_text {\n  z-index: 3;\n  font-size: 1.2em;\n  text-align: center;\n  margin-top: 0%;\n}\nion-footer .bars .profile_bar {\n  margin-top: 5%;\n}\nion-footer .bars button {\n  margin-top: 16%;\n  height: auto;\n  width: 95%;\n  border-radius: 12px;\n  text-align: center;\n}\nion-footer #myButtin {\n  border-radius: 12px;\n  margin-top: 3%;\n  padding: 5px;\n  font-size: 1.2em;\n  color: #ffffff;\n}\nion-footer p {\n  padding: 0px;\n  margin: 0px;\n}\nion-footer #aButton {\n  background: #248cd2;\n  color: #f7f7f7;\n  border-radius: 12px;\n  padding: 10px;\n  width: 90%;\n}\nion-footer ion-title {\n  color: #f7f7f7 !important;\n}\n@-webkit-keyframes left_right {\n  0% {\n    top: 75%;\n  }\n  50% {\n    top: 80%;\n  }\n  100% {\n    top: 75%;\n  }\n}\n@keyframes left_right {\n  0% {\n    top: 75%;\n  }\n  50% {\n    top: 80%;\n  }\n  100% {\n    top: 75%;\n  }\n}\n.rate {\n  position: absolute;\n  margin-left: 80%;\n  margin-top: -10%;\n  color: white;\n  top: 14%;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n  z-index: 4;\n  width: 50px;\n  border-radius: 12px;\n}\n.rate ion-icon {\n  font-size: 1.8em;\n  color: white;\n  z-index: 4;\n}\n#title {\n  height: 2% !important;\n}\n#mytite {\n  padding: 2px !important;\n  background: #f7f7f7;\n  border-radius: 30px;\n  margin-left: 19%;\n  margin-top: 2%;\n  width: 45%;\n  height: 14%;\n}\n#mytite ion-icon {\n  width: 10%;\n  margin: 2px;\n}\n#leftshit {\n  margin: 2%;\n  border: 2px solid #fbb91d;\n  border-radius: 17px;\n  padding: 5px;\n  color: white;\n  background: #fbb91d;\n  left: 35% !important;\n  position: relative;\n}\n#star {\n  border: 1px solid rgba(212, 212, 212, 0.93);\n  margin-top: 1%;\n}\n#rides {\n  border-radius: 12px;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n}\n#unstar_1 {\n  margin-top: 3%;\n  background-size: contain;\n  border: 1px solid #fbb91d;\n}\n#unstar_1 h2 {\n  padding: 4px !important;\n  background: #fbb91d;\n  border-radius: 30px;\n}\n#unstar {\n  margin-top: 3%;\n  margin-left: 2.5% !important;\n}\n#unstar_2 {\n  margin-top: 3%;\n  margin-left: 2.5% !important;\n  background-size: cover;\n  border: 1px solid #fbb91d;\n}\n#unstar_2 h2 {\n  padding: 4px !important;\n  background: #fbb91d;\n  border-radius: 30px;\n}\n#top_container {\n  height: 30%;\n  width: 100%;\n  overflow: hidden;\n  background-size: cover;\n  border-top-left-radius: 0px !important;\n  border-top-right-radius: 0px !important;\n  border-bottom-left-radius: 0px !important;\n  border-bottom-right-radius: 0px !important;\n}\n#top_container .stuff {\n  margin-top: 10%;\n}\n#buttonContainer {\n  float: right;\n}\n#buttonContainer button {\n  background: black;\n  color: #ffffff;\n}\n#buttonContainer .button {\n  font-size: 1em;\n  width: 130px;\n  height: 50px;\n  font-weight: bolder;\n}\n#buttonContainer2 button {\n  background: black;\n  color: #ffffff;\n}\n#buttonContainer2 .button {\n  font-size: 1em;\n  width: 130px;\n  height: 50px;\n  font-weight: bolder;\n}\n.gutton {\n  height: auto;\n  width: 100%;\n  font-size: 1.1em;\n  text-align: center;\n}\n#container {\n  margin-top: 2%;\n  height: auto;\n  width: 100%;\n  padding: 5px;\n  color: white;\n  text-align: center;\n}\n#container h2 {\n  font-size: 1em;\n  height: auto;\n}\n#container ion-icon {\n  margin: 3px;\n}\n#container_1 {\n  padding: 10px;\n  width: 50%;\n  color: white;\n  margin: 1px;\n  border-radius: 20px;\n  font-size: 1.3em;\n  font-weight: 600;\n  text-align: center;\n  border: 0.5px solid #f8f8f8;\n}\n#container_1 ion-icon {\n  color: #ffffff;\n}\n#container_1 h2 {\n  font-size: 1em;\n}\n#container_1 .profile-pic {\n  width: 15%;\n  height: 18%;\n}\nion-toggle {\n  float: right;\n  width: 100%;\n  border: 2px solid #f7f7f7;\n  border-radius: 30px;\n  background: #ffffff;\n  padding: auto;\n  margin-top: 4% !important;\n  margin: 3%;\n}\n#header {\n  height: auto;\n  width: 100%;\n  padding: 5px;\n  border-radius: 0px;\n  color: black;\n  text-align: center;\n  background: #f7f7f7;\n}\n#slopw {\n  width: 100%;\n  margin-left: -1%;\n  overflow: auto !important;\n  padding: 10px;\n}\n.mid-right {\n  padding-left: 80%;\n  margin-top: 20%;\n  z-index: 3;\n}\n.mid-right ion-icon {\n  color: #0a0a0a !important;\n}\n.topBar {\n  height: auto;\n  width: 100%;\n  margin-top: 5%;\n  z-index: 3;\n  padding: 8px;\n}\n.topBar button {\n  background: black;\n  border-radius: 12px;\n  color: #ffffff;\n  margin: 6px;\n  padding: 15px;\n}\n.topBar ion-icon {\n  font-size: 2em;\n  color: #000000;\n  padding: 15px;\n}\n.topBar .button-right {\n  float: left;\n}\n#myBadge {\n  height: auto;\n  font-size: 1.6em;\n  border-radius: 12px;\n}\n#map_layer {\n  z-index: 13;\n  position: unset !important;\n  -webkit-animation-name: wiki;\n          animation-name: wiki;\n  -webkit-animation-duration: 5s;\n          animation-duration: 5s;\n  padding: 5px;\n  overflow: auto !important;\n}\n#map_layer .button {\n  margin-top: 0% !important;\n  text-align: center;\n}\n#map_layer .button ion-icon {\n  color: #000000;\n}\n#map_layer .guts {\n  border-radius: 12px;\n  background: white;\n}\n#map_layer .button_1 {\n  background: #ffffff;\n  margin-top: 2px;\n  height: auto;\n  width: 100%;\n  border-radius: 12px;\n  text-align: center;\n}\n#map_layer .button_1 .profile-pic {\n  height: 70%;\n  width: 43%;\n  border: 1.6px solid white;\n}\n#map_layer .button_2 {\n  background: #9d08e2;\n  margin-top: 2px;\n  height: auto;\n  width: 100%;\n  border-radius: 12px;\n  text-align: center;\n  background-position: left;\n}\nh1 {\n  color: #ffffff;\n  font-size: 1.4em;\n}\n.stack {\n  text-align: center;\n  font-size: 0.5em;\n  color: white;\n}\n.guttonz {\n  background: red;\n  border-radius: 15px;\n  height: auto;\n  width: 50%;\n  margin-left: 4%;\n  color: white;\n}\n.mid-right {\n  padding-left: 80%;\n  margin-top: 20%;\n  z-index: 3;\n}\n.mid-right2 {\n  padding-left: 80%;\n  margin-top: 40%;\n  z-index: 3;\n}\n.mid-right2 ion-icon {\n  color: #0a0a0a !important;\n}\nion-footer {\n  background: #00000015;\n}\nion-footer #lower_items {\n  height: auto;\n  width: 100%;\n  color: white !important;\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n  z-index: 15;\n}\nion-footer .button {\n  width: 100%;\n}\nion-footer .buttons {\n  margin-top: 10% !important;\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n  text-align: center;\n  margin-left: 60%;\n  padding: auto !important;\n  background: #fbb91d;\n  z-index: 300 !important;\n  border: 0.7px solid #fbb91d;\n  color: white;\n  font-size: 17px;\n}\nion-footer #rides {\n  border-radius: 30px;\n  margin: 1px;\n  border: 1px solid rgba(212, 212, 212, 0.93);\n}\nion-footer #rides ion-title {\n  color: white;\n}\nion-footer .back_col {\n  background: #1e181f;\n  color: white;\n}\n.driverFound {\n  height: 50%;\n  width: 95%;\n  margin-left: 1.5%;\n  background: white;\n  position: absolute;\n  bottom: 2%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n.headSection {\n  background-color: #000000;\n  color: #fbb91d;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 88px;\n  text-align: center;\n}\n.moveHeader {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  align-items: center;\n  margin-top: 20px;\n  margin-left: 0px;\n  margin-right: 0px;\n}\n.request-for-ride2 {\n  height: 200px;\n  background: white;\n}\n.centerText {\n  text-align: center;\n  font-size: 16px !important;\n}\n.resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding-right: 5%;\n  padding-left: 5%;\n  border-bottom: #bababa solid 1px;\n  background-color: white;\n}\n.resultContainer2 {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n  background-color: white;\n}\n.bookImage,\n.bookTitle,\n.bookPrice {\n  margin-left: 7px;\n}\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n  margin-left: 0px;\n}\n.content-wrap2 {\n  display: inline-block;\n}\n#drivericonSize {\n  font-size: 90px !important;\n}\n.centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n#btn-center {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  text-align: center;\n  background: white;\n}\n.chatIcon {\n  width: 40px;\n  height: 40px;\n  position: relative;\n}\n.callIcon {\n  width: 40px;\n  height: 40px;\n  position: relative;\n}\n.dott {\n  width: 20px;\n  height: 20px;\n}\n.buttons123 {\n  margin-top: 10% !important;\n  border-top-left-radius: 5px;\n  border-top-right-radius: 5px;\n  text-align: center;\n  margin-left: 60%;\n  padding: auto !important;\n  background: #fbb91d;\n  z-index: 300 !important;\n  color: #050505;\n  font-size: 17px;\n}\n.button-native {\n  box-shadow: none !important;\n}\n.centerEarnings {\n  text-align: center;\n  margin: 10px;\n  font-weight: 600;\n  color: #fbb91d;\n}\n.topped {\n  text-align: center;\n}\n/* your custom CSS \\*/\n@keyframes pulsate {\n  from {\n    -moz-transform: scale(0.25);\n    opacity: 1;\n  }\n  95% {\n    -moz-transform: scale(1.3);\n    opacity: 0;\n  }\n  to {\n    -moz-transform: scale(0.3);\n    opacity: 0;\n  }\n}\n@-webkit-keyframes pulsate {\n  from {\n    -webkit-transform: scale(0.25);\n    opacity: 1;\n  }\n  95% {\n    -webkit-transform: scale(1.3);\n    opacity: 0;\n  }\n  to {\n    -webkit-transform: scale(0.3);\n    opacity: 0;\n  }\n}\n/* get the container that's just outside the marker image, \n\t\twhich just happens to have our Marker title in it */\n#map-canvas div[title=pinIcon] {\n  -moz-animation: pulsate 1.5s ease-in-out infinite;\n  -webkit-animation: pulsate 1.5s ease-in-out infinite;\n  border: 1pt solid #fff;\n  /* make a circle */\n  border-radius: 51px;\n  /* multiply the shadows, inside and outside the circle */\n  box-shadow: inset 0 0 5px #fbb91d, inset 0 0 5px #fbb91d, inset 0 0 5px #fbb91d, 0 0 5px #fbb91d, 0 0 5px #fbb91d, 0 0 5px #fbb91d;\n  /* set the ring's new dimension and re-center it */\n  height: 51px !important;\n  margin: -18px 0 0 -18px;\n  width: 51px !important;\n}\n/* hide the superfluous marker image since it would expand and shrink with its containing element */\n/*\t#map-canvas div[style*=\"987654\"][title] img {*/\n#map-canvas div[title=pinIcon] img {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9ob21lL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNEJBQUE7RUFDQSx5QkFBQTtBQ0NGO0FES0U7RUFDRSxjQUFBO0VBQ0Esd0VBQUE7QUNISjtBRE1FO0VBQ0UsV0FBQTtBQ0pKO0FET0U7RUFDRSxjQUFBO0FDTEo7QURRRTtFQUNFLGNBQUE7QUNOSjtBRFNFO0VBQ0UsaUJBQUE7QUNQSjtBRFVFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtBQ1JKO0FEaUJBO0VBQ0UsYUFBQTtBQ2RGO0FEa0JBO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0Esd0JBQUE7RUFDQSxzQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxtQ0FBQTtFQUVBLDJCQUFBO0VBQ0EsWUFBQTtBQ2ZGO0FEa0JBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUVBLHNCQUFBO0VBQ0Esc0NBQUE7RUFDQSw4QkFBQTtFQUNBLHNCQUFBO0dBQUEscUJBQUE7T0FBQSxpQkFBQTtFQUNBLHlCQUFBO0FDaEJGO0FEbUJBO0VBR0UsT0FBQTtFQUNFLE1BQUE7RUFDQSxRQUFBO0VBRUEseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQkFBQTtFQUVBLGtCQUFBO0FDcEJKO0FEd0JBO0VBQ0Usb0JBQUE7RUFDQSxtQkFBQTtFQUVBLHNDQUFBO0VBQ0EsOEJBQUE7QUN0QkY7QUQwQkE7RUFDRSx1QkFBQTtBQ3ZCRjtBRDBCRTtFQUNBLFFBQUE7RUFFSSxTQUFBO0VBQ0YsV0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EsVUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7QUN4Qko7QUQ2QkU7RUFDRTtJQUNFLFVBQUE7RUMxQko7RUQ2QkU7SUFDRSxVQUFBO0VDM0JKO0FBQ0Y7QURvQkU7RUFDRTtJQUNFLFVBQUE7RUMxQko7RUQ2QkU7SUFDRSxVQUFBO0VDM0JKO0FBQ0Y7QUQ4QkU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQzVCSjtBRDhCSTtFQUNFLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQzVCTjtBRCtCSTtFQUNFLGNBQUE7QUM3Qk47QURnQ0k7RUFDRSxlQUFBO0VBQ0EsWUFBQTtFQUdBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FDaENOO0FEb0NFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBR0EsY0FBQTtBQ3BDSjtBRHVDRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FDckNKO0FEd0NFO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtBQ3RDSjtBRDBDRTtFQUNFLHlCQUFBO0FDeENKO0FENENBO0VBQ0U7SUFDRSxRQUFBO0VDekNGO0VENENBO0lBQ0UsUUFBQTtFQzFDRjtFRDZDQTtJQUNFLFFBQUE7RUMzQ0Y7QUFDRjtBRGdDQTtFQUNFO0lBQ0UsUUFBQTtFQ3pDRjtFRDRDQTtJQUNFLFFBQUE7RUMxQ0Y7RUQ2Q0E7SUFDRSxRQUFBO0VDM0NGO0FBQ0Y7QUQ4Q0E7RUFDRSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLDJDQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQzVDRjtBRDZDRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUMzQ0o7QUQrQ0E7RUFDRSxxQkFBQTtBQzVDRjtBRGlEQTtFQUNFLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FDOUNGO0FEK0NFO0VBQ0UsVUFBQTtFQUNBLFdBQUE7QUM3Q0o7QURtREE7RUFDRSxVQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0FDaERGO0FEbURBO0VBRUUsMkNBQUE7RUFDQSxjQUFBO0FDakRGO0FEcURBO0VBQ0UsbUJBQUE7RUFDQSwyQ0FBQTtBQ2xERjtBRHFEQTtFQUNFLGNBQUE7RUFHQSx3QkFBQTtFQUNBLHlCQUFBO0FDcERGO0FEc0RFO0VBQ0UsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FDcERKO0FEd0RBO0VBQ0UsY0FBQTtFQUNBLDRCQUFBO0FDckRGO0FEMERBO0VBQ0UsY0FBQTtFQUNBLDRCQUFBO0VBR0Esc0JBQUE7RUFDQSx5QkFBQTtBQ3pERjtBRDJERTtFQUNFLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQ3pESjtBRDZEQTtFQUVFLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUVBLHNDQUFBO0VBQ0EsdUNBQUE7RUFDQSx5Q0FBQTtFQUNBLDBDQUFBO0FDNURGO0FEOERFO0VBQ0UsZUFBQTtBQzVESjtBRGlFQTtFQUVFLFlBQUE7QUMvREY7QURnRUU7RUFFRSxpQkFBQTtFQUNBLGNBQUE7QUMvREo7QURrRUU7RUFDRSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQ2hFSjtBRHFFRTtFQUVFLGlCQUFBO0VBQ0EsY0FBQTtBQ25FSjtBRHNFRTtFQUNFLGNBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FDcEVKO0FEd0VBO0VBR0UsWUFBQTtFQUNBLFdBQUE7RUFJQSxnQkFBQTtFQUNBLGtCQUFBO0FDMUVGO0FENkVBO0VBQ0UsY0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUVBLFlBQUE7RUFDQSxrQkFBQTtBQzNFRjtBRGdGRTtFQUNFLGNBQUE7RUFDQSxZQUFBO0FDOUVKO0FEaUZFO0VBQ0UsV0FBQTtBQy9FSjtBRHFGQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLDJCQUFBO0FDbEZGO0FEbUZFO0VBQ0UsY0FBQTtBQ2pGSjtBRG1GRTtFQUNFLGNBQUE7QUNqRko7QURvRkU7RUFDRSxVQUFBO0VBQ0EsV0FBQTtBQ2xGSjtBRHNGQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUVBLHlCQUFBO0VBQ0EsVUFBQTtBQ3BGRjtBRHlGQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUN0RkY7QUQyRkE7RUFFRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7QUN6RkY7QUQ0RkE7RUFLRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDN0ZGO0FEdUZFO0VBQ0UseUJBQUE7QUNyRko7QUQ2RkE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBQzFGRjtBRDZGRTtFQUVFLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUVBLGFBQUE7QUM3Rko7QUQrRkU7RUFDRSxjQUFBO0VBRUEsY0FBQTtFQUNBLGFBQUE7QUM5Rko7QURnR0U7RUFDRSxXQUFBO0FDOUZKO0FEa0dBO0VBQ0UsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUMvRkY7QURrR0E7RUFDRSxXQUFBO0VBQ0EsMEJBQUE7RUFFQSw0QkFBQTtVQUFBLG9CQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtFQUNBLFlBQUE7RUFFQSx5QkFBQTtBQ2pHRjtBRG1HRTtFQUNFLHlCQUFBO0VBSUEsa0JBQUE7QUNwR0o7QUR1R0k7RUFDRSxjQUFBO0FDckdOO0FEeUdFO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtBQ3ZHSjtBRDBHRTtFQUNFLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQ3hHSjtBRHlHSTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUN2R047QUQ0R0U7RUFDRSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtBQzFHSjtBRGtIQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtBQy9HRjtBRGtIQTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FDL0dGO0FEa0hBO0VBQ0UsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBRUEsWUFBQTtBQ2hIRjtBRG1IQTtFQUVFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7QUNqSEY7QURvSEE7RUFFRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxVQUFBO0FDbEhGO0FEb0hFO0VBQ0UseUJBQUE7QUNsSEo7QURzSEE7RUFDRSxxQkFBQTtBQ25IRjtBRG9IRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0EsdUJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBRUEsV0FBQTtBQ25ISjtBRHNIRTtFQUNFLFdBQUE7QUNwSEo7QUR3SEU7RUFDRSwwQkFBQTtFQUNBLDJCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esd0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMkJBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQ3RISjtBRHlIRTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLDJDQUFBO0FDdkhKO0FEeUhJO0VBQ0UsWUFBQTtBQ3ZITjtBRDJIRTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtBQ3pISjtBRDhIQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0FDM0hGO0FEOEhBO0VBQ0UseUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDRCQUFBO0VBQ0EsNkJBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7QUMzSEY7QUQ2SEE7RUFDRSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FDMUhGO0FENkhBO0VBQ0UsYUFBQTtFQUNDLGlCQUFBO0FDMUhIO0FENkhBO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtBQzFIRjtBRDZIQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFFQyxpQkFBQTtFQUNDLGdCQUFBO0VBQ0YsZ0NBQUE7RUFDQSx1QkFBQTtBQzNIRjtBRDZIQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7QUMxSEY7QUQ2SEE7OztFQUdFLGdCQUFBO0FDMUhGO0FENkhBOztFQUVFLHFCQUFBO0VBQ0EsZ0JBQUE7QUMxSEY7QUQ2SEE7RUFDRSxxQkFBQTtBQzFIRjtBRDZIQTtFQUNFLDBCQUFBO0FDMUhGO0FENkhBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7QUMxSEY7QUQ2SEE7RUFDRSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0EseUJBQUE7VUFBQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0UsaUJBQUE7QUMxSEo7QUQ2SEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDMUhGO0FEK0hBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtBQzVIRjtBRGlJQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDOUhGO0FEaUlBO0VBQ0UsMEJBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUVBLGNBQUE7RUFDQSxlQUFBO0FDL0hGO0FEa0lBO0VBQ0UsMkJBQUE7QUMvSEY7QURpSUE7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUM5SEY7QURpSUE7RUFDRSxrQkFBQTtBQzlIRjtBRDZJQSxxQkFBQTtBQUNBO0VBQ0U7SUFDRSwyQkFBQTtJQUNBLFVBQUE7RUMxSUY7RUQ0SUE7SUFDRSwwQkFBQTtJQUNBLFVBQUE7RUMxSUY7RUQ0SUE7SUFDRSwwQkFBQTtJQUNBLFVBQUE7RUMxSUY7QUFDRjtBRDRJQTtFQUNFO0lBQ0UsOEJBQUE7SUFDQSxVQUFBO0VDMUlGO0VENElBO0lBQ0UsNkJBQUE7SUFDQSxVQUFBO0VDMUlGO0VENElBO0lBQ0UsNkJBQUE7SUFDQSxVQUFBO0VDMUlGO0FBQ0Y7QUQ0SUE7cURBQUE7QUFFQTtFQUNFLGlEQUFBO0VBQ0Esb0RBQUE7RUFDQSxzQkFBQTtFQUNBLGtCQUFBO0VBR0EsbUJBQUE7RUFDQSx3REFBQTtFQUtBLGtJQUFBO0VBRUEsa0RBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7QUM3SUY7QUQrSUEsbUdBQUE7QUFDQSxpREFBQTtBQUNBO0VBQ0UsYUFBQTtBQzVJRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gIG92ZXJmbG93OiB2aXNpYmxlICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi10b3A6IDAlICFpbXBvcnRhbnQ7IC8vIGNoYW5nZSBoZXIgZm9yIGlvc1xuXG4gIC8vIFtub1Njcm9sbF0ge1xuICAvLyAgIG92ZXJmbG93OiBoaWRkZW47XG4gIC8vIH1cblxuICAuZHJpdmUge1xuICAgIGNvbG9yOiAjZjdmN2Y3O1xuICAgIGZvbnQtZmFtaWx5OiBcIkZyYW5rbGluIEdvdGhpYyBNZWRpdW1cIiwgXCJBcmlhbCBOYXJyb3dcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIH1cblxuICAucHJpY2Uge1xuICAgIGNvbG9yOiByZ2IoMCwgMjU1LCAwKTtcbiAgfVxuXG4gIC5kYXRlIHtcbiAgICBjb2xvcjogcmdiKDI1NSwgMjA0LCA2Mik7XG4gIH1cblxuICAuZGVzdGluYXRpb24ge1xuICAgIGNvbG9yOiByZ2IoMjQ2LCAyNTIsIDI1NSk7XG4gIH1cblxuICAubG9jYXRpb24ge1xuICAgIGNvbG9yOiB3aGl0ZXNtb2tlO1xuICB9XG5cbiAgI21hcC1jYW52YXMge1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogMTAwJTtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgei1pbmRleDogMDtcbiAgfVxuXG5cblxuICBcbn1cblxuLy9ORXcgVHJhbnNpdGlvblxubGFiZWwgPiBpbnB1dCB7IFxuICBkaXNwbGF5OiBub25lO1xuICAgXG59XG5cbiNidXR0b24wIHsgIFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxpbmUtaGVpZ2h0OiA0NXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDUwcHg7ICBcbiAgYm9yZGVyOiAycHggZGFzaGVkIGJsYWNrO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiBhdXRvO1xuICB0b3A6IDUwJTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5sYWJlbCA+IGlucHV0IH4gLmJ1dHRvbiB7IFxuICBjdXJzb3I6IHBvaW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTsgIFxuICBcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IGFsbCAxcyBlYXNlLWluLW91dDtcbiAgdXNlci1zZWxlY3Q6IG5vbmU7XG4gIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XG59XG5cbmxhYmVsID4gaW5wdXQgfiAjYnV0dG9uMSB7IFxuICBcbiBcbiAgbGVmdDogMDtcbiAgICB0b3A6IDA7XG4gICAgdG9wOiA0NSU7XG5cbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xuICAgIGNvbG9yOiBibGFjaztcbiAgICBtYXJnaW4tdG9wOiAtNiU7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIFxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuXG5sYWJlbCA+IGlucHV0OmNoZWNrZWQgfiAjYnV0dG9uMSB7IFxuICBsZWZ0OiBjYWxjKDAlIC0gMHB4KTtcbiAgdG9wOiBjYWxjKDc1JSAtIDAlKTtcbiBcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IGFsbCAxcyBlYXNlLWluLW91dDtcbn1cblxuXG4ubWFpbi1jb257XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG4gIC5kcml2ZXJGb3VuZE5ld3tcbiAgbGVmdDogMCU7XG4gICBcbiAgICAgIHRvcDogMTQ3JTtcbiAgICBoZWlnaHQ6IDUwJTtcbiAgICB3aWR0aDogOTUlO1xuICAgIG1hcmdpbi1sZWZ0OiAxLjUlO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDAlO1xuICAgIHotaW5kZXg6IDE7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgfVxuLy9FTkRFXG5cbmlvbi1mb290ZXIge1xuICBAa2V5ZnJhbWVzIHdpa2kge1xuICAgIGZyb20ge1xuICAgICAgb3BhY2l0eTogMDtcbiAgICB9XG5cbiAgICB0byB7XG4gICAgICBvcGFjaXR5OiAxO1xuICAgIH1cbiAgfVxuXG4gIC5iYXJzIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgYmFja2dyb3VuZDogI2Y3ZjdmNztcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgei1pbmRleDogNDtcbiAgICBsaW5lLWhlaWdodDogNTBweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG5cbiAgICAubmFtZV90ZXh0IHtcbiAgICAgIHotaW5kZXg6IDM7XG4gICAgICBmb250LXNpemU6IDEuMmVtO1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgbWFyZ2luLXRvcDogMCU7XG4gICAgfVxuXG4gICAgLnByb2ZpbGVfYmFyIHtcbiAgICAgIG1hcmdpbi10b3A6IDUlO1xuICAgIH1cblxuICAgIGJ1dHRvbiB7XG4gICAgICBtYXJnaW4tdG9wOiAxNiU7XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICAvLyBtYXJnaW4tbGVmdDogMi41JTtcbiAgICAgIC8vIHBhZGRpbmc6IDBweDtcbiAgICAgIHdpZHRoOiA5NSU7XG4gICAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cbiAgfVxuXG4gICNteUJ1dHRpbiB7XG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgICBtYXJnaW4tdG9wOiAzJTtcbiAgICBwYWRkaW5nOiA1cHg7XG4gICAgZm9udC1zaXplOiAxLjJlbTtcbiAgICAvL2JhY2tncm91bmQ6I2ZmZmZmZjtcbiAgICAvL1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICB9XG5cbiAgcCB7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIG1hcmdpbjogMHB4O1xuICB9XG5cbiAgI2FCdXR0b24ge1xuICAgIGJhY2tncm91bmQ6IHJnYigzNiwgMTQwLCAyMTApO1xuICAgIGNvbG9yOiAjZjdmN2Y3O1xuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgcGFkZGluZzogMTBweDtcbiAgICB3aWR0aDogOTAlO1xuICAgIC8vbWFyZ2luLWxlZnQ6IDdweDtcbiAgfVxuXG4gIGlvbi10aXRsZSB7XG4gICAgY29sb3I6ICNmN2Y3ZjcgIWltcG9ydGFudDtcbiAgfVxufVxuXG5Aa2V5ZnJhbWVzIGxlZnRfcmlnaHQge1xuICAwJSB7XG4gICAgdG9wOiA3NSU7XG4gIH1cblxuICA1MCUge1xuICAgIHRvcDogODAlO1xuICB9XG5cbiAgMTAwJSB7XG4gICAgdG9wOiA3NSU7XG4gIH1cbn1cblxuLnJhdGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA4MCU7XG4gIG1hcmdpbi10b3A6IC0xMCU7XG4gIGNvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gIHRvcDogMTQlO1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICB6LWluZGV4OiA0O1xuICB3aWR0aDogNTBweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgaW9uLWljb24ge1xuICAgIGZvbnQtc2l6ZTogMS44ZW07XG4gICAgY29sb3I6IHJnYigyNTUsIDI1NSwgMjU1KTtcbiAgICB6LWluZGV4OiA0O1xuICB9XG59XG5cbiN0aXRsZSB7XG4gIGhlaWdodDogMiUgIWltcG9ydGFudDtcbiAgLy8gYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNHB4O1xuICAvLyBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNHB4O1xufVxuXG4jbXl0aXRlIHtcbiAgcGFkZGluZzogMnB4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICNmN2Y3Zjc7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIG1hcmdpbi1sZWZ0OiAxOSU7XG4gIG1hcmdpbi10b3A6IDIlO1xuICB3aWR0aDogNDUlO1xuICBoZWlnaHQ6IDE0JTtcbiAgaW9uLWljb24ge1xuICAgIHdpZHRoOiAxMCU7XG4gICAgbWFyZ2luOiAycHg7XG4gIH1cbiAgLy8gaGVpZ2h0OjE0JSA7XG4gIC8vICBib3JkZXI6IDFweCBzb2xpZCAjZmJiOTFkO1xufVxuXG4jbGVmdHNoaXQge1xuICBtYXJnaW46IDIlO1xuICBib3JkZXI6IDJweCBzb2xpZCAjZmJiOTFkO1xuICBib3JkZXItcmFkaXVzOiAxMnB4KzU7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiAjZmJiOTFkO1xuICBsZWZ0OiAzNSUgIWltcG9ydGFudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4jc3RhciB7XG4gIC8vIGNvbG9yOiBjb2xvcigkY29sb3JzLCBkYXJrLCBiYXNlKTtcbiAgYm9yZGVyOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgbWFyZ2luLXRvcDogMSU7XG4gIC8vXG59XG5cbiNyaWRlcyB7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG59XG5cbiN1bnN0YXJfMSB7XG4gIG1hcmdpbi10b3A6IDMlO1xuICAvLyAtLWJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL2Nhci5wbmdcIikgbm8tcmVwZWF0IGNlbnRlcjtcblxuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmYmI5MWQ7XG4gIC8vXG4gIGgyIHtcbiAgICBwYWRkaW5nOiA0cHggIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiAjZmJiOTFkO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIH1cbn1cblxuI3Vuc3RhciB7XG4gIG1hcmdpbi10b3A6IDMlO1xuICBtYXJnaW4tbGVmdDogMi41JSAhaW1wb3J0YW50O1xuICAvL1xuICAvLyAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDM5LCAxOCwgMzcsIDAuNTQxKSwgcmdiYSg2OSwgMjMsIDczLCAwLjYyMykgcmdiYSg5MCwgMTgsIDkwLCAwLjMzNikpICwgdXJsKCRteVVybCkgbm8tcmVwZWF0O1xufVxuXG4jdW5zdGFyXzIge1xuICBtYXJnaW4tdG9wOiAzJTtcbiAgbWFyZ2luLWxlZnQ6IDIuNSUgIWltcG9ydGFudDtcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoXCJzcmMvYXNzZXRzL2ltZy9hLXNwbGFzaC0xLnBuZ1wiKSBuby1yZXBlYXQgY2VudGVyO1xuXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmYmI5MWQ7XG4gIC8vXG4gIGgyIHtcbiAgICBwYWRkaW5nOiA0cHggIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiAjZmJiOTFkO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIH1cbn1cblxuI3RvcF9jb250YWluZXIge1xuICAvLyAtLWJhY2tncm91bmQ6IHVybChcInNyYy9hc3NldHMvaW1nL2xvZ28taWNvbi5wbmdcIikgbm8tcmVwZWF0IGNlbnRlcjtcbiAgaGVpZ2h0OiAzMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICAvL1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAwcHggIWltcG9ydGFudDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAwcHggIWltcG9ydGFudDtcbiAgYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xuXG4gIC5zdHVmZiB7XG4gICAgbWFyZ2luLXRvcDogMTAlO1xuICB9XG4gIC8vYmFja2dyb3VuZDogI2Y3ZjdmNztcbn1cblxuI2J1dHRvbkNvbnRhaW5lciB7XG4gIC8vIG1hcmdpbi10b3A6IDAlO1xuICBmbG9hdDogcmlnaHQ7XG4gIGJ1dHRvbiB7XG4gICAgLy9oZWlnaHQ6IDgwJSAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6IHJnYigwLCAwLCAwKTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgfVxuXG4gIC5idXR0b24ge1xuICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgfVxufVxuXG4jYnV0dG9uQ29udGFpbmVyMiB7XG4gIGJ1dHRvbiB7XG4gICAgLy9oZWlnaHQ6IDgwJSAhaW1wb3J0YW50O1xuICAgIGJhY2tncm91bmQ6IHJnYigwLCAwLCAwKTtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgfVxuXG4gIC5idXR0b24ge1xuICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgIHdpZHRoOiAxMzBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbiAgfVxufVxuXG4uZ3V0dG9uIHtcbiAgLy8gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudChyZ2JhKDEsIDE3LCA0MSwgMC44OSksIHJnYmEoOCwgMjUsIDQ0LCAwLjYyMykgcmdiYSgzLCAxNywgMzEsIDAuNDM4KSkgLCB1cmwoJG15VXJsKSBuby1yZXBlYXQ7XG5cbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgLy9tYXJnaW4tbGVmdDogMjYlO1xuICAvL2JvcmRlcjogMXB4IHNvbGlkICNmYmI5MWQ7XG4gIC8vYmFja2dyb3VuZC1wb3NpdGlvbjogbGVmdDtcbiAgZm9udC1zaXplOiAxLjFlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4jY29udGFpbmVyIHtcbiAgbWFyZ2luLXRvcDogMiU7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbiAgLy8gYm9yZGVyLXJhZGl1czoxMnB4O1xuICBjb2xvcjogd2hpdGU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLy8gYmFja2dyb3VuZDogI2ZiYjkxZDtcbiAgLy8gYm9yZGVyOiAxcHggc29saWQgI2ZiYjkxZDtcbiAgLy9cbiAgLy8gb3ZlcmZsb3c6IGhpZGRlbjtcbiAgaDIge1xuICAgIGZvbnQtc2l6ZTogMWVtO1xuICAgIGhlaWdodDogYXV0bztcbiAgfVxuXG4gIGlvbi1pY29uIHtcbiAgICBtYXJnaW46IDNweDtcbiAgfVxufVxuXG5cblxuI2NvbnRhaW5lcl8xIHtcbiAgcGFkZGluZzogMTBweDtcbiAgd2lkdGg6IDUwJTtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW46IDFweDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgZm9udC1zaXplOiAxLjNlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXI6IDAuNXB4IHNvbGlkIHJnYigyNDgsIDI0OCwgMjQ4KTtcbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjZmZmZmZmO1xuICB9XG4gIGgyIHtcbiAgICBmb250LXNpemU6IDFlbTtcbiAgfVxuXG4gIC5wcm9maWxlLXBpYyB7XG4gICAgd2lkdGg6IDE1JTtcbiAgICBoZWlnaHQ6IDE4JTtcbiAgfVxufVxuXG5pb24tdG9nZ2xlIHtcbiAgZmxvYXQ6IHJpZ2h0O1xuICB3aWR0aDogMTAwJTtcbiAgYm9yZGVyOiAycHggc29saWQgI2Y3ZjdmNztcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgcGFkZGluZzogYXV0bztcblxuICBtYXJnaW4tdG9wOiA0JSAhaW1wb3J0YW50O1xuICBtYXJnaW46IDMlO1xufVxuXG5cblxuI2hlYWRlciB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDVweDtcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xuICBjb2xvcjogYmxhY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYmFja2dyb3VuZDogI2Y3ZjdmNztcbiAgLy9vdmVyZmxvdzogaGlkZGVuO1xuICAvLyBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xufVxuXG4jc2xvcHcge1xuICAvL2JvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tbGVmdDogLTElO1xuICBvdmVyZmxvdzogYXV0byAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWlkLXJpZ2h0IHtcbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjMGEwYTBhICFpbXBvcnRhbnQ7XG4gIH1cblxuICBwYWRkaW5nLWxlZnQ6IDgwJTtcbiAgbWFyZ2luLXRvcDogMjAlO1xuICB6LWluZGV4OiAzO1xufVxuXG4udG9wQmFyIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogNSU7IC8vY2hhbmdlIHRvIDEwJVxuICB6LWluZGV4OiAzO1xuICBwYWRkaW5nOiA4cHg7XG4gIC8vYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KHJnYmEoOTUsIDk0LCA5NCwgMC4wNDEpLCByZ2JhKDIsIDkyLCAxNjEsIDApIDM4KTtcblxuICBidXR0b24ge1xuICAgIC8vaGVpZ2h0OiA4MCUgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiByZ2IoMCwgMCwgMCk7XG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgICBjb2xvcjogI2ZmZmZmZjtcbiAgICBtYXJnaW46IDZweDtcbiAgICAvLyBib3JkZXI6IDZweCBzb2xpZCNmZmZmZmY7XG4gICAgcGFkZGluZzogMTVweDtcbiAgfVxuICBpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyZW07XG5cbiAgICBjb2xvcjogIzAwMDAwMDtcbiAgICBwYWRkaW5nOiAxNXB4O1xuICB9XG4gIC5idXR0b24tcmlnaHQge1xuICAgIGZsb2F0OiBsZWZ0O1xuICB9XG59XG5cbiNteUJhZGdlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICBmb250LXNpemU6IDEuNmVtO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuXG4jbWFwX2xheWVyIHtcbiAgei1pbmRleDogMTM7XG4gIHBvc2l0aW9uOiB1bnNldCAhaW1wb3J0YW50O1xuICAvLyBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBhbmltYXRpb24tbmFtZTogd2lraTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiA1cztcbiAgcGFkZGluZzogNXB4O1xuICAvLyBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBvdmVyZmxvdzogYXV0byAhaW1wb3J0YW50O1xuICAvLyBtYXJnaW4tdG9wOiAyMCUgIWltcG9ydGFudDtcbiAgLmJ1dHRvbiB7XG4gICAgbWFyZ2luLXRvcDogMCUgIWltcG9ydGFudDtcbiAgICAvLyBoZWlnaHQ6IGF1dG87XG4gICAgLy93aWR0aDogMTAwJTtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAvL21hcmdpbjogNXB4O1xuICAgIC8vIHBhZGRpbmc6IDIwcHg7XG4gICAgaW9uLWljb24ge1xuICAgICAgY29sb3I6ICMwMDAwMDA7XG4gICAgfVxuICB9XG5cbiAgLmd1dHMge1xuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xuICB9XG5cbiAgLmJ1dHRvbl8xIHtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAgIG1hcmdpbi10b3A6IDJweDtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgLnByb2ZpbGUtcGljIHtcbiAgICAgIGhlaWdodDogNzAlO1xuICAgICAgd2lkdGg6IDQzJTtcbiAgICAgIGJvcmRlcjogMS42cHggc29saWQgcmdiKDI1NSwgMjU1LCAyNTUpO1xuICAgIH1cbiAgICAvL2JhY2tncm91bmQtcG9zaXRpb246IGxlZnQ7XG4gIH1cblxuICAuYnV0dG9uXzIge1xuICAgIGJhY2tncm91bmQ6ICM5ZDA4ZTI7XG4gICAgbWFyZ2luLXRvcDogMnB4O1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0O1xuICB9XG5cbiAgLy8gaW9uLWljb257XG4gIC8vICAgY29sb3I6I2ZmZmZmZjtcbiAgLy8gfVxufVxuXG5oMSB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXNpemU6IDEuNGVtO1xufVxuXG4uc3RhY2sge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMC41ZW07XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmd1dHRvbnoge1xuICBiYWNrZ3JvdW5kOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDUwJTtcbiAgbWFyZ2luLWxlZnQ6IDQlO1xuICAvLyBib3gtc2hhZG93OiAwcHggMHB4IDFweCAwcHggY29sb3IoJGNvbG9ycywgbmF2LXNoYWRvdywgYmFzZSk7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1pZC1yaWdodCB7XG4gIC8vIGZsb2F0OiByaWdodDtcbiAgcGFkZGluZy1sZWZ0OiA4MCU7XG4gIG1hcmdpbi10b3A6IDIwJTtcbiAgei1pbmRleDogMztcbn1cblxuLm1pZC1yaWdodDIge1xuICAvLyBmbG9hdDogcmlnaHQ7XG4gIHBhZGRpbmctbGVmdDogODAlO1xuICBtYXJnaW4tdG9wOiA0MCU7XG4gIHotaW5kZXg6IDM7XG5cbiAgaW9uLWljb24ge1xuICAgIGNvbG9yOiAjMGEwYTBhICFpbXBvcnRhbnQ7XG4gIH1cbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6ICMwMDAwMDAxNTtcbiAgI2xvd2VyX2l0ZW1zIHtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNXB4O1xuICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1cHg7XG4gICAgLy8gYmFja2dyb3VuZDojZmZmZmZmO1xuICAgIHotaW5kZXg6IDE1O1xuICB9XG5cbiAgLmJ1dHRvbiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgLy9jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgfVxuXG4gIC5idXR0b25zIHtcbiAgICBtYXJnaW4tdG9wOiAxMCUgIWltcG9ydGFudDtcbiAgICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XG4gICAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgbWFyZ2luLWxlZnQ6IDYwJTtcbiAgICBwYWRkaW5nOiBhdXRvICFpbXBvcnRhbnQ7XG4gICAgYmFja2dyb3VuZDogI2ZiYjkxZDtcbiAgICB6LWluZGV4OiAzMDAgIWltcG9ydGFudDtcbiAgICBib3JkZXI6IDAuN3B4IHNvbGlkICNmYmI5MWQ7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGZvbnQtc2l6ZTogMTdweDtcbiAgfVxuXG4gICNyaWRlcyB7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBtYXJnaW46IDFweDtcbiAgICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuXG4gICAgaW9uLXRpdGxlIHtcbiAgICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB9XG4gIH1cblxuICAuYmFja19jb2wge1xuICAgIGJhY2tncm91bmQ6ICMxZTE4MWY7XG4gICAgY29sb3I6IHdoaXRlO1xuICB9XG59XG5cbi8vTkVXIERFU0lHTkdTXG4uZHJpdmVyRm91bmQge1xuICBoZWlnaHQ6IDUwJTtcbiAgd2lkdGg6IDk1JTtcbiAgbWFyZ2luLWxlZnQ6IDEuNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMiU7XG4gIHotaW5kZXg6IDE7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xufVxuXG4uaGVhZFNlY3Rpb24ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xuICBjb2xvcjogI2ZiYjkxZDtcbiAgbWFyZ2luLXRvcDogLTYlO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA4OHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubW92ZUhlYWRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBtYXJnaW4tdG9wOiAyMHB4O1xuICBtYXJnaW4tbGVmdDogMHB4O1xuICBtYXJnaW4tcmlnaHQ6IDBweDtcbn1cblxuLnJlcXVlc3QtZm9yLXJpZGUyIHtcbiAgaGVpZ2h0OiAyMDBweDtcbiAgIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uY2VudGVyVGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHRDb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgLy8gcGFkZGluZzogMTBweDtcbiAgIHBhZGRpbmctcmlnaHQ6IDUlO1xuICAgIHBhZGRpbmctbGVmdDogNSU7XG4gIGJvcmRlci1ib3R0b206ICNiYWJhYmEgc29saWQgMXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cbi5yZXN1bHRDb250YWluZXIyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbGVmdDogMDtcbiAgbWFyZ2luLXRvcDogMHB4O1xuICBtYXJnaW4tYm90dG9tOiAwcHg7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xufVxuXG4uYm9va0ltYWdlLFxuLmJvb2tUaXRsZSxcbi5ib29rUHJpY2Uge1xuICBtYXJnaW4tbGVmdDogN3B4O1xufVxuXG4uY29udGVudC13cmFwLFxuLmltZy13cmFwcGVyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tbGVmdDogMHB4O1xufVxuXG4uY29udGVudC13cmFwMiB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cblxuI2RyaXZlcmljb25TaXplIHtcbiAgZm9udC1zaXplOiA5MHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jZW50ZXJCdG4ge1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuI2J0bi1jZW50ZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uY2hhdEljb24ge1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIC8vIG1hcmdpbi1sZWZ0OiAwJTtcbiAgLy8gbWFyZ2luLXRvcDogLTEyJTtcbn1cblxuLmNhbGxJY29uIHtcbiAgd2lkdGg6IDQwcHg7XG4gIGhlaWdodDogNDBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAvLyBtYXJnaW4tbGVmdDogMTAlO1xuICAvLyBtYXJnaW4tdG9wOiAtMTIlO1xufVxuXG4uZG90dCB7XG4gIHdpZHRoOiAyMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG59XG5cbi5idXR0b25zMTIzIHtcbiAgbWFyZ2luLXRvcDogMTAlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogNjAlO1xuICBwYWRkaW5nOiBhdXRvICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICNmYmI5MWQ7XG4gIHotaW5kZXg6IDMwMCAhaW1wb3J0YW50O1xuXG4gIGNvbG9yOiAjMDUwNTA1O1xuICBmb250LXNpemU6IDE3cHg7XG59XG5cbi5idXR0b24tbmF0aXZlIHtcbiAgYm94LXNoYWRvdzogbm9uZSAhaW1wb3J0YW50O1xufVxuLmNlbnRlckVhcm5pbmdzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjZmJiOTFkO1xufVxuXG4udG9wcGVkIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAvLyBtYXJnaW46IDEwcHg7XG4gIC8vIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIC8vIGNvbG9yOiAjZmJiOTFkO1xufVxuXG4vLyAuaW9uLWNvbG9yLWRhcmsge1xuLy8gICAtLWlvbi1jb2xvci1iYXNlOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4vLyAgIC0taW9uLWNvbG9yLWJhc2UtcmdiOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4vLyAgIC0taW9uLWNvbG9yLWNvbnRyYXN0OiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4vLyAgIC0taW9uLWNvbG9yLWNvbnRyYXN0LXJnYjogI2ZiYjkxZCAhaW1wb3J0YW50O1xuLy8gICAtLWlvbi1jb2xvci1zaGFkZTogI2ZiYjkxZCAhaW1wb3J0YW50O1xuLy8gICAtLWlvbi1jb2xvci10aW50OiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4vLyB9XG5cbi8qIHlvdXIgY3VzdG9tIENTUyBcXCovXG5Aa2V5ZnJhbWVzIHB1bHNhdGUge1xuICBmcm9tIHtcbiAgICAtbW96LXRyYW5zZm9ybTogc2NhbGUoMC4yNSk7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICA5NSUge1xuICAgIC1tb3otdHJhbnNmb3JtOiBzY2FsZSgxLjMpO1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgdG8ge1xuICAgIC1tb3otdHJhbnNmb3JtOiBzY2FsZSgwLjMpO1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbn1cbkAtd2Via2l0LWtleWZyYW1lcyBwdWxzYXRlIHtcbiAgZnJvbSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHNjYWxlKDAuMjUpO1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbiAgOTUlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMS4zKTtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG4gIHRvIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMC4zKTtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG59XG4vKiBnZXQgdGhlIGNvbnRhaW5lciB0aGF0J3MganVzdCBvdXRzaWRlIHRoZSBtYXJrZXIgaW1hZ2UsIFxuXHRcdHdoaWNoIGp1c3QgaGFwcGVucyB0byBoYXZlIG91ciBNYXJrZXIgdGl0bGUgaW4gaXQgKi9cbiNtYXAtY2FudmFzIGRpdlt0aXRsZT1cInBpbkljb25cIl0ge1xuICAtbW96LWFuaW1hdGlvbjogcHVsc2F0ZSAxLjVzIGVhc2UtaW4tb3V0IGluZmluaXRlO1xuICAtd2Via2l0LWFuaW1hdGlvbjogcHVsc2F0ZSAxLjVzIGVhc2UtaW4tb3V0IGluZmluaXRlO1xuICBib3JkZXI6IDFwdCBzb2xpZCAjZmZmO1xuICAvKiBtYWtlIGEgY2lyY2xlICovXG4gIC1tb3otYm9yZGVyLXJhZGl1czogNTFweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MXB4O1xuICBib3JkZXItcmFkaXVzOiA1MXB4O1xuICAvKiBtdWx0aXBseSB0aGUgc2hhZG93cywgaW5zaWRlIGFuZCBvdXRzaWRlIHRoZSBjaXJjbGUgKi9cbiAgLW1vei1ib3gtc2hhZG93OiBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCwgaW5zZXQgMCAwIDVweCAjZmJiOTFkLFxuICAgIDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkLCAwIDAgNXB4ICNmYmI5MWQ7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogaW5zZXQgMCAwIDVweCAjZmJiOTFkLCBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCxcbiAgICAwIDAgNXB4ICNmYmI5MWQsIDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkO1xuICBib3gtc2hhZG93OiBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCwgaW5zZXQgMCAwIDVweCAjZmJiOTFkLFxuICAgIDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkLCAwIDAgNXB4ICNmYmI5MWQ7XG4gIC8qIHNldCB0aGUgcmluZydzIG5ldyBkaW1lbnNpb24gYW5kIHJlLWNlbnRlciBpdCAqL1xuICBoZWlnaHQ6IDUxcHggIWltcG9ydGFudDtcbiAgbWFyZ2luOiAtMThweCAwIDAgLTE4cHg7XG4gIHdpZHRoOiA1MXB4ICFpbXBvcnRhbnQ7XG59XG4vKiBoaWRlIHRoZSBzdXBlcmZsdW91cyBtYXJrZXIgaW1hZ2Ugc2luY2UgaXQgd291bGQgZXhwYW5kIGFuZCBzaHJpbmsgd2l0aCBpdHMgY29udGFpbmluZyBlbGVtZW50ICovXG4vKlx0I21hcC1jYW52YXMgZGl2W3N0eWxlKj1cIjk4NzY1NFwiXVt0aXRsZV0gaW1nIHsqL1xuI21hcC1jYW52YXMgZGl2W3RpdGxlPVwicGluSWNvblwiXSBpbWcge1xuICBkaXNwbGF5OiBub25lO1xufVxuIiwiaW9uLWNvbnRlbnQge1xuICBvdmVyZmxvdzogdmlzaWJsZSAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiAwJSAhaW1wb3J0YW50O1xufVxuaW9uLWNvbnRlbnQgLmRyaXZlIHtcbiAgY29sb3I6ICNmN2Y3Zjc7XG4gIGZvbnQtZmFtaWx5OiBcIkZyYW5rbGluIEdvdGhpYyBNZWRpdW1cIiwgXCJBcmlhbCBOYXJyb3dcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG59XG5pb24tY29udGVudCAucHJpY2Uge1xuICBjb2xvcjogbGltZTtcbn1cbmlvbi1jb250ZW50IC5kYXRlIHtcbiAgY29sb3I6ICNmZmNjM2U7XG59XG5pb24tY29udGVudCAuZGVzdGluYXRpb24ge1xuICBjb2xvcjogI2Y2ZmNmZjtcbn1cbmlvbi1jb250ZW50IC5sb2NhdGlvbiB7XG4gIGNvbG9yOiB3aGl0ZXNtb2tlO1xufVxuaW9uLWNvbnRlbnQgI21hcC1jYW52YXMge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHotaW5kZXg6IDA7XG59XG5cbmxhYmVsID4gaW5wdXQge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4jYnV0dG9uMCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGluZS1oZWlnaHQ6IDQ1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbiAgYm9yZGVyOiAycHggZGFzaGVkIGJsYWNrO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiBhdXRvO1xuICB0b3A6IDUwJTtcbiAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgb3BhY2l0eTogMC41O1xufVxuXG5sYWJlbCA+IGlucHV0IH4gLmJ1dHRvbiB7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsaW5lLWhlaWdodDogNDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAtd2Via2l0LXRyYW5zaXRpb246IGFsbCAxcyBlYXNlLWluLW91dDtcbiAgdHJhbnNpdGlvbjogYWxsIDFzIGVhc2UtaW4tb3V0O1xuICB1c2VyLXNlbGVjdDogbm9uZTtcbiAgLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcbn1cblxubGFiZWwgPiBpbnB1dCB+ICNidXR0b24xIHtcbiAgbGVmdDogMDtcbiAgdG9wOiAwO1xuICB0b3A6IDQ1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW4tdG9wOiAtNiU7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDI3cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxubGFiZWwgPiBpbnB1dDpjaGVja2VkIH4gI2J1dHRvbjEge1xuICBsZWZ0OiBjYWxjKDAlIC0gMHB4KTtcbiAgdG9wOiBjYWxjKDc1JSAtIDAlKTtcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBhbGwgMXMgZWFzZS1pbi1vdXQ7XG4gIHRyYW5zaXRpb246IGFsbCAxcyBlYXNlLWluLW91dDtcbn1cblxuLm1haW4tY29uIHtcbiAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG59XG5cbi5kcml2ZXJGb3VuZE5ldyB7XG4gIGxlZnQ6IDAlO1xuICB0b3A6IDE0NyU7XG4gIGhlaWdodDogNTAlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW4tbGVmdDogMS41JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5cbkBrZXlmcmFtZXMgd2lraSB7XG4gIGZyb20ge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgdG8ge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cbmlvbi1mb290ZXIgLmJhcnMge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBiYWNrZ3JvdW5kOiAjZjdmN2Y3O1xuICBoZWlnaHQ6IGF1dG87XG4gIHotaW5kZXg6IDQ7XG4gIGxpbmUtaGVpZ2h0OiA1MHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5pb24tZm9vdGVyIC5iYXJzIC5uYW1lX3RleHQge1xuICB6LWluZGV4OiAzO1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDAlO1xufVxuaW9uLWZvb3RlciAuYmFycyAucHJvZmlsZV9iYXIge1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cbmlvbi1mb290ZXIgLmJhcnMgYnV0dG9uIHtcbiAgbWFyZ2luLXRvcDogMTYlO1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiA5NSU7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbmlvbi1mb290ZXIgI215QnV0dGluIHtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgbWFyZ2luLXRvcDogMyU7XG4gIHBhZGRpbmc6IDVweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG5pb24tZm9vdGVyIHAge1xuICBwYWRkaW5nOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuaW9uLWZvb3RlciAjYUJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICMyNDhjZDI7XG4gIGNvbG9yOiAjZjdmN2Y3O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICB3aWR0aDogOTAlO1xufVxuaW9uLWZvb3RlciBpb24tdGl0bGUge1xuICBjb2xvcjogI2Y3ZjdmNyAhaW1wb3J0YW50O1xufVxuXG5Aa2V5ZnJhbWVzIGxlZnRfcmlnaHQge1xuICAwJSB7XG4gICAgdG9wOiA3NSU7XG4gIH1cbiAgNTAlIHtcbiAgICB0b3A6IDgwJTtcbiAgfVxuICAxMDAlIHtcbiAgICB0b3A6IDc1JTtcbiAgfVxufVxuLnJhdGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIG1hcmdpbi1sZWZ0OiA4MCU7XG4gIG1hcmdpbi10b3A6IC0xMCU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdG9wOiAxNCU7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIHotaW5kZXg6IDQ7XG4gIHdpZHRoOiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xufVxuLnJhdGUgaW9uLWljb24ge1xuICBmb250LXNpemU6IDEuOGVtO1xuICBjb2xvcjogd2hpdGU7XG4gIHotaW5kZXg6IDQ7XG59XG5cbiN0aXRsZSB7XG4gIGhlaWdodDogMiUgIWltcG9ydGFudDtcbn1cblxuI215dGl0ZSB7XG4gIHBhZGRpbmc6IDJweCAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZjdmN2Y3O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBtYXJnaW4tbGVmdDogMTklO1xuICBtYXJnaW4tdG9wOiAyJTtcbiAgd2lkdGg6IDQ1JTtcbiAgaGVpZ2h0OiAxNCU7XG59XG4jbXl0aXRlIGlvbi1pY29uIHtcbiAgd2lkdGg6IDEwJTtcbiAgbWFyZ2luOiAycHg7XG59XG5cbiNsZWZ0c2hpdCB7XG4gIG1hcmdpbjogMiU7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmYmI5MWQ7XG4gIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6IHdoaXRlO1xuICBiYWNrZ3JvdW5kOiAjZmJiOTFkO1xuICBsZWZ0OiAzNSUgIWltcG9ydGFudDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4jc3RhciB7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIG1hcmdpbi10b3A6IDElO1xufVxuXG4jcmlkZXMge1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xufVxuXG4jdW5zdGFyXzEge1xuICBtYXJnaW4tdG9wOiAzJTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmJiOTFkO1xufVxuI3Vuc3Rhcl8xIGgyIHtcbiAgcGFkZGluZzogNHB4ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQ6ICNmYmI5MWQ7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG59XG5cbiN1bnN0YXIge1xuICBtYXJnaW4tdG9wOiAzJTtcbiAgbWFyZ2luLWxlZnQ6IDIuNSUgIWltcG9ydGFudDtcbn1cblxuI3Vuc3Rhcl8yIHtcbiAgbWFyZ2luLXRvcDogMyU7XG4gIG1hcmdpbi1sZWZ0OiAyLjUlICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmYmI5MWQ7XG59XG4jdW5zdGFyXzIgaDIge1xuICBwYWRkaW5nOiA0cHggIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZiYjkxZDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cblxuI3RvcF9jb250YWluZXIge1xuICBoZWlnaHQ6IDMwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMHB4ICFpbXBvcnRhbnQ7XG59XG4jdG9wX2NvbnRhaW5lciAuc3R1ZmYge1xuICBtYXJnaW4tdG9wOiAxMCU7XG59XG5cbiNidXR0b25Db250YWluZXIge1xuICBmbG9hdDogcmlnaHQ7XG59XG4jYnV0dG9uQ29udGFpbmVyIGJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IGJsYWNrO1xuICBjb2xvcjogI2ZmZmZmZjtcbn1cbiNidXR0b25Db250YWluZXIgLmJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICB3aWR0aDogMTMwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuI2J1dHRvbkNvbnRhaW5lcjIgYnV0dG9uIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuI2J1dHRvbkNvbnRhaW5lcjIgLmJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICB3aWR0aDogMTMwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGRlcjtcbn1cblxuLmd1dHRvbiB7XG4gIGhlaWdodDogYXV0bztcbiAgd2lkdGg6IDEwMCU7XG4gIGZvbnQtc2l6ZTogMS4xZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuI2NvbnRhaW5lciB7XG4gIG1hcmdpbi10b3A6IDIlO1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuI2NvbnRhaW5lciBoMiB7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBoZWlnaHQ6IGF1dG87XG59XG4jY29udGFpbmVyIGlvbi1pY29uIHtcbiAgbWFyZ2luOiAzcHg7XG59XG5cbiNjb250YWluZXJfMSB7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIHdpZHRoOiA1MCU7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgbWFyZ2luOiAxcHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4zZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyOiAwLjVweCBzb2xpZCAjZjhmOGY4O1xufVxuI2NvbnRhaW5lcl8xIGlvbi1pY29uIHtcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4jY29udGFpbmVyXzEgaDIge1xuICBmb250LXNpemU6IDFlbTtcbn1cbiNjb250YWluZXJfMSAucHJvZmlsZS1waWMge1xuICB3aWR0aDogMTUlO1xuICBoZWlnaHQ6IDE4JTtcbn1cblxuaW9uLXRvZ2dsZSB7XG4gIGZsb2F0OiByaWdodDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmN2Y3Zjc7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gIHBhZGRpbmc6IGF1dG87XG4gIG1hcmdpbi10b3A6IDQlICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMyU7XG59XG5cbiNoZWFkZXIge1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1cHg7XG4gIGJvcmRlci1yYWRpdXM6IDBweDtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6ICNmN2Y3Zjc7XG59XG5cbiNzbG9wdyB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tbGVmdDogLTElO1xuICBvdmVyZmxvdzogYXV0byAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4ubWlkLXJpZ2h0IHtcbiAgcGFkZGluZy1sZWZ0OiA4MCU7XG4gIG1hcmdpbi10b3A6IDIwJTtcbiAgei1pbmRleDogMztcbn1cbi5taWQtcmlnaHQgaW9uLWljb24ge1xuICBjb2xvcjogIzBhMGEwYSAhaW1wb3J0YW50O1xufVxuXG4udG9wQmFyIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLXRvcDogNSU7XG4gIHotaW5kZXg6IDM7XG4gIHBhZGRpbmc6IDhweDtcbn1cbi50b3BCYXIgYnV0dG9uIHtcbiAgYmFja2dyb3VuZDogYmxhY2s7XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBtYXJnaW46IDZweDtcbiAgcGFkZGluZzogMTVweDtcbn1cbi50b3BCYXIgaW9uLWljb24ge1xuICBmb250LXNpemU6IDJlbTtcbiAgY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmc6IDE1cHg7XG59XG4udG9wQmFyIC5idXR0b24tcmlnaHQge1xuICBmbG9hdDogbGVmdDtcbn1cblxuI215QmFkZ2Uge1xuICBoZWlnaHQ6IGF1dG87XG4gIGZvbnQtc2l6ZTogMS42ZW07XG4gIGJvcmRlci1yYWRpdXM6IDEycHg7XG59XG5cbiNtYXBfbGF5ZXIge1xuICB6LWluZGV4OiAxMztcbiAgcG9zaXRpb246IHVuc2V0ICFpbXBvcnRhbnQ7XG4gIGFuaW1hdGlvbi1uYW1lOiB3aWtpO1xuICBhbmltYXRpb24tZHVyYXRpb246IDVzO1xuICBwYWRkaW5nOiA1cHg7XG4gIG92ZXJmbG93OiBhdXRvICFpbXBvcnRhbnQ7XG59XG4jbWFwX2xheWVyIC5idXR0b24ge1xuICBtYXJnaW4tdG9wOiAwJSAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4jbWFwX2xheWVyIC5idXR0b24gaW9uLWljb24ge1xuICBjb2xvcjogIzAwMDAwMDtcbn1cbiNtYXBfbGF5ZXIgLmd1dHMge1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbn1cbiNtYXBfbGF5ZXIgLmJ1dHRvbl8xIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4jbWFwX2xheWVyIC5idXR0b25fMSAucHJvZmlsZS1waWMge1xuICBoZWlnaHQ6IDcwJTtcbiAgd2lkdGg6IDQzJTtcbiAgYm9yZGVyOiAxLjZweCBzb2xpZCB3aGl0ZTtcbn1cbiNtYXBfbGF5ZXIgLmJ1dHRvbl8yIHtcbiAgYmFja2dyb3VuZDogIzlkMDhlMjtcbiAgbWFyZ2luLXRvcDogMnB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIHdpZHRoOiAxMDAlO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQ7XG59XG5cbmgxIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMS40ZW07XG59XG5cbi5zdGFjayB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAwLjVlbTtcbiAgY29sb3I6IHdoaXRlO1xufVxuXG4uZ3V0dG9ueiB7XG4gIGJhY2tncm91bmQ6IHJlZDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNTAlO1xuICBtYXJnaW4tbGVmdDogNCU7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLm1pZC1yaWdodCB7XG4gIHBhZGRpbmctbGVmdDogODAlO1xuICBtYXJnaW4tdG9wOiAyMCU7XG4gIHotaW5kZXg6IDM7XG59XG5cbi5taWQtcmlnaHQyIHtcbiAgcGFkZGluZy1sZWZ0OiA4MCU7XG4gIG1hcmdpbi10b3A6IDQwJTtcbiAgei1pbmRleDogMztcbn1cbi5taWQtcmlnaHQyIGlvbi1pY29uIHtcbiAgY29sb3I6ICMwYTBhMGEgIWltcG9ydGFudDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6ICMwMDAwMDAxNTtcbn1cbmlvbi1mb290ZXIgI2xvd2VyX2l0ZW1zIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogMTAwJTtcbiAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDVweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcbiAgei1pbmRleDogMTU7XG59XG5pb24tZm9vdGVyIC5idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbn1cbmlvbi1mb290ZXIgLmJ1dHRvbnMge1xuICBtYXJnaW4tdG9wOiAxMCUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogNXB4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiA2MCU7XG4gIHBhZGRpbmc6IGF1dG8gIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZDogI2ZiYjkxZDtcbiAgei1pbmRleDogMzAwICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMC43cHggc29saWQgI2ZiYjkxZDtcbiAgY29sb3I6IHdoaXRlO1xuICBmb250LXNpemU6IDE3cHg7XG59XG5pb24tZm9vdGVyICNyaWRlcyB7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIG1hcmdpbjogMXB4O1xuICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xufVxuaW9uLWZvb3RlciAjcmlkZXMgaW9uLXRpdGxlIHtcbiAgY29sb3I6IHdoaXRlO1xufVxuaW9uLWZvb3RlciAuYmFja19jb2wge1xuICBiYWNrZ3JvdW5kOiAjMWUxODFmO1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5kcml2ZXJGb3VuZCB7XG4gIGhlaWdodDogNTAlO1xuICB3aWR0aDogOTUlO1xuICBtYXJnaW4tbGVmdDogMS41JTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAyJTtcbiAgei1pbmRleDogMTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMzBweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG59XG5cbi5oZWFkU2VjdGlvbiB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gIGNvbG9yOiAjZmJiOTFkO1xuICBtYXJnaW4tdG9wOiAtNiU7XG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDI3cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDg4cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm1vdmVIZWFkZXIge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgbWFyZ2luLWxlZnQ6IDBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwcHg7XG59XG5cbi5yZXF1ZXN0LWZvci1yaWRlMiB7XG4gIGhlaWdodDogMjAwcHg7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uY2VudGVyVGV4dCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHRDb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgcGFkZGluZy1yaWdodDogNSU7XG4gIHBhZGRpbmctbGVmdDogNSU7XG4gIGJvcmRlci1ib3R0b206ICNiYWJhYmEgc29saWQgMXB4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcbn1cblxuLnJlc3VsdENvbnRhaW5lcjIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgcGFkZGluZzogMTBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG59XG5cbi5ib29rSW1hZ2UsXG4uYm9va1RpdGxlLFxuLmJvb2tQcmljZSB7XG4gIG1hcmdpbi1sZWZ0OiA3cHg7XG59XG5cbi5jb250ZW50LXdyYXAsXG4uaW1nLXdyYXBwZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbi1sZWZ0OiAwcHg7XG59XG5cbi5jb250ZW50LXdyYXAyIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG4jZHJpdmVyaWNvblNpemUge1xuICBmb250LXNpemU6IDkwcHggIWltcG9ydGFudDtcbn1cblxuLmNlbnRlckJ0biB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG4jYnRuLWNlbnRlciB7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xufVxuXG4uY2hhdEljb24ge1xuICB3aWR0aDogNDBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5jYWxsSWNvbiB7XG4gIHdpZHRoOiA0MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmRvdHQge1xuICB3aWR0aDogMjBweDtcbiAgaGVpZ2h0OiAyMHB4O1xufVxuXG4uYnV0dG9uczEyMyB7XG4gIG1hcmdpbi10b3A6IDEwJSAhaW1wb3J0YW50O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWxlZnQ6IDYwJTtcbiAgcGFkZGluZzogYXV0byAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kOiAjZmJiOTFkO1xuICB6LWluZGV4OiAzMDAgIWltcG9ydGFudDtcbiAgY29sb3I6ICMwNTA1MDU7XG4gIGZvbnQtc2l6ZTogMTdweDtcbn1cblxuLmJ1dHRvbi1uYXRpdmUge1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5jZW50ZXJFYXJuaW5ncyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAxMHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogI2ZiYjkxZDtcbn1cblxuLnRvcHBlZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLyogeW91ciBjdXN0b20gQ1NTIFxcKi9cbkBrZXlmcmFtZXMgcHVsc2F0ZSB7XG4gIGZyb20ge1xuICAgIC1tb3otdHJhbnNmb3JtOiBzY2FsZSgwLjI1KTtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG4gIDk1JSB7XG4gICAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDEuMyk7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICB0byB7XG4gICAgLW1vei10cmFuc2Zvcm06IHNjYWxlKDAuMyk7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxufVxuQC13ZWJraXQta2V5ZnJhbWVzIHB1bHNhdGUge1xuICBmcm9tIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogc2NhbGUoMC4yNSk7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxuICA5NSUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgxLjMpO1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbiAgdG8ge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiBzY2FsZSgwLjMpO1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cbn1cbi8qIGdldCB0aGUgY29udGFpbmVyIHRoYXQncyBqdXN0IG91dHNpZGUgdGhlIG1hcmtlciBpbWFnZSwgXG5cdFx0d2hpY2gganVzdCBoYXBwZW5zIHRvIGhhdmUgb3VyIE1hcmtlciB0aXRsZSBpbiBpdCAqL1xuI21hcC1jYW52YXMgZGl2W3RpdGxlPXBpbkljb25dIHtcbiAgLW1vei1hbmltYXRpb246IHB1bHNhdGUgMS41cyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHB1bHNhdGUgMS41cyBlYXNlLWluLW91dCBpbmZpbml0ZTtcbiAgYm9yZGVyOiAxcHQgc29saWQgI2ZmZjtcbiAgLyogbWFrZSBhIGNpcmNsZSAqL1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDUxcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTFweDtcbiAgYm9yZGVyLXJhZGl1czogNTFweDtcbiAgLyogbXVsdGlwbHkgdGhlIHNoYWRvd3MsIGluc2lkZSBhbmQgb3V0c2lkZSB0aGUgY2lyY2xlICovXG4gIC1tb3otYm94LXNoYWRvdzogaW5zZXQgMCAwIDVweCAjZmJiOTFkLCBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkLCAwIDAgNXB4ICNmYmI5MWQsIDAgMCA1cHggI2ZiYjkxZDtcbiAgLXdlYmtpdC1ib3gtc2hhZG93OiBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCwgaW5zZXQgMCAwIDVweCAjZmJiOTFkLCAwIDAgNXB4ICNmYmI5MWQsIDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkO1xuICBib3gtc2hhZG93OiBpbnNldCAwIDAgNXB4ICNmYmI5MWQsIGluc2V0IDAgMCA1cHggI2ZiYjkxZCwgaW5zZXQgMCAwIDVweCAjZmJiOTFkLCAwIDAgNXB4ICNmYmI5MWQsIDAgMCA1cHggI2ZiYjkxZCwgMCAwIDVweCAjZmJiOTFkO1xuICAvKiBzZXQgdGhlIHJpbmcncyBuZXcgZGltZW5zaW9uIGFuZCByZS1jZW50ZXIgaXQgKi9cbiAgaGVpZ2h0OiA1MXB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogLTE4cHggMCAwIC0xOHB4O1xuICB3aWR0aDogNTFweCAhaW1wb3J0YW50O1xufVxuXG4vKiBoaWRlIHRoZSBzdXBlcmZsdW91cyBtYXJrZXIgaW1hZ2Ugc2luY2UgaXQgd291bGQgZXhwYW5kIGFuZCBzaHJpbmsgd2l0aCBpdHMgY29udGFpbmluZyBlbGVtZW50ICovXG4vKlx0I21hcC1jYW52YXMgZGl2W3N0eWxlKj1cIjk4NzY1NFwiXVt0aXRsZV0gaW1nIHsqL1xuI21hcC1jYW52YXMgZGl2W3RpdGxlPXBpbkljb25dIGltZyB7XG4gIGRpc3BsYXk6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/onesignal/ngx */ "./node_modules/@ionic-native/onesignal/ngx/index.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! firebase */ "./node_modules/firebase/dist/index.cjs.js");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/vibration/ngx */ "./node_modules/@ionic-native/vibration/ngx/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _services_settings_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var _services_location_tracker_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../services/location-tracker.service */ "./src/app/services/location-tracker.service.ts");
/* harmony import */ var _services_directionservice_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../../services/directionservice.service */ "./src/app/services/directionservice.service.ts");
/* harmony import */ var _services_onesignal_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../../services/onesignal.service */ "./src/app/services/onesignal.service.ts");
/* harmony import */ var _services_language_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../../services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _services_geocoder_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../../services/geocoder.service */ "./src/app/services/geocoder.service.ts");
/* harmony import */ var _services_pop_up_service__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../../services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _services_event_service__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../../services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var _services_profile_service__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../../services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var _news_news_page__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../news/news.page */ "./src/app/pages/news/news.page.ts");
/* harmony import */ var _chat_chat_page__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../chat/chat.page */ "./src/app/pages/chat/chat.page.ts");
/* harmony import */ var _paymentpage_paymentpage_page__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../paymentpage/paymentpage.page */ "./src/app/pages/paymentpage/paymentpage.page.ts");
/* harmony import */ var src_app_pages_accept_accept_page__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! src/app/pages/accept/accept.page */ "./src/app/pages/accept/accept.page.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _tripinfo_tripinfo_page__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! ../tripinfo/tripinfo.page */ "./src/app/pages/tripinfo/tripinfo.page.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! ../../services/auth.service */ "./src/app/services/auth.service.ts");
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ "./node_modules/@ionic-native/call-number/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");






















// import { AcceptPage } from"../../pages/accept/accept.page";






// declare var Stripe;
var HomePage = /** @class */ (function () {
    //OneSignal: any;
    //showLoadRefresh: any;
    function HomePage(vibration, lp, modalCtrl, storage, settings, One, statusBar, loadingCtrl, router, locationTracker, myGcode, dProvider, platform, // tslint:disable-next-line: no-shadowed-variable
    OneSignal, alert, pop, auth, ph, navCtrl, eventProvider, menu, call1, geo) {
        var _this = this;
        this.vibration = vibration;
        this.lp = lp;
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.settings = settings;
        this.One = One;
        this.statusBar = statusBar;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.locationTracker = locationTracker;
        this.myGcode = myGcode;
        this.dProvider = dProvider;
        this.platform = platform;
        this.OneSignal = OneSignal;
        this.alert = alert;
        this.pop = pop;
        this.auth = auth;
        this.ph = ph;
        this.navCtrl = navCtrl;
        this.eventProvider = eventProvider;
        this.menu = menu;
        this.call1 = call1;
        this.geo = geo;
        this.colors = ["#1E90FF", "#FF1493", "#32CD32", "#FF8C00", "#4B0082"];
        this.colorButtons = {};
        this.toggleMore = false;
        this.geocoder = new google.maps.Geocoder();
        this.gpsIson = false;
        this.hideMe = false;
        this.hasArrived = false;
        this.poolRequest = false;
        this.hasAdded_Destination2 = false;
        this.hasPooled = true;
        this.poolEnd = true;
        this.hasDone = true;
        this.isArrived = false;
        this.isBlocked = false;
        this.canLoad = true;
        this.doneK = true;
        this.canDO = true;
        this.returningUser = true;
        this.NotifyTimes = -1;
        this.timerBeforeNotify = 2000;
        this.mapSection = false;
        this.toggleButtons = false;
        this.moreText = "Hide All";
        this.moreIcon = "ios-arrow-up";
        this.canShowOther = false;
        this.hasNotPicked = true;
        this.hasNotPicked2 = true;
        this.hasPooled2 = true;
        this.hideNews = false;
        this.notification = false;
        this.mapFalse = false;
        this.connected = false;
        this.toggleStatus = false;
        // notify_ID: any;
        this.isNotDestinyOption = true;
        this.userHasPhone = false;
        this.hasAdded_Destination = false;
        this.canEnd2 = true;
        this.connect = false;
        this.canEnd = true;
        this.rating = 0;
        this.ratingText = "Cool";
        this.appReady = false;
        this.hasSent = false;
        this.canStopCheck = false;
        this.canStop = false;
        this.isNetwork = true;
        this.greut = true;
        this.called = true;
        this.showGps = false;
        this.hasEnded = true;
        this.called2 = true;
        this.called_2 = true;
        this.itsOver = true;
        this.called_22 = true;
        this.tacktime = true;
        this.dropped = true;
        this.dropped2 = true;
        this.cliet = true;
        this.clientClrCheck = false;
        this.PoolClrCheck = false;
        this.done = true;
        this.hasNotFoundMap = true;
        this.showPassenger2 = false;
        this.c_picked = true;
        this.c_dropped = true;
        this.c_ended = true;
        this.c_added = true;
        this.c_complete = true;
        this.isInBooking = true;
        this.canListen = true;
        this.isPassengerCleared = true;
        this.hasG = true;
        this.isPickedDone = false;
        this.canStart = false;
        this.isUser = true;
        this.rating_positive = 1;
        this.rating_negative = 1;
        this.zoner = false;
        this.canIncur = false;
        this.hasTimed = true;
        this.scope = false;
        this.radius = false;
        this.timeLeft = 120;
        this.Arrined = false;
        this.isPiked = true;
        this.jkl = true;
        this.hour = 1;
        this.hours = 0;
        this.hasPAused = false;
        this.seconds = 1;
        this.minutes = 1;
        this.canBe = true;
        this.currency = "GHC";
        this.currencyIcon = "C";
        this.stripe_key = "pk_test_8tqD66FgCZq0DosnjKmXqdHe00aCCiUfTN";
        this.cardDetails = {};
        this.totalTipped = 0;
        this.subscribed = false;
        this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
        this.platform.ready().then(function () {
            console.log("I AM IN HOMEPAGE");
            _this.goOnline();
            _this.locationTracker.loadMap();
            _this.NowWaitingForID();
            //SUBSCRIBE TO ONE SIGNAL
            _this.subscribeToOnesignal();
        });
    }
    HomePage.prototype.subscribeToOnesignal = function () {
        var _this = this;
        if (!this.platform.is("cordova")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
            this.locationTracker.notifID = this.notify_ID;
            this.OneSignal.notif_id = this.notify_ID;
            console.log("NOTIFY ID IN BROSWER::--" + this.notify_ID);
        }
        else if (this.platform.is("desktop")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
            console.log("NOTFICA IN desktop BROWSER-->>" + this.notify_ID);
        }
        else if (this.platform.is("mobileweb")) {
            this.notify_ID = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
            console.log("NOTFICA IN mobileweb BROWSER-->>" + this.notify_ID);
        }
        else {
            this.One.getIds().then(function (success) {
                _this.notify_ID = success.userId;
                _this.OneSignal.notif_id = success.userId;
                _this.locationTracker.notifID = _this.notify_ID;
                console.log("NOTIFY ID IN CORDOVA:: --" + _this.notify_ID);
            });
        }
    };
    HomePage.prototype.toggle = function () {
        this.menu.enable(true);
        this.menu.open();
    };
    HomePage.prototype.checkTotalEarnings = function () {
        var _this = this;
        var n = [];
        this.pop.showLoader("").then(function () {
            _this.pop.hideLoader();
        });
        this.eventProvider.getEventList().on("value", function (snapshot) {
            _this.eventList = [];
            snapshot.forEach(function (snap) {
                _this.eventList.push({ price: snap.val().realPrice, tip: snap.val().tip, });
                _this.eventList.sort();
                _this.eventList.reverse();
                console.log(_this.eventList);
                return false;
            });
        });
        for (var index = 0; index < this.eventList.length; index++) {
            var element = this.eventList[index];
            // console.log(element.price.replace(/[^\d\.]/g, ''));
            // n.push(this.eventList[index].price || 0);
            // n.push(parseFloat(this.eventList[index].price.replace(/[^\d\.]/g, '')));
            n.push(parseFloat(this.eventList[index].price));
            var add4 = function (a, b) { return a + b; };
            var result4 = n.reduce(add4);
            this.mr = result4.toFixed(2);
            // sum += this.eventList[index].price + element.tip || 0;
        }
        console.log("HOW MUCH MADE::", this.mr);
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.checkTotalEarnings();
        this.StartTracker();
        console.log("LATTITUDE FIRSTD::", this.locationTracker.driver_lat);
        if (this.locationTracker.watch)
            this.locationTracker.watch.unsubscribe();
        if (this.isInBooking) {
            this.platform.ready().then(function () { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var _this = this;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    this.subscribeToOnesignal();
                    console.log("PAGE HAS ENTERED--");
                    if (!this.platform.is("cordova")) {
                        this.locationTracker.driver_lat = 5.7186233;
                        this.locationTracker.driver_lng = -0.0240626;
                        console.log("LATTITUDE SEC::", this.locationTracker.driver_lat);
                    }
                    this.eventProvider.getUserProfiler().on("value", function (userProfileSnapshot) {
                        console.log(userProfileSnapshot.val().userInfo);
                        if (userProfileSnapshot.val().userInfo.rating_positive) {
                            _this.rating_positive = userProfileSnapshot.val().userInfo.rating_positive;
                            _this.eventProvider.getUserProfiler().off("value");
                        }
                        if (userProfileSnapshot.val().userInfo.rating_negative) {
                            _this.rating_negative = userProfileSnapshot.val().userInfo.rating_negative;
                            _this.eventProvider.getUserProfiler().off("value");
                        }
                    });
                    if (this.appReady && this.locationTracker.driver_lat != 0) {
                        console.log(this.settings.current_ID);
                        if (!this.platform.is("cordova")) {
                            if (this.hasEnded) {
                                console.log("Updating car on database.");
                            }
                        }
                        if (this.platform.is("cordova")) {
                            if (this.hasEnded) {
                                this.subscription =
                                    this.platform.backButton.subscribe(function () {
                                        navigator["app"].exitApp();
                                        _this.subscription.unsubscribe();
                                    });
                            }
                            if (this.settings.current_ID) {
                                this.showLoadRefresh().then(function () {
                                    _this.notify_ID = _this.settings.id;
                                    console.log("scheduling activated");
                                });
                            }
                        }
                        else {
                            if (this.settings.current_ID) {
                                this.showLoadRefresh().then(function () {
                                    _this.notify_ID = _this.settings.id;
                                    console.log("scheduling activated");
                                });
                            }
                        }
                    }
                    return [2 /*return*/];
                });
            }); });
        }
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.StartTracker();
        this.subscribeToOnesignal();
        //GET USER PROFILE DETAILS
        this.ph.getUserProfile().on("value", function (userProfileSnapshot) {
            console.log("USER PROFILE:: " + userProfileSnapshot.val());
            _this.emergenc = userProfileSnapshot.val().emergencyNumber;
            _this.first_name = userProfileSnapshot.val().first_name;
            _this.last_name = userProfileSnapshot.val().last_name;
            _this.picture = userProfileSnapshot.val().picture;
            _this.phonenumber = userProfileSnapshot.val().phonenumber;
            _this.unique = userProfileSnapshot.val().unique_number;
            _this.has_subscribed = userProfileSnapshot.val().subscribe;
            console.log("first_name:: " + _this.first_name);
            console.log("picture:: " + _this.picture);
            console.log("EMERGENCY NUM:: " + _this.emergenc);
            console.log("SUBSCRIBEDD????:: " + _this.has_subscribed);
        });
        this.ph.getUserProfile().once("value", function (us) {
            if (us.val().favoriteSeek) {
            }
            if (us.val().isOffline != null)
                if (us.val().isOffline) {
                    _this.isBlocked = true;
                    _this.pop.loggedOff = true;
                    console.log("The user is offline");
                    _this.canTrack = false;
                    _this.pop.presentToast("You Are Offline, Will not recieve any jobs.");
                }
                else {
                    _this.isBlocked = false;
                    _this.pop.loggedOff = false;
                    console.log("The user is online");
                    _this.canTrack = true;
                    // this.starTimer();
                }
            // this.ph.getUserProfile().off("value"); }); 
            _this.NowWaitingForID();
        });
    };
    HomePage.prototype.goBack = function () {
        console.log("Hittine back");
        this.navCtrl.navigateBack("home");
    };
    HomePage.prototype.UpdateCredits = function (number) {
        return this.userProfile.update({
            credits: number,
        });
    };
    HomePage.prototype.checkMe = function (price, surcharge) {
        var _this = this;
        var c = [];
        var n = [];
        console.log(surcharge);
        surcharge.forEach(function (element) {
            _this.riderpaid = parseFloat(price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent 
                if (element.bone == 1) {
                    var nb = element.price / 100;
                    console.log(nb * _this.riderpaid);
                    var fo = nb * _this.riderpaid;
                    n.push(fo);
                    var add2 = function (a, b) { return a + b; };
                    var result2 = n.reduce(add2);
                    _this.percentDriver = result2;
                    console.log((Math.floor(element.price) /
                        100) * _this.riderpaid);
                } //if flat fee 
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    var add4 = function (a, b) { return a + b; };
                    var result4 = c.reduce(add4);
                    _this.flatDriver = result4;
                    console.log(result4);
                }
                _this.totalDriverSurge = _this.flatDriver + _this.percentDriver;
                console.log(_this.totalDriverSurge, _this.flatDriver, _this.percentDriver);
            }
        });
        return this.totalDriverSurge;
    };
    HomePage.prototype.check = function (price, surcharge) {
        var _this = this;
        var c = [];
        var n = [];
        console.log(surcharge);
        surcharge.forEach(function (element) {
            _this.wef = parseFloat(price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    var nb = element.price / 100;
                    console.log(nb * _this.wef);
                    var fo = nb * _this.wef;
                    n.push(fo);
                    var add2 = function (a, b) { return a + b; };
                    var result2 = n.reduce(add2);
                    _this.percent2Driver = result2;
                    console.log((Math.floor(element.price) / 100) * _this.wef);
                }
                //if    flat fee 
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    var add4 = function (a, b) { return a + b; };
                    var result4 = c.reduce(add4);
                    _this.flat2Driver = result4;
                    console.log(result4);
                }
                _this.myDriverSurge = _this.flat2Driver + _this.percent2Driver;
                console.log(_this.myDriverSurge, _this.flatDriver, _this.percentDriver);
            }
        });
        return this.myDriverSurge;
    };
    HomePage.prototype.ionViewDidExit = function () {
        this.pauseTimer();
    };
    HomePage.prototype.RemoveZones = function () {
        var _this = this;
        this.ph.userProfile.child("zone_lat").remove();
        this.ph.userProfile.child("zone_lng").remove();
        this.ph.ActiveState().then(function () {
            _this.pop.showPimp("Restart The App To  Use Normal Mode.");
        });
    };
    HomePage.prototype.starTimer = function () {
        var _this = this;
        this.canBe = true;
        this.ph.getDatabaseProfile().on("value", function (snapshot) {
            if (snapshot.val().timeOn) {
                _this.manTime = snapshot.val().timeOn;
            }
            else {
                _this.manTime = 0;
            }
            _this.ph.getDatabaseProfile().off();
        });
        this.ph.getUserProfile().on("value", function (us) {
            if (us.val().seconds)
                _this.seconds = us.val().seconds;
            if (us.val().minutes)
                _this.minutes =
                    us.val().minutes;
            _this.ph.getUserProfile().off("value");
            if (us.val().elapse)
                _this.noww = us.val().elapse;
            console.log(_this.noww);
        });
        this.myVal = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["interval"])(1000).subscribe(function () {
            var demo = document.getElementById("demo");
            _this.trop = new Date().getHours() - new Date(_this.noww).getHours();
            if (_this.trop >= _this.manTime && _this.canBe) {
                _this.ph.getUserProfile().off("value");
                _this.closEm();
                _this.canBe =
                    false;
                _this.seconds = 1;
                _this.minutes = 0;
                _this.hours = 0;
            }
            if (demo)
                if (_this.seconds) {
                    _this.seconds++;
                    _this.ph.getUserProfile().update({
                        seconds: _this.seconds,
                    });
                    if (_this.seconds >= 60) {
                        _this.minutes++;
                        _this.seconds = 1;
                        console.log(_this.minutes);
                        _this.ph.getUserProfile().update({
                            minutes: _this.minutes,
                        });
                    }
                    if (_this.minutes >= 60) {
                        _this.hours++;
                        _this.minutes = 1;
                        console.log(_this.minutes);
                        _this.ph.getUserProfile().update({
                            hours: _this.hours,
                        });
                    }
                    // Output the result in an element with id="demo"
                    demo.innerHTML = _this.hours + "h : " + _this.minutes + "m : " +
                        _this.seconds + "s ";
                }
        });
    };
    HomePage.prototype.closEm = function () {
        this.ph.getUserProfile().update({
            seconds: 0,
        });
        this.ph.getUserProfile().update({ minutes: 0, });
        this.ph.getUserProfile().update({ hours: 0, });
        this.ph.getUserProfile().update({ elapse: 0, });
    };
    HomePage.prototype.pauseTimer = function () {
        //this.myVal.unsubscribe();
        console.log(this.noww);
        this.noww = new Date();
        this.ph.getUserProfile().update({ elapse: this.noww, });
    };
    HomePage.prototype.anyAlert = function (message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: message, buttons: [{
                                    text: "OK", role: "cancel", handler: function () { },
                                },], backdropDismiss: true,
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.showMe = function (name, id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: name + " Wants To Add You As A Favorite.",
                            buttons: [{
                                    text: "Yes", role: "cancel", handler: function () {
                                        _this.ph.doShit();
                                    },
                                }, {
                                    text: "No", role: "cancel", handler: function () {
                                        _this.ph.RejectFavorite(id);
                                    },
                                },], backdropDismiss: true,
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.showMe3 = function (name) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: "You Were Rated. " + name, buttons: [{
                                    text: "Okay", role: "cancel", handler: function () { _this.ph.doShit3(); },
                                },],
                            backdropDismiss: true,
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.showMe2 = function (name, id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: "You Were Tipped. " +
                                name, buttons: [{
                                    text: "Okay", role: "cancel", handler: function () {
                                        _this.ph.doShit2(id, name);
                                    },
                                },], backdropDismiss: true,
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.toggleMapFalse = function () {
        this.mapFalse = this.mapFalse ?
            false : true;
    };
    HomePage.prototype.openNews = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var mod;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({ component: _news_news_page__WEBPACK_IMPORTED_MODULE_18__["NewsPage"], })];
                    case 1:
                        mod = _a.sent();
                        return [4 /*yield*/, mod.present()];
                    case 2:
                        _a.sent();
                        mod.onDidDismiss().then(function () {
                            _this.hideNews = true;
                            _this.storage.set("News", _this.news);
                        });
                        return [4 /*yield*/, mod.present()];
                    case 3: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HomePage.prototype.togglePassengers = function () {
        this.showPassenger2 = this.showPassenger2 ? false : true;
        console.log("Now Toggling");
    };
    HomePage.prototype.gotoSupport = function () {
        var _this = this;
        this.navCtrl.navigateRoot("Suppor");
        this.platform.backButton.subscribe(function () { return _this.navCtrl.pop(); });
    };
    HomePage.prototype.hoseMe = function () {
        this.hideMe = this.hideMe ? false : true;
    };
    HomePage.prototype.showMoreRating = function () {
        this.pop.showPimp(this.ratingText);
    };
    HomePage.prototype.showMoreMessage = function () {
        this.pop.showPimp(this.message);
    };
    HomePage.prototype.call_phone = function () {
        var customerPhone = this.emergenc;
        window.open("tel:" +
            customerPhone);
    };
    HomePage.prototype.call_phone_other = function () {
        window.open("tel:+233276113371");
        //window.open("tel:" + this.emergenc); 
    };
    HomePage.prototype.gotoScheduled = function () {
        var _this = this;
        this.platform.backButton.subscribe(function () { return _this.navCtrl.pop(); });
        this.router.navigate(["schedule", {
                lat: this.locationTracker.driver_lat, lng: this.locationTracker.driver_lng, notif: this.notify_ID,
            },]);
        // Push your "OtherPage"
    };
    HomePage.prototype.ionViewDidLoad = function () {
        // this.locationTracker.loadMap();
        // this.NowWaitingForID();
        // this.showLoadRefresh().then(() => {
        //   console.log("ABOUT TO LOAD MAP");
        // });
    };
    HomePage.prototype.NowWaitingForID = function () {
        var _this = this;
        var location_locationTracker_loop = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["interval"])(3000).subscribe(function () {
            console.log("WAITING FOR ID:: IF  LOCATION hasShown:: ", _this.locationTracker.hasShown);
            //this.watchPositionSubscription.clearWatch(this.mapTracker)
            if (_this.locationTracker.hasShown) {
                location_locationTracker_loop.unsubscribe();
                console.log("LONGITIUDESHOWIN", _this.locationTracker.driver_lng);
                _this.showGps = false;
                _this.locationTracker.mapLoadComplete = true;
                _this.toggleStatus = true;
                _this.hasNotFoundMap = false;
                // if (!this.isBlocked) {
                //   this.Register();
                // }
            }
            else {
                _this.hasNotFoundMap = false;
                console.log("LOCATIONTRACKER NOT SHOWN");
                _this.NowWaitingForID();
                location_locationTracker_loop.unsubscribe();
            }
        });
    };
    HomePage.prototype.Register = function () {
        var _this = this;
        this.pop.loggedOff = false;
        this.StartTracker();
        //this.CheckLocation()
        if (this.platform.is("cordova")) {
            // this.WaitForNotificationID();
            console.log("NOTIFY ID IN RGISTR::", this.notify_ID);
            this.myListening = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["interval"])(3000).subscribe(function () {
                _this.StartListeningForChanges(_this.notify_ID);
            });
        }
        else {
            this.myListening = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["interval"])(3000).subscribe(function () {
                _this.StartListeningForChanges(_this.notify_ID);
            });
            console.log("waiting.....");
        }
        this.appReady = true;
        this.pop.presentToast("Now Online");
        console.log("connected");
        this.OneSignal.UpdateInfo(this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.ph.carType);
        this.myGcode.Reverse_Geocode(this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.locationTracker.map1, false);
    };
    HomePage.prototype.StartTracker = function () {
        var _this = this;
        console.log("start tracking..........");
        this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(function (position) {
            if (position.coords != undefined) {
                // if (this.locationTracker.marker && this.canTrack) {
                var geoposition = position;
                console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);
                console.log("Location is ready!--location ");
                _this.currLat = geoposition.coords.latitude,
                    _this.currLng = geoposition.coords.longitude;
                _this.OneSignal.UpdateInfo(_this.currLat, _this.currLng, _this.ph.carType);
                // this.locationTracker.marker.remove();
                // }
                // else {
                //   var geoposition = (position as Geoposition);
                //   console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);
                //   this.currLat = geoposition.coords.latitude;
                //   this.currLng = geoposition.coords.longitude;
                //   this.OneSignal.UpdateInfo(this.currLat, this.currLng,
                //     this.ph.carType);
                //   this.locationTracker.map1.moveCamera({
                //     target: new LatLng(this.currLat, this.currLng), zoom: 19, tilt: 0, bearing: 0, duration: 500,
                //   });
                // }
            }
            else {
                var positionError = position;
                console.log('Error ' + positionError.code + ': ' + positionError.message);
                //this.pop.presentToast("Your GPS is off.Please turn it on")
            }
        });
        // this.watchPositionSubscription = navigator.geolocation;
        // this.mapTracker = this.watchPositionSubscription.watchPosition((position) => {
        //   console.log("position callback", position);
        //   // reset route points 
        //   if (this.locationTracker.marker && this.canTrack) {
        //     this.currLat = position.coords.latitude;
        //     this.currLng = position.coords.longitude;
        //     // this.locationTracker.marker.setPosition(new LatLng(this.currLat,
        //     //   this.currLng));
        //     // this.locationTracker.AnimateToLoc(this.currLat, this.currLng);
        //     this.OneSignal.UpdateInfo(this.currLat, this.currLng,
        //       this.ph.carType);
        //     this.locationTracker.map1.moveCamera({
        //       target: new LatLng(this.currLat, this.currLng), zoom: 15, tilt: 0, bearing: 0, duration: 500,
        //     });
        //   }
        //   else {
        //     this.currLat = position.coords.latitude;
        //     this.currLng = position.coords.longitude;
        //     // this.locationTracker.AnimateToLoc(this.currLat, this.currLng);
        //     this.OneSignal.UpdateInfo(this.currLat, this.currLng,
        //       this.ph.carType);
        //   }
        //   console.log("Updating Your Location ....");
        // },
        //   (error) => {
        //     this.pop.presentToast("Your GPS is off.Please turn it on"); console.log(error);
        //   }, { enableHighAccuracy: true, });
    };
    HomePage.prototype.gotoProfile = function () {
        var _this = this;
        this.navCtrl.navigateRoot("proflie");
        this.platform.backButton.subscribe(function () { return _this.navCtrl.pop(); });
    };
    HomePage.prototype.gotoEarnings = function () {
        var _this = this;
        this.navCtrl.navigateRoot("payment");
        this.platform.backButton.subscribe(function () { return _this.navCtrl.pop(); });
    };
    HomePage.prototype.SendMessage = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalCtrl.create({
                            component: _chat_chat_page__WEBPACK_IMPORTED_MODULE_19__["ChatPage"], componentProps: { id: this.notify_ID },
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, modal.onDidDismiss().then(function () {
                                _this.notification
                                    = false;
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.gotoHistory = function () {
        var _this = this;
        this.navCtrl.navigateRoot("history");
        this.platform.backButton.subscribe(function () { return _this.navCtrl.pop(); });
    };
    HomePage.prototype.goOnline = function () {
        // this.pauseTimer(); 
        // if (this.locationTracker.driver_lat != 0) {
        this.Register();
        this.ph.ActiveState();
        // this.pop.loggedOff = false;
        this.pop.presentToast(this.lp.translate()[0].e5);
        this.starTimer();
        this.pop.loggedOff = false;
        this.pop.presentToast(this.lp.translate()[0].e5);
        this.OneSignal.UpdateInfo(this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.ph.carType);
        this.canTrack = true;
        // } else {
        //   this.pop.alertMe("No latitude and Longitude On Your Device. Are You running on an emulator ? ");
        // }
    };
    HomePage.prototype.goOffline = function () {
        this.canTrack = false;
        this.pop.GoOffline();
        // this.watchPositionSubscription.clearWatch(this.mapTracker);
        this.watch.unsubscribe();
        this.pop.loggedOff = true;
        this.ph.InActiveState();
        this.pauseTimer();
    };
    HomePage.prototype.HasArrived = function () {
        this.pauseTimer();
        if (this.platform.is("cordova")) {
            this.sendPush(this.customerID, "Driver has arrived");
        }
        else if (this.platform.is("desktop")) {
            this.pop.presentToast("Driver has arrived");
            console.log("NOTIFCATION IN DESKTOP");
        }
        else if (this.platform.is("mobileweb")) {
            this.pop.presentToast("Driver has arrived");
            console.log("NOTIFCATION IN mobileweb");
        }
        this.isArrived = true;
        this.eventProvider.ApprovedArrival(true, this.notify_ID);
        this.startTimer();
        this.isPiked = false;
        this.Arrined = true;
        var currentdate = new Date();
        var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") + "-" + this.paddNumber(currentdate.getDate(), "00") + "-" + currentdate.getFullYear() + " @ " + this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") + ":" + ("0" + currentdate.getMinutes()).slice(-2) + " " + (currentdate.getHours() > 11 ? "PM" : "AM");
        this.pickArrived = datetime;
    };
    HomePage.prototype.startTimer = function () {
        var _this = this;
        this.interval = setInterval(function () {
            if (_this.timeLeft > 0) {
                _this.timeLeft--;
                _this.timeOver = false;
            }
            else {
                clearInterval(_this.interval);
                _this.timeOver = true;
            }
        }, 1000);
    };
    HomePage.prototype.moonTimer = function () {
        clearInterval(this.interval);
    };
    HomePage.prototype.OpenInfo = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var obj, myAlert2;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        obj = {
                            info: this.bull, accepted: this.pickArrived, arrived: this.isDrop,
                            distance: this.dProvider.distance2,
                        };
                        return [4 /*yield*/, this.modalCtrl.create({ component: _tripinfo_tripinfo_page__WEBPACK_IMPORTED_MODULE_23__["TripinfoPage"], componentProps: obj, })];
                    case 1:
                        myAlert2 = _a.sent();
                        return [4 /*yield*/, myAlert2.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, myAlert2.onDidDismiss().then(function (data) {
                                console.log("DAT FROM MODAL", data); // if (data && this.canIncur)
                                if (data.data == null) { //this.onChange(data); //this.ReturnHome(); 
                                }
                                else {
                                    _this.onChange(data); // this.ReturnHome(); // this.onChanger(data);
                                }
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.ReturnHome = function () {
        this.platform.backButton.subscribe(function () {
            navigator["app"].exitApp();
        });
    };
    HomePage.prototype.call = function () {
        console.log("CALl Customer Phone --->>" + this.customerPhone);
        window.open("tel:" +
            this.customerPhone);
    };
    HomePage.prototype.clearConnection = function () {
        var _this = this;
        console.log(this.notify_ID, 'Has Been Cleared Already....');
        var customer = firebase__WEBPACK_IMPORTED_MODULE_5___default.a.database().ref("Customer/" + this.notify_ID);
        customer.remove().then(function () {
            _this.pop.hasPicked = false;
            _this.hasNotPicked = true;
            _this.isPickedDone = false;
        });
    };
    // showHelp(){
    // this.pop.alertMe('PLEASE MAKE SURE YOU HAVE GOOGLE MAPS INSTALLED ON
    // YOUR DEVICE.Click on start navigation, to drive to client. ') // }
    HomePage.prototype.onChanger = function (e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: 'A charge Of $5 Will be Incurred',
                            buttons: [{
                                    text: 'Okay',
                                    role: 'cancel',
                                    handler: function () {
                                        var charge = 5;
                                        var r;
                                        _this.ph.getUserProfil().child(_this.ph.id).on('child_added', function (userProfileSnapshot) {
                                            r = userProfileSnapshot.val().credits;
                                            console.log(r);
                                        });
                                        var cred;
                                        _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').on('value', function (snapshot) {
                                            cred = snapshot.val().credits;
                                        });
                                        _this.HideFunk();
                                        var yu = e;
                                        var currentdate = new Date();
                                        var datetime = (_this.paddNumber(currentdate.getMonth() + 1, "00")) +
                                            "-" +
                                            (_this.paddNumber(currentdate.getDate(), "00")) +
                                            "-" +
                                            currentdate.getFullYear() +
                                            " @ " +
                                            _this.paddNumber(((currentdate.getHours() + 24) % 12 || 12), "00") +
                                            ":" +
                                            ('0' + currentdate.getMinutes()).slice(-2) +
                                            " " +
                                            (currentdate.getHours() > 11 ? 'PM' : 'AM');
                                        if (_this.ph.name && datetime && _this.myGcode.locationName && _this.destination && _this.dProvider.price && yu)
                                            _this.ph.Cancelled(_this.client_Name, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () {
                                                _this.timeOver = false;
                                                var m;
                                                if (r) {
                                                    var h = r;
                                                    m = h - 5;
                                                    _this.ph.UpdateCredits(m);
                                                }
                                                else {
                                                    _this.ph.UpdateCredits(-5);
                                                }
                                                var g;
                                                if (cred) {
                                                    g = cred + 5;
                                                    console.log(cred);
                                                    _this.ph.UpdateRiderCredits(g, _this.customeID);
                                                    _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').off('value');
                                                }
                                                else {
                                                    _this.ph.UpdateRiderCredits(5, _this.customeID);
                                                    _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').off('value');
                                                }
                                                _this.ph.CancelledMe2(_this.data.Client_username, _this.customeID, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () { });
                                                _this.ph.CancelledMe(_this.data.Client_username, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () { });
                                                _this.ph.getCancelledProfile().child('Cancelled/documents').on('value', function (snapshot) {
                                                    _this.items = [];
                                                    console.log(snapshot.val());
                                                    _this.currentCar = null;
                                                    snapshot.forEach(function (snap) {
                                                        console.log(snap.val());
                                                        if (snap.val().type == 'Driver')
                                                            _this.items.push({
                                                                key: snap.key,
                                                                text: snap.val().title,
                                                                type: snap.val().type,
                                                                status: snap.val().status
                                                            });
                                                        console.log(snap.val());
                                                        return false;
                                                    });
                                                });
                                            });
                                        console.log(_this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.dProvider.price, yu);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                    }
                                },
                            ],
                            backdropDismiss: false
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.RiderNoSHow = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            message: 'Are you sure you want to cancel?',
                            buttons: [{
                                    text: 'Okay',
                                    role: 'cancel',
                                    handler: function () {
                                        var charge = 5;
                                        var r;
                                        _this.ph.getUserProfil().child(_this.ph.id).on('child_added', function (userProfileSnapshot) {
                                            r = userProfileSnapshot.val().credits;
                                            console.log(r);
                                        });
                                        var cred;
                                        _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').on('value', function (snapshot) {
                                            cred = snapshot.val().credits;
                                        });
                                        _this.HideFunk();
                                        var yu = 'Rider No Show';
                                        var currentdate = new Date();
                                        var datetime = (_this.paddNumber(currentdate.getMonth() + 1, "00")) +
                                            "-" +
                                            (_this.paddNumber(currentdate.getDate(), "00")) +
                                            "-" +
                                            currentdate.getFullYear() +
                                            " @ " +
                                            _this.paddNumber(((currentdate.getHours() + 24) % 12 || 12), "00") +
                                            ":" +
                                            ('0' + currentdate.getMinutes()).slice(-2) +
                                            " " +
                                            (currentdate.getHours() > 11 ? 'PM' : 'AM');
                                        if (_this.ph.name && datetime && _this.myGcode.locationName && _this.destination && _this.dProvider.price && yu)
                                            _this.ph.Cancelled(_this.data.Client_username, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () {
                                                _this.timeOver = false;
                                                var m;
                                                if (r) {
                                                    var h = r;
                                                    m = h + 5;
                                                    _this.ph.UpdateCredits(m);
                                                }
                                                else {
                                                    _this.ph.UpdateCredits(5);
                                                }
                                                var g;
                                                if (cred) {
                                                    g = cred - 5;
                                                    console.log(cred);
                                                    _this.ph.UpdateRiderCredits(g, _this.customeID);
                                                    _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').off('value');
                                                }
                                                else {
                                                    _this.ph.UpdateRiderCredits(-5, _this.customeID);
                                                    _this.ph.getOtherProfile().child(_this.customeID).child('userInfo').off('value');
                                                }
                                                _this.ph.CancelledMe2(_this.data.Client_username, _this.customeID, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () { });
                                                _this.ph.CancelledMe(_this.data.Client_username, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () { });
                                                _this.ph.getCancelledProfile().child('Cancelled/documents').on('value', function (snapshot) {
                                                    _this.items = [];
                                                    console.log(snapshot.val());
                                                    _this.currentCar = null;
                                                    snapshot.forEach(function (snap) {
                                                        console.log(snap.val());
                                                        if (snap.val().type == 'Driver')
                                                            _this.items.push({
                                                                key: snap.key,
                                                                text: snap.val().title,
                                                                type: snap.val().type,
                                                                status: snap.val().status
                                                            });
                                                        console.log(snap.val());
                                                        return false;
                                                    });
                                                });
                                            });
                                        console.log(_this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.dProvider.price, yu);
                                    }
                                },
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    handler: function () {
                                    }
                                },
                            ],
                            backdropDismiss: false
                        })];
                    case 1:
                        alert = _a.sent();
                        alert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.paddNumber = function (number, paddingValue) {
        return String(paddingValue +
            number).slice(-paddingValue.length);
    };
    HomePage.prototype.onChange = function (e) {
        var _this = this;
        this.HideFunk();
        var yu = "Wrong Trip";
        var charge = 0;
        var currentdate = new Date();
        var datetime = (this.paddNumber(currentdate.getMonth() + 1, "00")) +
            "-" +
            (this.paddNumber(currentdate.getDate(), "00")) +
            "-" +
            currentdate.getFullYear() +
            " @ " +
            this.paddNumber(((currentdate.getHours() + 24) % 12 || 12), "00") +
            ":" +
            ('0' + currentdate.getMinutes()).slice(-2) +
            " " +
            (currentdate.getHours() > 11 ? 'PM' : 'AM');
        if (this.ph.name && datetime && this.myGcode.locationName && this.destination && this.dProvider.price && yu)
            this.ph.Cancelled(this.data.Client_username, this.ph.name, datetime, this.myGcode.locationName, this.destination, this.ridePrice, yu, charge).then(function () {
                // this.ph.UpdateCredits(5);
                _this.ph.CancelledMe(_this.data.Client_username, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () {
                    _this.ph.CancelledMe2(_this.data.Client_username, _this.customeID, _this.ph.name, datetime, _this.myGcode.locationName, _this.destination, _this.ridePrice, yu, charge).then(function () { });
                });
                _this.ph.getCancelledProfile().child('Cancelled/documents').on('value', function (snapshot) {
                    _this.items = [];
                    console.log(snapshot.val());
                    _this.currentCar = null;
                    snapshot.forEach(function (snap) {
                        console.log(snap.val());
                        if (snap.val().type == 'Driver')
                            _this.items.push({
                                key: snap.key,
                                text: snap.val().title,
                                type: snap.val().type,
                                status: snap.val().status
                            });
                        console.log(snap.val());
                        return false;
                    });
                });
            });
        console.log(this.ph.name, datetime, this.myGcode.locationName, this.destination, this.dProvider.price, yu);
    };
    HomePage.prototype.Arrived = function () {
        if (this.platform.is("cordova")) {
            this.sendPush(this.customerID, "Driver has arrived");
        }
        else if (this.platform.is("desktop")) {
            this.pop.presentToast("Driver has arrived");
            console.log("NOTIFCATION IN DESKTOP");
        }
        else if (this.platform.is("mobileweb")) {
            this.pop.presentToast("Driver has arrived");
            console.log("NOTIFCATION IN mobileweb");
        }
        this.hasArrived = false;
        this.eventProvider.ApprovedArrival(true, this.notify_ID);
    };
    HomePage.prototype.sendPush = function (id, message) {
        var _this = this;
        var notificationObj = {
            include_player_ids: [id], contents: {
                en: message
            },
        };
        this.One.postNotification(notificationObj).then(function (good) {
            console.log("NOTIFICATIONS SENT");
        }, function (error) {
            console.log("ERROR SENDING NOTIFICATIONS " + error);
            _this.pop.presentToast("Push notification failed");
        });
    };
    HomePage.prototype.showFilters = function () {
        this.isNotDestinyOption = this.isNotDestinyOption ? false : true;
        this.scope = false;
        this.radius = false;
    };
    HomePage.prototype.Scope = function () {
        var _this = this;
        this.scope = true;
        var polyOptions = {
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true
        };
        // Creates a drawing manager attached to the map that allows the user to draw
        // markers, lines, and shapes.
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['polygon']
            },
            markerOptions: {
                draggable: true
            },
            polylineOptions: {
                editable: true
            },
            polygonOptions: polyOptions,
            map: this.thatmap
        });
        google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (e) {
            if (e.type != google.maps.drawing.OverlayType.MARKER) {
                // Switch back to non-drawing mode after drawing a shape.
                _this.drawingManager.setDrawingMode(null);
            }
        });
    };
    HomePage.prototype.Radius = function () {
        var _this = this;
        this.radius = true;
        var polyOptions = {
            strokeWeight: 0,
            fillOpacity: 0.45,
            editable: true
        };
        // Creates a drawing manager attached to the map that allows the user to draw
        // markers, lines, and shapes.
        this.drawingManager = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.CIRCLE,
            drawingControlOptions: {
                position: google.maps.ControlPosition.TOP_CENTER,
                drawingModes: ['circle']
            },
            markerOptions: {
                draggable: true
            },
            polylineOptions: {
                editable: true
            },
            // rectangleOptions: polyOptions,
            // circleOptions: polyOptions,
            polygonOptions: polyOptions,
            map: this.thatmap
        });
        google.maps.event.addListener(this.drawingManager, 'overlaycomplete', function (e) {
            if (e.type != google.maps.drawing.OverlayType.MARKER) {
                // Switch back to non-drawing mode after drawing a shape.
                _this.drawingManager.setDrawingMode(null);
            }
        });
    };
    HomePage.prototype.Pickup = function () {
        var _this = this;
        this.locationTracker.isPickedUp = false;
        this.eventProvider.ApprovePickup(true, this.notify_ID);
        //this.locationTracker.watchPositionSubscription.clearWatch(this.locationTracker.goto_passenger_watcher);
        this.pop.hasPicked = true;
        this.isPickedDone = true;
        this.moonTimer();
        // this.hasNotPicked = false;
        if (this.platform.is("cordova")) {
            this.sendPush(this.customerID, "Driver has started trip");
        }
        else if (this.platform.is("desktop")) {
            this.pop.presentToast("Driver has started trip");
            console.log("NOTIFCATION IN DESKTOP");
        }
        else if (this.platform.is("mobileweb")) {
            this.pop.presentToast("Driver has started trip");
            console.log("NOTIFCATION IN mobileweb");
        }
        // this.locationTracker.hasTransactionEnded = false
        // clearTimeout(this.locationTracker.tracking_timeout)
        this.locationTracker.ClearDetection = true;
        //clearInterval(this.locationTracker.detectCarChange)
        this.myGcode.Reverse_Geocode(this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.locationTracker.map1, false);
        //this.locationTracker.ResetMe()
        // this.pop.showLoader('fdgtedhhgdhd')
        if (this.platform.is('cordova')) {
            console.log('this detiny ' + this.destination);
            this.geocoder.geocode({ 'address': this.destination }, function (results, status) {
                if (status == 'OK') {
                    var position = results[0].geometry.location;
                    var lat = position.lat();
                    var lng = position.lng();
                    var urPos = new google.maps.LatLng(lat, lng);
                    var uLOC = new google.maps.LatLng(_this.locationTracker.driver_lat, _this.locationTracker.driver_lng);
                    console.log(lat, lng);
                    _this.showLoadRefresh();
                    _this.locationTracker.setMarkersDestination(lat, lng, _this.notify_ID);
                    _this.dProvider.calcDestRoute(_this.name, uLOC, urPos, _this.destination, _this.notify_ID);
                }
            });
        }
        else {
            var urPos = new google.maps.LatLng(5.4975394, 7.5029374);
            var uLOC = new google.maps.LatLng(this.locationTracker.driver_lat, this.locationTracker.driver_lng);
            this.dProvider.calcDestRoute(this.name, uLOC, urPos, this.destination, this.notify_ID);
            // this.pop.hideLoader();
        }
    };
    HomePage.prototype.Drop = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var urPos, uLOC, latlng, obj, payme;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(this.data);
                        console.log(this.locationTracker.myLat, this.locationTracker.myLng);
                        urPos = new google.maps.LatLng(this.locationTracker.myLat, this.locationTracker.myLng);
                        uLOC = new google.maps.LatLng(this.locationTracker.driver_lat, this.locationTracker.driver_lng);
                        if (this.platform.is("cordova")) {
                            console.log("this detiny " + this.destination);
                            latlng = {
                                lat: this.locationTracker.myLat,
                                lng: this.locationTracker.myLng,
                            };
                            this.geocoder.geocode({ location: latlng }, function (results, status) {
                                if (status === "OK") {
                                    if (results[0]) {
                                        var desty = results[0].formatted_address;
                                        _this.dProvider.calcPrice(uLOC, urPos, desty, _this.notify_ID);
                                    }
                                    else { }
                                }
                                else {
                                    // window.alert('Geocoderfailed due to: ' + status);
                                }
                            });
                        }
                        obj = {
                            amount: this.data.Client_price,
                            payment_method: 0
                        };
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: _paymentpage_paymentpage_page__WEBPACK_IMPORTED_MODULE_20__["PaymentpagePage"], componentProps: obj,
                            })];
                    case 1:
                        payme = _a.sent();
                        return [4 /*yield*/, payme.present()];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, payme.onDidDismiss().then(function () {
                                _this.Finalize();
                                _this.pop.showLoader("Please Wait...");
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.HideFunk = function () {
        this.canListen = true;
        this.c_dropped = true;
        this.c_picked = true;
        this.c_ended = true;
        this.c_added = true;
        this.hasTimed = true;
        console.log('hide funk called.........', this.notify_ID);
        this.locationTracker.isPickedUp = false;
        this.locationTracker.isDropped = false;
        if (this.locationTracker.location_tracker_loop)
            this.locationTracker.location_tracker_loop.unsubscribe();
        if (this.locationTracker.destination_tracker_loop)
            this.locationTracker.destination_tracker_loop.unsubscribe();
        this.ph.getUserAsClientInfo(this.notify_ID).off('child_removed');
        this.ph.getUserAsClientInfo(this.notify_ID).off('child_changed');
        this.ph.getUserAsClientInfo(this.notify_ID).off('child_added');
        this.ph.getUserAsClientInfo(this.notify_ID).off('value');
        this.locationTracker.hasCompleted = true;
        this.c_complete = true;
        this.clearConnection();
        this.clearAllNow();
    };
    HomePage.prototype.clearAllNow = function () {
        var _this = this;
        console.log("CALLIN CLEAR NOW");
        this.hasArrived = true;
        this.locationTracker.routeNumber = 0;
        this.timeLeft = 120;
        this.hour = 1;
        this.locationTracker.ClearDetection = true;
        // this.pop.GoOffline2();
        this.jkl = true;
        this.locationTracker.isDestination = false;
        this.canIncur = false;
        this.timeOver = false;
        this.isArrived = false;
        if (this.platform.is("cordova")) {
            // this.locationTracker.map1.clear().then(() => {
            //   console.log("all markers cleared");
            // });
        }
        //clearInterval(this.locationTracker.detectCarChange)
        console.log(this.name);
        this.hasPooled2 = true;
        this.hasPooled = true;
        this.clientClrCheck = false;
        this.PoolClrCheck = false;
        this.canEnd = true;
        this.canEnd2 = true;
        this.isInBooking = true;
        this.hasDone = true;
        this.hasEnded = true;
        this.canShowOther = false;
        this.itsOver = false;
        this.locationTracker.driver = false;
        this.OneSignal.isInDeepSh_t = false;
        if (this.platform.is("cordova")) {
            this.locationTracker.map1.setClickable(true);
        }
        this.hasNotPicked = true;
        clearTimeout(this.locationTracker.tracking_timeout);
        this.locationTracker.canTrack_ = false;
        this.locationTracker.hasTransactionEnded = false;
        this.pop.hasPicked = false;
        console.log("database has been wiped");
        this.ph.getUserProfile().once("value", function (userProfileSnapshot) {
            _this.earnings = Math.floor(userProfileSnapshot.val().earnings);
        });
        this.isArrived = false;
        this.locationTracker.hasCompleted = true;
        this.platform.ready().then(function () {
            console.log("called once.............");
            Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["timer"])(1000).subscribe(function () {
                console.log("HITTING RESTART");
                _this.StartListeningForChanges(_this.notify_ID);
                _this.StartTracker();
                _this.locationTracker.refreshForChangesRemove();
                _this.isUser = true;
                _this.isPassengerCleared = true;
            });
            _this.subscription = _this.platform.backButton.subscribe(function () {
                navigator["app"].exitApp();
            });
            _this.subscription.unsubscribe();
            _this.OneSignal.UpdateInfo(_this.locationTracker.driver_lat, _this.locationTracker.driver_lng, _this.ph.carType);
        });
    };
    HomePage.prototype.hasCompletedRide = function (driverSnap) {
        var _this = this;
        if (this.c_complete) {
            this.showLoadRefresh();
            this.ph.getUserProfile()
                .child("favorite").on("value", function (us) {
                console.log(us.val());
                if (us.val())
                    if (us.val().favoriteSeek) {
                    }
                _this.ph.getUserProfile().child("favorite").off("value");
            });
            this.ph
                .getUserProfile().child("tipper").on("value", function (us) {
                if (us.val())
                    if (us.val().tipped != null) {
                        _this.totalTipped = us.val().tipped;
                        console.log(_this.totalTipped);
                        if (_this.eventProvider.historyKey != null) {
                            _this.eventProvider.UpdateTip(_this.totalTipped).then(function () {
                                _this.ph.getUserProfile().child("tipper").remove();
                                //this.pop.presentToast('You Were Tipped.') 
                                //this.ph.getUserProfile().child('tipper').off('value');
                            });
                        }
                    }
            });
            this.ph
                .getUserProfile().child("rater").on("value", function (us) {
                console.log(us.val());
                if (us.val())
                    if (us.val().rating != null) {
                        _this.ph.getUserProfile().child("rater").remove();
                        //this.showMe3(us.val().rate); // this.pop.presentToast('You were Tipped')
                    }
                _this.ph.getUserProfile().child("rater").off("value");
            }); // this.pop.loading.dismiss(); 
            var currentdate = new Date();
            var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") + "-" +
                this.paddNumber(currentdate.getDate(), "00") + "-" +
                currentdate.getFullYear() + " @ " +
                this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") + ":" +
                ("0" + currentdate.getMinutes()).slice(-2) + " " + (currentdate.getHours()
                > 11 ? "PM" : "AM");
            // this.showLoadRefresh()
            var date_result = "";
            var d = new Date();
            date_result += d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
            date_result;
            this.ph
                .UpdateEarnings(driverSnap.Client_realPrice + this.totalTipped || 0)
                .then(function () {
                var toll = driverSnap.Client_toll || [];
                _this.eventProvider
                    .createHistory(driverSnap.Client_realPrice, driverSnap.Client_username, driverSnap.Client_price, datetime, driverSnap.Client_locationName, driverSnap.Client_destinationName, date_result, driverSnap.Client_Surcharges || 0, toll, driverSnap.Client_OutOfStateCharge, driverSnap.Client_ID, _this.totalTipped).then(function () {
                    _this.router.navigate(["rate", {
                            eventId: driverSnap.Client_ID, name: driverSnap.Client_username, lat: _this.locationTracker.driver_lat, lng: _this.locationTracker.driver_lng, cartype: _this.ph.carType, positive_Rating: driverSnap.Client_Positive_rating,
                            negative_Rating: driverSnap.Client_Negative_rating, price: driverSnap.Client_price - driverSnap.Client_DriverSurharge, time: _this.dProvider.time2, distance: _this.dProvider.distance,
                        },]).then(function () {
                        _this.locationTracker.map1.clear().then(function () {
                            console.log("CLEARED MAP AFTER RATING....");
                        });
                        // this.notify_ID = null;
                        _this.HideFunk();
                        _this.ph.getUserProfile().child("tipper").remove();
                        _this.Arrined = false;
                        //this.isPiked = true;
                        _this.timeOver = false;
                        //this.locationTracker.watchPositionSubscription.clearWatch(this.locationTracker.goto_passenger_watcher)
                    });
                });
            });
            this.c_complete = false;
        }
    };
    HomePage.prototype.UserChanged = function (driverSnap) {
        if (driverSnap.Client_hasPaid) {
            console.log("Passenger 1 has paid");
            this.pop.hideLoader();
        }
        if (driverSnap.Pool_ended) {
            this.pop.presentToast("Passenger Has Cancelled Request");
        }
        if (driverSnap.Client_PickedUp && this.c_picked) {
            this.c_picked = false;
            this.pop.hasPicked = true;
            console.log("picked up passenger 1");
        }
    };
    HomePage.prototype.UserRemoved = function () {
        this.showLoadRefresh();
        this.HideFunk();
        console.log("Stopped The movement of driver towards user");
    };
    HomePage.prototype.showLoadRefresh = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingCtrl.create({})];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present().then(function () {
                                var myTimeout = setTimeout(function () {
                                    clearTimeout(myTimeout);
                                    loading.dismiss();
                                }, 300);
                            })];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.NewClientFound = function (client_locationName, location, client_name, picture, destinationName, rate, ratingText, price) {
        this.name = client_name;
        this.picture = picture;
        this.location = client_locationName;
        this.destination = destinationName;
        this.rating = rate;
        this.ratingText = ratingText;
        this.ridePrice = price;
        this.isInBooking = false;
        //this.locationTracker.setMarkers(location) 
        //this.pop.GoOffline2();  //- was showing offline but took it off
        //this.canShowOther = true;
        var urPos = new google.maps.LatLng(location[0], location[1]);
        var uLOC = new google.maps.LatLng(this.locationTracker.driver_lat, this.locationTracker.driver_lng);
        // console.log(price)
        this.userLat = location[0];
        this.userLng = location[1];
        this.locationTracker.userLat = location[0];
        this.locationTracker.userLng = location[1];
        this.locationTracker.D_lat = location[0];
        this.locationTracker.D_lat = location[1];
        this.showLoadRefresh();
        console.log("RUNING CALROUTE HERE -- IN HOME.ts");
        this.dProvider.calcRoute(this.name, uLOC, urPos, this.location, false);
        console.log("END--RUNING CALROUTE HERE -- IN HOME.ts");
        this.myGcode.Reverse_Geocode(this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.locationTracker.map1, false);
        console.log(location);
        console.log("Set Marker func called");
    };
    HomePage.prototype.gotoMap = function () {
        if (this.platform.is("android"))
            console.log("MAP IN ANDROID");
        var destination = this.locationTracker.driver_lat + "," + this.locationTracker.driver_lng;
        window.open("https://www.google.com/maps/search/?api=1&query=" + destination, '_system');
        if (this.platform.is("ios"))
            console.log("MAP IN IOS");
        var destination1 = this.locationTracker.driver_lat + "," + this.locationTracker.driver_lng;
        window.open("https://www.google.com/maps/search/?api=1&query=" + destination1, '_system');
        if (this.platform.is("mobileweb"))
            console.log("MAP IN MOBILE");
        var destination2 = this.locationTracker.driver_lat + "," +
            this.locationTracker.driver_lng;
        window.open("https://www.google.com/maps/search/?api=1&query=" + destination2, '_system');
    };
    HomePage.prototype.gotoDestination = function () {
        var _this = this;
        this.pop.showLoader(this.lp.translate()[0].e10);
        console.log("the result..........");
        this.geocoder.geocode({
            address: this.destination
        }, function (results, status) {
            if (status == "OK") {
                var position = results[0].geometry.location;
                var lat = position.lat();
                var lng = position.lng();
                console.log("This is the result..........");
                console.log("DESTINATION LAT && LNG.........." + lat, lng);
                // this.locationTracker.driver_lat = 5.614818
                // this.locationTracker.driver_lng = - 0.205874,
                _this.pop.hideLoader();
                if (_this.platform.is("android")) {
                    var destination = _this.locationTracker.driver_lat + "," + _this.locationTracker.driver_lng;
                    window.open("https://www.google.com/maps/search/?api=1&query=" + destination, '_system');
                }
                if (_this.platform.is("ios")) {
                    var destination = _this.locationTracker.driver_lat + "," + _this.locationTracker.driver_lng;
                    window.open("https://www.google.com/maps/search/?api=1&query=" + destination, '_system');
                }
                if (_this.platform.is("mobileweb")) {
                    var destination = _this.locationTracker.driver_lat + "," +
                        _this.locationTracker.driver_lng;
                    window.open("https://www.google.com/maps/search/?api=1&query=" + destination, '_system');
                }
            }
            else {
                _this.pop.showPimp(_this.lp.translate()[0].f1);
            }
        });
    };
    HomePage.prototype.HideFunc = function () {
        this.canListen = true;
        this.c_dropped = true;
        this.c_picked = true;
        this.c_ended = true;
        this.c_added = true;
        this.c_complete = true;
    };
    HomePage.prototype.RejectJob = function () {
        var _this = this;
        this.canStart = false;
        this.hasEnded = true;
        this.isPassengerCleared = false;
        console.log("IT HAS ENDEDNSHOULD BE TRUE", this.hasEnded);
        this.platform.ready().then(function () {
            if (_this.notify_ID)
                _this.ph.getUserAsClientInfo(_this.notify_ID).remove();
            Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["timer"])(1200).subscribe(function () {
                //this.StartListeningForChanges(this.notify_ID);
                _this.canStart = true;
                _this.jkl = true;
                // this.starTimer();
            });
            _this.OneSignal.UpdateInfo(_this.locationTracker.driver_lat, _this.locationTracker.driver_lng, _this.ph.carType);
        });
        if (this.myAlert) {
            this.myAlert.dismiss();
        }
    };
    HomePage.prototype.AcceptJob = function () {
        var _this = this;
        this.called = true;
        this.called_2 = true;
        //this.watchPositionSubscription.clearWatch(this.mapTracker);
        this.watch.unsubscribe();
        this.dropped2 = true;
        this.done = true;
        this.connected = true;
        console.log("driver loc is empty");
        if (this.driverLoc != null) {
            console.log("driver loc is filled");
            this.locationTracker.hasTransactionEnded = true;
            this.locationTracker.canTrack_ = true;
            this.platform.ready().then(function () {
                _this.platform.backButton.subscribe(function () { return _this.pop.presentToast(_this.lp.translate()[0].e4); });
            });
            console.log('Stopped Normal Movement Of Driver');
            this.OneSignal.isInDeepSh_t = true;
            console.log('removed now');
            if (!this.ph.rating) {
                this.ph.rateText = "";
                this.ph.rating = 0;
            }
            if (!this.platform.is("cordova")) {
                console.log("removed now" + this.ph.name, this.ph.picture, this.notify_ID, 5.4975394, 7.5029374, this.myGcode.locationName, this.ph.plate, this.ph.seat, this.ph.carType, this.ph.rating, this.phonenumber);
                this.createUserInformation(this.ph.name, this.ph.picture, this.notify_ID, 5.4975394, 7.5029374, this.myGcode.locationName, this.ph.plate, this.ph.seat, this.ph.carType, this.rating_positive, this.rating_negative, this.ph.rateText, this.phonenumber);
            }
            else {
                console.log("removed now"
                    + this.ph.name, this.ph.picture, this.notify_ID, this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.myGcode.locationName, this.ph.plate, this.ph.seat, this.ph.carType, this.ph.rating, this.phonenumber);
                this.createUserInformation(this.ph.name, this.ph.picture, this.notify_ID, this.locationTracker.driver_lat, this.locationTracker.driver_lng, this.myGcode.locationName, this.ph.plate, this.ph.seat, this.ph.carType, this.rating_positive, this.rating_negative, this.ph.rateText, this.phonenumber);
            }
            console.log("Accept Job");
        }
    };
    HomePage.prototype.createUserInformation = function (name, picture, id, lat, lng, locationName, plate, seats, carType, positive, negative, rateText, number) {
        var _this = this;
        // console.log(this.notify_ID)
        this.eventProvider
            .PushUserDetails(name, picture, id, lat, lng, locationName, plate, seats, carType, positive, negative, rateText, number, this.notify_ID).then(function () {
            _this.locationTracker.map1.clear().then(function () {
                console.log("SetMarkers Called.............");
                _this.showLoadRefresh();
                //commenting all remove out
                _this.locationTracker.hasCompleted = false;
                _this.locationTracker.setMarkers(_this.notify_ID, _this.passenger_Lat, _this.passenger_Lng);
            });
        });
    };
    HomePage.prototype.Finalize = function () {
        var _this = this;
        if (this.platform.is("cordova")) {
            this.sendPush(this.customerID, "Trip Ended");
        }
        else if (this.platform.is("desktop")) {
            this.pop.presentToast("Trip Ended");
            console.log("NOTIFCATION IN DESKTOP");
        }
        else if (this.platform.is("mobileweb")) {
            this.pop.presentToast("Trip Ended");
            console.log("NOTIFCATION IN mobileweb");
        }
        clearTimeout(this.locationTracker.tracking_timeout);
        this.locationTracker.canTrack_ = false;
        this.locationTracker.hasTransactionEnded = false;
        this.locationTracker.hasCompleted = true;
        this.eventProvider.ApproveDrop(true, this.notify_ID).then(function () {
            _this.isPassengerCleared = false;
            _this.c_dropped = false;
            _this.pop.hideLoader();
            // this.locationTracker.watchPositionSubscription.clearWatch(this.locationTracker.goto_passenger_watcher)
            _this.watch.unsubscribe();
            _this.ph.getUserAsClientInfo(_this.notify_ID).off("child_removed");
            _this.ph.getUserAsClientInfo(_this.notify_ID).off("child_changed");
            _this.ph.getUserAsClientInfo(_this.notify_ID).off("child_added");
            console.log(_this.locationTracker.goto_passenger_watcher);
            _this.hasCompletedRide(_this.data); // this.clearDuty();
            _this.locationTracker.hasTransactionEnded = false;
            _this.OneSignal.isInDeepSh_t = false;
            _this.c_ended = false;
            _this.canEnd = false;
        });
    };
    HomePage.prototype.ConfirmDrop = function () {
        this.Drop();
        var currentdate = new Date();
        var datetime = this.paddNumber(currentdate.getMonth() + 1, "00") + "-" +
            this.paddNumber(currentdate.getDate(), "00") + "-" +
            currentdate.getFullYear() + " @ " +
            this.paddNumber((currentdate.getHours() + 24) % 12 || 12, "00") + ":" +
            ("0" + currentdate.getMinutes()).slice(-2) + " " + (currentdate.getHours()
            > 11 ? "PM" : "AM");
        this.isDrop = datetime;
    };
    HomePage.prototype.ConfirmPickup = function () {
        this.isPiked = true;
        this.Pickup();
    };
    HomePage.prototype.StartListeningForChanges = function (id) {
        var _this = this;
        console.log("STARTED LISTENING FOR CHANGES--ID", id);
        console.log("CAN LISTEN ??", this.canListen);
        if (this.canListen) {
            this.notify_ID = id;
            console.log("NOTIFIATION ID IN STARTLISTENIN", this.notify_ID);
            if (this.settings.current_ID) {
                this.storage.set("currentUserId", this.notify_ID).then(function () {
                    console.log("saved id");
                });
            }
            this.eventProvider
                .getChatList(this.notify_ID).on("child_added", function (snapshot) {
                if (_this.canStart) {
                    if (snapshot.val().Client_Message) {
                        _this.notification = true;
                        _this.pop.presentToast("New Message From Client");
                        _this.vibration.vibrate(1000);
                    }
                }
            });
            this.ph.getUserAsClientInfo(this.notify_ID).on("child_added", function () {
                console.log("RETRIEVING RIDER DETAILS HERE WITH NOTIFY ID.....", _this.notify_ID);
                _this.ph.getUserAsClientInfo(_this.notify_ID).once("value", function (driverSnap) {
                    _this.bull = driverSnap.val().client;
                    console.log("SEE WHATS COMING FROM CLIENT INFO", driverSnap);
                    console.log(_this.bull, "Client has Been Added Now............");
                    _this.customeID = driverSnap.val().client.Client_ID;
                    if (!driverSnap.val().Client_Driver_Reject) {
                        if (_this.jkl) {
                            if (driverSnap.val().client.Client_username) {
                                _this.Client_Added(driverSnap.val().client, driverSnap.val().client.Client_username);
                                _this.myListening.unsubscribe();
                            }
                            else {
                                _this.ph.getUserAsClientInfo(_this.notify_ID).remove();
                            }
                            _this.jkl = false;
                        }
                    }
                    if (_this.hasTimed) {
                        _this.canIncur = false;
                        _this.cancelTimer = Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["timer"])(120000).subscribe(function () {
                            _this.canIncur = true;
                        });
                        _this.hasTimed = false;
                    }
                    _this.hasEnded = false;
                    console.log("HAS ENDED??::", _this.hasEnded);
                    if (_this.canStart && driverSnap.val().client.Client_location) {
                        _this.locationTracker.D_lat = driverSnap.val().client.Client_location[0];
                        _this.locationTracker.D_lat = driverSnap.val().client.Client_location[1];
                    }
                    //this.ph.getUserAsClientInfo(this.notify_ID).off('child_added');
                    console.log("new user added");
                    _this.ph.getUserAsClientInfo(_this.notify_ID).off("value");
                });
            });
            this.ph.getUserAsClientInfo(this.notify_ID).on("child_changed", function () {
                _this.ph.getUserAsClientInfo(_this.notify_ID).once("value", function (driverSnap) {
                    if (driverSnap.val().client.Client_location != undefined) {
                        if (_this.canStart && driverSnap.val().client.Client_location[0]) {
                            _this.data = driverSnap.val().client;
                            _this.customeID = driverSnap.val().client.Client_ID;
                            _this.client_Name = _this.data.Client_username;
                            _this.UserChanged(driverSnap.val().client);
                            _this.locationTracker.D_lat = driverSnap.val().client.Client_location[0];
                            _this.locationTracker.D_lat = driverSnap.val().client.Client_location[1];
                            _this.ph.getUserAsClientInfo(_this.notify_ID).off("value");
                        }
                    }
                });
            });
            this.ph.getUserAsClientInfo(this.notify_ID).on("child_removed", function () {
                _this.ph.getUserAsClientInfo(_this.notify_ID).once("value", function () {
                    if (_this.canStart) {
                        if (_this.isPassengerCleared) {
                            console.log("is passenger that cleared called.........");
                            _this.UserRemoved();
                            if (_this.isUser)
                                _this.pop.showPimp("Passenger Ended The Trip");
                        }
                        else {
                            if (_this.myAlert) {
                                _this.RejectJob();
                                _this.myAlert = false;
                                // this.myAlert.dismiss();
                            }
                            console.log("is not passenger that cleared called.........");
                        }
                        _this.ph.getUserAsClientInfo(_this.notify_ID).off("value");
                    }
                });
            });
            // this.canListen = false;
        }
    };
    HomePage.prototype.Client_Added = function (rp, f) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var obj, _a;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.NewClientFound(rp.Client_locationName, rp.Client_location, rp.Client_username, rp.Client_picture, rp.Client_destinationName, rp.Client_Rating, rp.Client_RatingText, rp.Client_price);
                        this.pauseTimer();
                        this.rateUp = rp.Client_Positive_rating;
                        this.rateDown = rp.Client_Negative_rating;
                        console.log("CLIENT INFORMATION::", rp);
                        console.log("PROVIDER TIME::", this.dProvider.time);
                        console.log("DISTANCE::", this.dProvider.distance2);
                        console.log("CHARGE::", rp.Client_price);
                        console.log(rp.Client_locationName, rp.Client_destinationName);
                        obj = {
                            info: f, loc: rp.Client_locationName, des: rp.Client_destinationName,
                            charge: rp.Client_price, up: rp.Client_Positive_rating, down: rp.Client_Negative_rating, status: rp.Client_status, time: this.dProvider.time, distance: this.dProvider.distance2,
                        };
                        _a = this;
                        return [4 /*yield*/, this.modalCtrl.create({
                                component: src_app_pages_accept_accept_page__WEBPACK_IMPORTED_MODULE_21__["AcceptPage"], componentProps: obj,
                            })];
                    case 1:
                        _a.myAlert =
                            _b.sent();
                        return [4 /*yield*/, this.myAlert.present()];
                    case 2:
                        _b.sent();
                        if (rp.Client_pold) {
                            this.locationTracker.routeNumber = rp.Client_pold;
                            console.log(rp.Client_pold);
                        }
                        if (this.platform.is("cordova"))
                            this.vibration.vibrate(1000);
                        return [4 /*yield*/, this.myAlert.onDidDismiss().then(function (data) {
                                console.log("Accept  page dismissed.......DATA COMING--", data, data.data);
                                if (data.data == 1) {
                                    _this.ph
                                        .getUserAsClientInfo(_this.notify_ID).once("value", function (driverSnap) {
                                        _this.driverLoc = rp.Client_location;
                                        if (driverSnap.val() != null) {
                                            _this.AcceptJob();
                                            _this.canStart = true;
                                            // this.locationTracker.marker = null; --- WHY MAKE THIS NULL???
                                            //this.watchPositionSubscription.clearWatch(this.mapTracker); --- WHY CLEAR???
                                            _this.passenger_Lat = rp.Client_location[0];
                                            _this.passenger_Lng = rp.Client_location[1];
                                            _this.rateUp = rp.Client_Positive_rating;
                                            _this.rateDown = rp.Client_Negative_rating;
                                            console.log("called");
                                            _this.mapSection = true;
                                            _this.canDO = true;
                                            _this.hideNews = true;
                                            _this.userHasPhone = true;
                                            _this.c_added = false;
                                            console.log("driver location not null");
                                            _this.customerPhone = rp.Client_phoneNumber;
                                            _this.destination = rp.Client_destinationName;
                                            _this.customerID = rp.Client_Notif;
                                            console.log("Customer Phone --->>" + _this.customerPhone);
                                        }
                                        else {
                                            _this.pop.presentToast("You Lost the Job");
                                            _this.RejectJob();
                                            _this.canStart = false;
                                        }
                                        _this.ph.getUserAsClientInfo(_this.notify_ID).off("value");
                                    });
                                }
                                else {
                                    // this.ph.Reject(this.notify_ID);
                                    _this.RejectJob();
                                    _this.canStart = false;
                                    console.log("Job was rejeceted");
                                    _this.isUser = false;
                                }
                            })];
                    case 3:
                        _b.sent();
                        this.myAlert.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.prototype.chooseSubscriptiontype = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alert.create({
                            header: 'Either You pay Subscriptions or Commissions',
                            // buttons: ['OK'],
                            inputs: [
                                {
                                    label: 'Pay Commission',
                                    type: 'radio',
                                    value: 'commission',
                                },
                                {
                                    label: 'Pay Subscriptions',
                                    type: 'radio',
                                    value: 'subscriptions',
                                },
                            ],
                            buttons: [
                                {
                                    text: this.lp.translate()[0].b9,
                                    role: 'cancel',
                                    handler: function (data) {
                                    }
                                },
                                {
                                    text: 'Ok',
                                    handler: function (data) {
                                        console.log("WHAT DATA COMING FROM ALERT::", data);
                                        _this.ph.UpdateSubscription(data).then(function (success) {
                                            _this.subscribed = true;
                                            console.log("SUBSCRIBED SUCCESSFULLY", success);
                                        });
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    HomePage.ctorParameters = function () { return [
        { type: _ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__["Vibration"] },
        { type: _services_language_service__WEBPACK_IMPORTED_MODULE_13__["LanguageService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _services_settings_service__WEBPACK_IMPORTED_MODULE_9__["SettingsService"] },
        { type: _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"] },
        { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_22__["Router"] },
        { type: _services_location_tracker_service__WEBPACK_IMPORTED_MODULE_10__["LocationTrackerService"] },
        { type: _services_geocoder_service__WEBPACK_IMPORTED_MODULE_14__["GeocoderService"] },
        { type: _services_directionservice_service__WEBPACK_IMPORTED_MODULE_11__["DirectionserviceService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _services_onesignal_service__WEBPACK_IMPORTED_MODULE_12__["OnesignalService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: _services_pop_up_service__WEBPACK_IMPORTED_MODULE_15__["PopUpService"] },
        { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_24__["AuthService"] },
        { type: _services_profile_service__WEBPACK_IMPORTED_MODULE_17__["ProfileService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: _services_event_service__WEBPACK_IMPORTED_MODULE_16__["EventService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"] },
        { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_25__["CallNumber"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_26__["Geolocation"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-home",
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_vibration_ngx__WEBPACK_IMPORTED_MODULE_7__["Vibration"],
            _services_language_service__WEBPACK_IMPORTED_MODULE_13__["LanguageService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _services_settings_service__WEBPACK_IMPORTED_MODULE_9__["SettingsService"],
            _ionic_native_onesignal_ngx__WEBPACK_IMPORTED_MODULE_3__["OneSignal"],
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_22__["Router"],
            _services_location_tracker_service__WEBPACK_IMPORTED_MODULE_10__["LocationTrackerService"],
            _services_geocoder_service__WEBPACK_IMPORTED_MODULE_14__["GeocoderService"],
            _services_directionservice_service__WEBPACK_IMPORTED_MODULE_11__["DirectionserviceService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _services_onesignal_service__WEBPACK_IMPORTED_MODULE_12__["OnesignalService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            _services_pop_up_service__WEBPACK_IMPORTED_MODULE_15__["PopUpService"],
            _services_auth_service__WEBPACK_IMPORTED_MODULE_24__["AuthService"],
            _services_profile_service__WEBPACK_IMPORTED_MODULE_17__["ProfileService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            _services_event_service__WEBPACK_IMPORTED_MODULE_16__["EventService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"],
            _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_25__["CallNumber"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_26__["Geolocation"]])
    ], HomePage);
    return HomePage;
}());



/***/ }),

/***/ "./src/app/pages/paymentpage/paymentpage.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/paymentpage/paymentpage.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".inner-scroll {\n  left: 0px;\n  right: 0px;\n  top: calc(var(--offset-top) * -1);\n  bottom: calc(var(--offset-bottom) * -1);\n  padding-left: var(--padding-start);\n  padding-right: var(--padding-end);\n  padding-top: calc(var(--padding-top) + var(--offset-top));\n  padding-bottom: calc( var(--padding-bottom) + var(--keyboard-offset) + var(--offset-bottom) );\n  position: absolute;\n  background: #cf0d0dc7 !important;\n  color: var(--color);\n  box-sizing: border-box;\n  overflow: hidden;\n}\n\n.driverFound {\n  height: 47%;\n  width: 90%;\n  margin-left: 5%;\n  background: white;\n  position: absolute;\n  bottom: 30%;\n  z-index: 1;\n  border-top-left-radius: 30px;\n  border-top-right-radius: 30px;\n}\n\n.request-for-ride2 {\n  height: 200px;\n}\n\n.headSection {\n  background-color: #000000;\n  color: #fbb91d;\n  margin-top: -6%;\n  border-top-left-radius: 27px;\n  border-top-right-radius: 30px;\n  display: inline-block;\n  width: 100%;\n  height: 35%;\n  text-align: center;\n}\n\n.moveHeader {\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n  align-items: center;\n  margin-top: 20px;\n  margin-left: 0%;\n  margin-right: 0%;\n  width: 100%;\n}\n\n.centerText {\n  text-align: center;\n  font-size: 16px !important;\n}\n\n.resultContainer {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n  border-bottom: #bababa solid 1px;\n}\n\n.content-wrap,\n.img-wrapper {\n  display: inline-block;\n  margin-left: 50px;\n}\n\n#drivericonSize {\n  font-size: 70px !important;\n}\n\n.bookImage,\n.bookTitle,\n.bookPrice {\n  margin-left: 20px;\n}\n\n.resultContainer2 {\n  width: 100%;\n  position: relative;\n  left: 0;\n  margin-top: 0px;\n  margin-bottom: 0px;\n  padding: 10px;\n}\n\n.content-wrap2 {\n  display: inline-block;\n}\n\n.dott {\n  width: 20px;\n  height: 20px;\n}\n\n.centerBtn {\n  display: -webkit-box;\n  display: flex;\n  -webkit-box-pack: center;\n          justify-content: center;\n  -webkit-box-align: center;\n          align-items: center;\n}\n\nion-button {\n  height: 50px;\n}\n\n.butt {\n  display: inline-table;\n  height: auto;\n  overflow: hidden;\n}\n\n.price {\n  color: #248cd2;\n  font-size: 1.67em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-radius: 12px;\n}\n\n.price ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.location {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.location p {\n  font-size: 1.3em;\n  height: auto;\n}\n\n.location ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: orange;\n}\n\n.date {\n  color: orange;\n  font-size: 1.47em;\n  padding-top: 14px;\n  padding-bottom: 14px;\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n}\n\n.date ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.destination {\n  width: auto;\n  padding-top: 8px;\n  padding-bottom: 8px;\n}\n\n.destination ion-icon {\n  font-size: 0.8em;\n  padding: 5px;\n  color: darkslateblue;\n}\n\n#envelope {\n  height: auto;\n  width: 6em;\n}\n\n.bars {\n  margin-top: 0%;\n  padding: 12px;\n}\n\n.bars .poiter {\n  z-index: 5;\n  margin-left: 1%;\n  background: white;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n}\n\n.bars .bars-locate {\n  height: 50px;\n  width: 100%;\n  background: white;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  margin-left: 0%;\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-locate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n\n.bars .bars-destinate {\n  height: 100px;\n  width: 100%;\n  background: white;\n  margin: 3% 0 0 -50px;\n  margin-left: 0%;\n  z-index: 3;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  overflow: hidden;\n  border-radius: 50px;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-destinate ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  padding: 5px;\n  color: #248cd2;\n}\n\n.bars .bars-price {\n  height: 50px;\n  width: 100%;\n  background: #ffffff;\n  border-left: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-right: 1.1px solid rgba(212, 212, 212, 0.93);\n  border-top: 1px solid rgba(212, 212, 212, 0.93);\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93);\n  z-index: 3;\n  border-radius: 12px;\n  overflow: hidden;\n  line-height: 20px;\n  font-size: 1.2em;\n  text-align: center;\n}\n\n.bars .bars-price ion-icon {\n  position: absolute;\n  font-size: 1em;\n  left: 2%;\n  color: #0a64eb;\n  padding: 5px;\n}\n\n#position {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#whereto {\n  text-align: center;\n  padding-left: 17px;\n}\n\n#stuff {\n  color: #248cd2;\n  width: 100%;\n  height: 60% !important;\n  border: 1px solid #248cd2;\n}\n\n.no-scroll {\n  background: #0a64eb;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy9wYXltZW50cGFnZS9wYXltZW50cGFnZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BheW1lbnRwYWdlL3BheW1lbnRwYWdlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFtQkE7RUFDRSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlDQUFBO0VBQ0EsdUNBQUE7RUFDQSxrQ0FBQTtFQUNBLGlDQUFBO0VBQ0EseURBQUE7RUFDQSw2RkFBQTtFQUdBLGtCQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUNwQkY7O0FEMkJBO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSw2QkFBQTtBQ3hCRjs7QUQyQkE7RUFDRSxhQUFBO0FDeEJGOztBRDJCQTtFQUNFLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSw0QkFBQTtFQUNBLDZCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FDeEJGOztBRDBCQTtFQUNFLHdCQUFBO1VBQUEsdUJBQUE7RUFDQSx5QkFBQTtVQUFBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUN2QkY7O0FEMEJBO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtBQ3ZCRjs7QUQwQkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxPQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtFQUNBLGdDQUFBO0FDdkJGOztBRDBCQTs7RUFFRSxxQkFBQTtFQUNBLGlCQUFBO0FDdkJGOztBRDBCQTtFQUNFLDBCQUFBO0FDdkJGOztBRDBCQTs7O0VBR0UsaUJBQUE7QUN2QkY7O0FEMEJBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUN2QkY7O0FEMEJBO0VBQ0UscUJBQUE7QUN2QkY7O0FEMEJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUN2QkY7O0FEMEJBO0VBQ0Usb0JBQUE7RUFBQSxhQUFBO0VBQ0Esd0JBQUE7VUFBQSx1QkFBQTtFQUNBLHlCQUFBO1VBQUEsbUJBQUE7QUN2QkY7O0FENEJBO0VBQ0UsWUFBQTtBQ3pCRjs7QUQyQkE7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFFQSxnQkFBQTtBQ3pCRjs7QUQ0QkE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBRUEsbUJBQUE7QUMxQkY7O0FEMkJFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQ3pCSjs7QUQ2QkE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQzFCRjs7QUQyQkU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7QUN6Qko7O0FENEJFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtBQzFCSjs7QUQ4QkE7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0Esa0RBQUE7QUMzQkY7O0FENkJFO0VBQ0UsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtBQzNCSjs7QUQrQkE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQzVCRjs7QUQ2QkU7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxvQkFBQTtBQzNCSjs7QUQrQkE7RUFDRSxZQUFBO0VBQ0EsVUFBQTtBQzVCRjs7QUQrQkE7RUFDRSxjQUFBO0VBQ0EsYUFBQTtBQzVCRjs7QUQ4QkU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0RBQUE7RUFDQSxtREFBQTtFQUNBLCtDQUFBO0VBQ0Esa0RBQUE7QUM1Qko7O0FEK0JFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGtEQUFBO0VBQ0EsbURBQUE7RUFDQSwrQ0FBQTtFQUNBLGtEQUFBO0VBQ0EsZUFBQTtFQUVBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDOUJKOztBRGdDSTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQzlCTjs7QURrQ0U7RUFDRSxhQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLGtEQUFBO0VBQ0EsbURBQUE7RUFDQSwrQ0FBQTtFQUNBLGtEQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQ2hDSjs7QURrQ0k7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUNoQ047O0FEb0NFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGtEQUFBO0VBQ0EsbURBQUE7RUFDQSwrQ0FBQTtFQUNBLGtEQUFBO0VBRUEsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUNuQ0o7O0FEcUNJO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0VBQ0EsUUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FDbkNOOztBRHdDQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7QUNyQ0Y7O0FEd0NBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQ3JDRjs7QUR3Q0E7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EseUJBQUE7QUNyQ0Y7O0FEd0NBO0VBQ0UsbUJBQUE7QUNyQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9wYXltZW50cGFnZS9wYXltZW50cGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvL05FVyBERVNJR04gQ1NTXHJcblxyXG4vLyAuc2MtaW9uLW1vZGFsLW1kLWgge1xyXG4vLyAgIC0td2lkdGg6IDg2JTtcclxuLy8gICAtLW1pbi13aWR0aDogYXV0bztcclxuLy8gICAtLW1heC13aWR0aDogYXV0bztcclxuLy8gICAtLWhlaWdodDogNzAlICFpbXBvcnRhbnQ7XHJcbi8vICAgLS1taW4taGVpZ2h0OiBhdXRvO1xyXG4vLyAgIC0tbWF4LWhlaWdodDogYXV0bztcclxuLy8gICAtLW92ZXJmbG93OiBoaWRkZW47XHJcbi8vICAgLS1ib3JkZXItcmFkaXVzOiAwO1xyXG4vLyAgIC0tYm9yZGVyLXdpZHRoOiAwO1xyXG4vLyAgIC0tYm9yZGVyLXN0eWxlOiBub25lO1xyXG4vLyAgIC0tYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudDtcclxuLy8gICAtLWJhY2tncm91bmQ6IHZhcigtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yLCAjZmZmKTtcclxuLy8gICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcblxyXG4vLyB9XHJcblxyXG4uaW5uZXItc2Nyb2xsIHtcclxuICBsZWZ0OiAwcHg7XHJcbiAgcmlnaHQ6IDBweDtcclxuICB0b3A6IGNhbGModmFyKC0tb2Zmc2V0LXRvcCkgKiAtMSk7XHJcbiAgYm90dG9tOiBjYWxjKHZhcigtLW9mZnNldC1ib3R0b20pICogLTEpO1xyXG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFkZGluZy1zdGFydCk7XHJcbiAgcGFkZGluZy1yaWdodDogdmFyKC0tcGFkZGluZy1lbmQpO1xyXG4gIHBhZGRpbmctdG9wOiBjYWxjKHZhcigtLXBhZGRpbmctdG9wKSArIHZhcigtLW9mZnNldC10b3ApKTtcclxuICBwYWRkaW5nLWJvdHRvbTogY2FsYyhcclxuICAgIHZhcigtLXBhZGRpbmctYm90dG9tKSArIHZhcigtLWtleWJvYXJkLW9mZnNldCkgKyB2YXIoLS1vZmZzZXQtYm90dG9tKVxyXG4gICk7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGJhY2tncm91bmQ6ICNjZjBkMGRjNyAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB2YXIoLS1jb2xvcik7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG4vLyAuYmFja2dyb3VuZENvbG9yIHtcclxuLy8gICBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhO1xyXG4vLyAgIGhlaWdodDogOTBweDtcclxuLy8gfVxyXG4uZHJpdmVyRm91bmQge1xyXG4gIGhlaWdodDogNDclO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBib3R0b206IDMwJTtcclxuICB6LWluZGV4OiAxO1xyXG4gIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDMwcHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XHJcbn1cclxuXHJcbi5yZXF1ZXN0LWZvci1yaWRlMiB7XHJcbiAgaGVpZ2h0OiAyMDBweDtcclxufVxyXG5cclxuLmhlYWRTZWN0aW9uIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwO1xyXG4gIGNvbG9yOiAjZmJiOTFkO1xyXG4gIG1hcmdpbi10b3A6IC02JTtcclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyN3B4O1xyXG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAzMHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDM1JTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLm1vdmVIZWFkZXIge1xyXG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAwJTtcclxuICBtYXJnaW4tcmlnaHQ6IDAlO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uY2VudGVyVGV4dCB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ucmVzdWx0Q29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbGVmdDogMDtcclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgYm9yZGVyLWJvdHRvbTogI2JhYmFiYSBzb2xpZCAxcHg7XHJcbn1cclxuXHJcbi5jb250ZW50LXdyYXAsXHJcbi5pbWctd3JhcHBlciB7XHJcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xyXG4gIG1hcmdpbi1sZWZ0OiA1MHB4O1xyXG59XHJcblxyXG4jZHJpdmVyaWNvblNpemUge1xyXG4gIGZvbnQtc2l6ZTogNzBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYm9va0ltYWdlLFxyXG4uYm9va1RpdGxlLFxyXG4uYm9va1ByaWNlIHtcclxuICBtYXJnaW4tbGVmdDogMjBweDtcclxufVxyXG5cclxuLnJlc3VsdENvbnRhaW5lcjIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBsZWZ0OiAwO1xyXG4gIG1hcmdpbi10b3A6IDBweDtcclxuICBtYXJnaW4tYm90dG9tOiAwMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbn1cclxuXHJcbi5jb250ZW50LXdyYXAyIHtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbn1cclxuXHJcbi5kb3R0IHtcclxuICB3aWR0aDogMjBweDtcclxuICBoZWlnaHQ6IDIwcHg7XHJcbn1cclxuXHJcbi5jZW50ZXJCdG4ge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxufVxyXG5cclxuLy8gRU5EIE5FVyBERVNJR04gQ1NTXHJcblxyXG5pb24tYnV0dG9uIHtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbn1cclxuLmJ1dHQge1xyXG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcclxuICBoZWlnaHQ6IGF1dG87XHJcblxyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuXHJcbi5wcmljZSB7XHJcbiAgY29sb3I6IHJnYigzNiwgMTQwLCAyMTApO1xyXG4gIGZvbnQtc2l6ZTogMS42N2VtO1xyXG4gIHBhZGRpbmctdG9wOiAxNHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xyXG5cclxuICBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gIGlvbi1pY29uIHtcclxuICAgIGZvbnQtc2l6ZTogMC44ZW07XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICBjb2xvcjogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgfVxyXG59XHJcblxyXG4ubG9jYXRpb24ge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIHBhZGRpbmctdG9wOiA4cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDhweDtcclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMS4zZW07XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgfVxyXG5cclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IG9yYW5nZTtcclxuICB9XHJcbn1cclxuXHJcbi5kYXRlIHtcclxuICBjb2xvcjogb3JhbmdlO1xyXG4gIGZvbnQtc2l6ZTogMS40N2VtO1xyXG4gIHBhZGRpbmctdG9wOiAxNHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNHB4O1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG5cclxuICBpb24taWNvbiB7XHJcbiAgICBmb250LXNpemU6IDAuOGVtO1xyXG4gICAgcGFkZGluZzogNXB4O1xyXG4gICAgY29sb3I6IHJnYigzNiwgMTQwLCAyMTApO1xyXG4gIH1cclxufVxyXG5cclxuLmRlc3RpbmF0aW9uIHtcclxuICB3aWR0aDogYXV0bztcclxuICBwYWRkaW5nLXRvcDogOHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbiAgaW9uLWljb24ge1xyXG4gICAgZm9udC1zaXplOiAwLjhlbTtcclxuICAgIHBhZGRpbmc6IDVweDtcclxuICAgIGNvbG9yOiBkYXJrc2xhdGVibHVlO1xyXG4gIH1cclxufVxyXG5cclxuI2VudmVsb3BlIHtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgd2lkdGg6IDZlbTtcclxufVxyXG5cclxuLmJhcnMge1xyXG4gIG1hcmdpbi10b3A6IDAlO1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcblxyXG4gIC5wb2l0ZXIge1xyXG4gICAgei1pbmRleDogNTtcclxuICAgIG1hcmdpbi1sZWZ0OiAxJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICB9XHJcblxyXG4gIC5iYXJzLWxvY2F0ZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICAgIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIG1hcmdpbi1sZWZ0OiAwJTtcclxuXHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgY29sb3I6IHJnYigxMCwgMTAwLCAyMzUpO1xyXG4gICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYmFycy1kZXN0aW5hdGUge1xyXG4gICAgaGVpZ2h0OiAxMDBweDtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgYmFja2dyb3VuZDogcmdiKDI1NSwgMjU1LCAyNTUpO1xyXG4gICAgbWFyZ2luOiAzJSAwIDAgLTUwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMCU7XHJcbiAgICB6LWluZGV4OiAzO1xyXG4gICAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcbiAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgICBsaW5lLWhlaWdodDogMjBweDtcclxuICAgIGZvbnQtc2l6ZTogMS4yZW07XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG4gICAgaW9uLWljb24ge1xyXG4gICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgIGZvbnQtc2l6ZTogMWVtO1xyXG4gICAgICBsZWZ0OiAyJTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgICBjb2xvcjogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgICB9XHJcbiAgfVxyXG5cclxuICAuYmFycy1wcmljZSB7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XHJcbiAgICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xyXG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XHJcblxyXG4gICAgei1pbmRleDogMztcclxuICAgIGJvcmRlci1yYWRpdXM6IDEycHg7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICAgIGlvbi1pY29uIHtcclxuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICBmb250LXNpemU6IDFlbTtcclxuICAgICAgbGVmdDogMiU7XHJcbiAgICAgIGNvbG9yOiByZ2IoMTAsIDEwMCwgMjM1KTtcclxuICAgICAgcGFkZGluZzogNXB4O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuI3Bvc2l0aW9uIHtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgcGFkZGluZy1sZWZ0OiAxN3B4O1xyXG59XHJcblxyXG4jd2hlcmV0byB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHBhZGRpbmctbGVmdDogMTdweDtcclxufVxyXG5cclxuI3N0dWZmIHtcclxuICBjb2xvcjogcmdiKDM2LCAxNDAsIDIxMCk7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiA2MCUgIWltcG9ydGFudDtcclxuICBib3JkZXI6IDFweCBzb2xpZCByZ2IoMzYsIDE0MCwgMjEwKTtcclxufVxyXG5cclxuLm5vLXNjcm9sbCB7XHJcbiAgYmFja2dyb3VuZDogcmdiKDEwLCAxMDAsIDIzNSk7XHJcbn1cclxuIiwiLmlubmVyLXNjcm9sbCB7XG4gIGxlZnQ6IDBweDtcbiAgcmlnaHQ6IDBweDtcbiAgdG9wOiBjYWxjKHZhcigtLW9mZnNldC10b3ApICogLTEpO1xuICBib3R0b206IGNhbGModmFyKC0tb2Zmc2V0LWJvdHRvbSkgKiAtMSk7XG4gIHBhZGRpbmctbGVmdDogdmFyKC0tcGFkZGluZy1zdGFydCk7XG4gIHBhZGRpbmctcmlnaHQ6IHZhcigtLXBhZGRpbmctZW5kKTtcbiAgcGFkZGluZy10b3A6IGNhbGModmFyKC0tcGFkZGluZy10b3ApICsgdmFyKC0tb2Zmc2V0LXRvcCkpO1xuICBwYWRkaW5nLWJvdHRvbTogY2FsYyggdmFyKC0tcGFkZGluZy1ib3R0b20pICsgdmFyKC0ta2V5Ym9hcmQtb2Zmc2V0KSArIHZhcigtLW9mZnNldC1ib3R0b20pICk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYmFja2dyb3VuZDogI2NmMGQwZGM3ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB2YXIoLS1jb2xvcik7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5kcml2ZXJGb3VuZCB7XG4gIGhlaWdodDogNDclO1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMzAlO1xuICB6LWluZGV4OiAxO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAzMHB4O1xuICBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMzBweDtcbn1cblxuLnJlcXVlc3QtZm9yLXJpZGUyIHtcbiAgaGVpZ2h0OiAyMDBweDtcbn1cblxuLmhlYWRTZWN0aW9uIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgY29sb3I6ICNmYmI5MWQ7XG4gIG1hcmdpbi10b3A6IC02JTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMjdweDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDMwcHg7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzUlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tb3ZlSGVhZGVyIHtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgbWFyZ2luLXJpZ2h0OiAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jZW50ZXJUZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbn1cblxuLnJlc3VsdENvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGxlZnQ6IDA7XG4gIG1hcmdpbi10b3A6IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xuICBwYWRkaW5nOiAxMHB4O1xuICBib3JkZXItYm90dG9tOiAjYmFiYWJhIHNvbGlkIDFweDtcbn1cblxuLmNvbnRlbnQtd3JhcCxcbi5pbWctd3JhcHBlciB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDUwcHg7XG59XG5cbiNkcml2ZXJpY29uU2l6ZSB7XG4gIGZvbnQtc2l6ZTogNzBweCAhaW1wb3J0YW50O1xufVxuXG4uYm9va0ltYWdlLFxuLmJvb2tUaXRsZSxcbi5ib29rUHJpY2Uge1xuICBtYXJnaW4tbGVmdDogMjBweDtcbn1cblxuLnJlc3VsdENvbnRhaW5lcjIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICBtYXJnaW4tdG9wOiAwcHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmNvbnRlbnQtd3JhcDIge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbi5kb3R0IHtcbiAgd2lkdGg6IDIwcHg7XG4gIGhlaWdodDogMjBweDtcbn1cblxuLmNlbnRlckJ0biB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xufVxuXG5pb24tYnV0dG9uIHtcbiAgaGVpZ2h0OiA1MHB4O1xufVxuXG4uYnV0dCB7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgaGVpZ2h0OiBhdXRvO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogIzI0OGNkMjtcbiAgZm9udC1zaXplOiAxLjY3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbn1cbi5wcmljZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6ICMyNDhjZDI7XG59XG5cbi5sb2NhdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmxvY2F0aW9uIHAge1xuICBmb250LXNpemU6IDEuM2VtO1xuICBoZWlnaHQ6IGF1dG87XG59XG4ubG9jYXRpb24gaW9uLWljb24ge1xuICBmb250LXNpemU6IDAuOGVtO1xuICBwYWRkaW5nOiA1cHg7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG5cbi5kYXRlIHtcbiAgY29sb3I6IG9yYW5nZTtcbiAgZm9udC1zaXplOiAxLjQ3ZW07XG4gIHBhZGRpbmctdG9wOiAxNHB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTRweDtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG59XG4uZGF0ZSBpb24taWNvbiB7XG4gIGZvbnQtc2l6ZTogMC44ZW07XG4gIHBhZGRpbmc6IDVweDtcbiAgY29sb3I6ICMyNDhjZDI7XG59XG5cbi5kZXN0aW5hdGlvbiB7XG4gIHdpZHRoOiBhdXRvO1xuICBwYWRkaW5nLXRvcDogOHB4O1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuLmRlc3RpbmF0aW9uIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogZGFya3NsYXRlYmx1ZTtcbn1cblxuI2VudmVsb3BlIHtcbiAgaGVpZ2h0OiBhdXRvO1xuICB3aWR0aDogNmVtO1xufVxuXG4uYmFycyB7XG4gIG1hcmdpbi10b3A6IDAlO1xuICBwYWRkaW5nOiAxMnB4O1xufVxuLmJhcnMgLnBvaXRlciB7XG4gIHotaW5kZXg6IDU7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIGJvcmRlci1sZWZ0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItcmlnaHQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSB7XG4gIGhlaWdodDogNTBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgei1pbmRleDogMztcbiAgYm9yZGVyLXJhZGl1czogMTJweDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5iYXJzIC5iYXJzLWxvY2F0ZSBpb24taWNvbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAxZW07XG4gIGxlZnQ6IDIlO1xuICBjb2xvcjogIzBhNjRlYjtcbiAgcGFkZGluZzogNXB4O1xufVxuLmJhcnMgLmJhcnMtZGVzdGluYXRlIHtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6IHdoaXRlO1xuICBtYXJnaW46IDMlIDAgMCAtNTBweDtcbiAgbWFyZ2luLWxlZnQ6IDAlO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItbGVmdDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXJpZ2h0OiAxLjFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBmb250LXNpemU6IDEuMmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4uYmFycyAuYmFycy1kZXN0aW5hdGUgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgcGFkZGluZzogNXB4O1xuICBjb2xvcjogIzI0OGNkMjtcbn1cbi5iYXJzIC5iYXJzLXByaWNlIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyLWxlZnQ6IDEuMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci1yaWdodDogMS4xcHggc29saWQgcmdiYSgyMTIsIDIxMiwgMjEyLCAwLjkzKTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpO1xuICB6LWluZGV4OiAzO1xuICBib3JkZXItcmFkaXVzOiAxMnB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxLjJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLmJhcnMgLmJhcnMtcHJpY2UgaW9uLWljb24ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGZvbnQtc2l6ZTogMWVtO1xuICBsZWZ0OiAyJTtcbiAgY29sb3I6ICMwYTY0ZWI7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuI3Bvc2l0aW9uIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiN3aGVyZXRvIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLWxlZnQ6IDE3cHg7XG59XG5cbiNzdHVmZiB7XG4gIGNvbG9yOiAjMjQ4Y2QyO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA2MCUgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzI0OGNkMjtcbn1cblxuLm5vLXNjcm9sbCB7XG4gIGJhY2tncm91bmQ6ICMwYTY0ZWI7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/paymentpage/paymentpage.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/paymentpage/paymentpage.page.ts ***!
  \*******************************************************/
/*! exports provided: PaymentpagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PaymentpagePage", function() { return PaymentpagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







// import { ViewEncapsulation } from "@angular/compiler/src/core";
var PaymentpagePage = /** @class */ (function () {
    function PaymentpagePage(navCtrl, lp, settings, modal, pop, activatedRoute) {
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.modal = modal;
        this.pop = pop;
        this.activatedRoute = activatedRoute;
        this.payment_method = this.activatedRoute.snapshot.paramMap.get("payment_method");
        this.price = this.activatedRoute.snapshot.paramMap.get("amount");
        console.log("AMOUNT CLIENT IS PAYING");
        // this.amount = this.amount.toFixed(2);
    }
    PaymentpagePage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad AcceptPage");
    };
    PaymentpagePage.prototype.closeModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PaymentpagePage.prototype.acceptModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss()];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    PaymentpagePage.prototype.ngOnInit = function () { };
    PaymentpagePage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PaymentpagePage.prototype, "amount", void 0);
    PaymentpagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-paymentpage",
            template: __webpack_require__(/*! raw-loader!./paymentpage.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/paymentpage/paymentpage.page.html"),
            encapsulation: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewEncapsulation"].None,
            styles: [__webpack_require__(/*! ./paymentpage.page.scss */ "./src/app/pages/paymentpage/paymentpage.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_4__["SettingsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_5__["PopUpService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"]])
    ], PaymentpagePage);
    return PaymentpagePage;
}());



/***/ }),

/***/ "./src/app/pages/tripinfo/tripinfo.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/tripinfo/tripinfo.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "page-trip-info ion-card-header {\n  text-align: center;\n}\npage-trip-info .drive {\n  color: blue;\n}\npage-trip-info .price {\n  color: blue;\n}\npage-trip-info .date {\n  color: red;\n}\npage-trip-info .destination {\n  color: green;\n}\npage-trip-info h3 {\n  font-size: 1.2em;\n  color: blue;\n  text-align: center;\n}\npage-trip-info ion-item {\n  color: black;\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93) !important;\n}\npage-trip-info h1 {\n  text-align: center;\n  font-size: 1.2em;\n  color: blue;\n}\npage-trip-info p {\n  font-size: 1.3em;\n  height: auto;\n  color: black;\n}\npage-trip-info h3 {\n  font-size: 1em;\n  color: #5a5a5a;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy90cmlwaW5mby90cmlwaW5mby5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3RyaXBpbmZvL3RyaXBpbmZvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGtCQUFBO0FDQUo7QURHRTtFQUNFLFdBQUE7QUNESjtBRElFO0VBQ0UsV0FBQTtBQ0ZKO0FES0U7RUFDRSxVQUFBO0FDSEo7QURNRTtFQUNFLFlBQUE7QUNKSjtBRE9FO0VBQ0UsZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUNMSjtBRFFFO0VBRUUsWUFBQTtFQUNBLDZEQUFBO0FDUEo7QURVRTtFQUNFLGtCQUFBO0VBQ0EsZ0JBQUE7RUFFQSxXQUFBO0FDVEo7QURZRTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUNWSjtBRGFFO0VBQ0UsY0FBQTtFQUVBLGNBQUE7QUNaSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3RyaXBpbmZvL3RyaXBpbmZvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbInBhZ2UtdHJpcC1pbmZvIHtcclxuICBpb24tY2FyZC1oZWFkZXIge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuXHJcbiAgLmRyaXZlIHtcclxuICAgIGNvbG9yOiBibHVlO1xyXG4gIH1cclxuXHJcbiAgLnByaWNlIHtcclxuICAgIGNvbG9yOiBibHVlO1xyXG4gIH1cclxuXHJcbiAgLmRhdGUge1xyXG4gICAgY29sb3I6IHJlZDtcclxuICB9XHJcblxyXG4gIC5kZXN0aW5hdGlvbiB7XHJcbiAgICBjb2xvcjogZ3JlZW47XHJcbiAgfVxyXG5cclxuICBoMyB7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgY29sb3I6IGJsdWU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgfVxyXG5cclxuICBpb24taXRlbSB7XHJcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxMnB4O1xyXG4gICAgY29sb3I6IHJnYigwLCAwLCAwKTtcclxuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG5cclxuICBoMSB7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICBmb250LXNpemU6IDEuMmVtO1xyXG4gICAgLy93aWR0aDogMTAwJSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IGJsdWU7XHJcbiAgfVxyXG5cclxuICBwIHtcclxuICAgIGZvbnQtc2l6ZTogMS4zZW07XHJcbiAgICBoZWlnaHQ6IGF1dG87XHJcbiAgICBjb2xvcjogcmdiKDAsIDAsIDApO1xyXG4gIH1cclxuXHJcbiAgaDMge1xyXG4gICAgZm9udC1zaXplOiAxZW07XHJcblxyXG4gICAgY29sb3I6IHJnYig5MCwgOTAsIDkwKTtcclxuICB9XHJcblxyXG4gIC8vIC5oaXN0cyB7XHJcbiAgLy8gICBib3JkZXI6IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpICFpbXBvcnRhbnQ7XHJcblxyXG4gIC8vICAgZm9udC1zaXplOiAxLjFlbTtcclxuICAvLyAgIHdpZHRoOiAxMDAlO1xyXG4gIC8vICAgYm9yZGVyLXJhZGl1czogMTJweDtcclxuICAvLyAgIC8vcGFkZGluZzogNnB4O1xyXG4gIC8vICAgYmFja2dyb3VuZDogI2Y3ZjdmNztcclxuICAvLyAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XHJcbiAgLy8gfVxyXG59XHJcbiIsInBhZ2UtdHJpcC1pbmZvIGlvbi1jYXJkLWhlYWRlciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbnBhZ2UtdHJpcC1pbmZvIC5kcml2ZSB7XG4gIGNvbG9yOiBibHVlO1xufVxucGFnZS10cmlwLWluZm8gLnByaWNlIHtcbiAgY29sb3I6IGJsdWU7XG59XG5wYWdlLXRyaXAtaW5mbyAuZGF0ZSB7XG4gIGNvbG9yOiByZWQ7XG59XG5wYWdlLXRyaXAtaW5mbyAuZGVzdGluYXRpb24ge1xuICBjb2xvcjogZ3JlZW47XG59XG5wYWdlLXRyaXAtaW5mbyBoMyB7XG4gIGZvbnQtc2l6ZTogMS4yZW07XG4gIGNvbG9yOiBibHVlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5wYWdlLXRyaXAtaW5mbyBpb24taXRlbSB7XG4gIGNvbG9yOiBibGFjaztcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45MykgIWltcG9ydGFudDtcbn1cbnBhZ2UtdHJpcC1pbmZvIGgxIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEuMmVtO1xuICBjb2xvcjogYmx1ZTtcbn1cbnBhZ2UtdHJpcC1pbmZvIHAge1xuICBmb250LXNpemU6IDEuM2VtO1xuICBoZWlnaHQ6IGF1dG87XG4gIGNvbG9yOiBibGFjaztcbn1cbnBhZ2UtdHJpcC1pbmZvIGgzIHtcbiAgZm9udC1zaXplOiAxZW07XG4gIGNvbG9yOiAjNWE1YTVhO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/tripinfo/tripinfo.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/tripinfo/tripinfo.page.ts ***!
  \*************************************************/
/*! exports provided: TripinfoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TripinfoPage", function() { return TripinfoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");








var TripinfoPage = /** @class */ (function () {
    function TripinfoPage(navCtrl, prof, lp, settings, eventSerEventService, modal, activatedRoute, alertCtl) {
        this.navCtrl = navCtrl;
        this.prof = prof;
        this.lp = lp;
        this.settings = settings;
        this.eventSerEventService = eventSerEventService;
        this.modal = modal;
        this.activatedRoute = activatedRoute;
        this.alertCtl = alertCtl;
        this.currentEvent = {};
        this.total = 0;
        this.allTotals = 0;
        this.driverMade = 0;
        this.surcharges = [];
        this.totalSurge = 0;
        this.actual = 0;
        this.riderPaid = 0;
        this.totalRiderSurge = 0;
        this.totalDriverSurge = 0;
        this.percentRider = 0;
        this.flatRider = 0;
        this.percentDriver = 0;
        this.flatDriver = 0;
        // info= this.info;
        // accepted= this.accepted1;
        // arrived= this.arrived1;
        // distance= this.distance1;
        this.riderPercents = [];
        this.driverPercents = [];
        this.math = Math;
        this.riderpaid = 0;
    }
    TripinfoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.prof
            .getCompanies()
            .child("Cancelled/documents")
            .on("value", function (snapshot) {
            _this.items = [];
            snapshot.forEach(function (snap) {
                if (snap.val().type == "Driver")
                    _this.items.push({
                        status: snap.val().status,
                        text: snap.val().title,
                    });
                return false;
            });
        });
        var g = [];
        var f = [];
        var b = [];
        var k = [];
        var o = [];
        var c = [];
        var n = [];
        var sa;
        console.log("INFORMATION ICOMING", this.info);
        this.tolls = this.info.Client_toll;
        this.info.Client_realPrice;
        this.osc = this.info.Client_OutOfStateCharge;
        this.surcharges = this.info.Client_Surcharges;
        if (this.surcharges)
            for (var index = 0; index < this.surcharges.length; index++) {
                k.push(parseFloat(this.info.Client_Surcharges[index].price));
                var add = function (a, b) { return a + b; };
                var result = k.reduce(add);
                this.totalSurge = result;
                this.actual = this.info.Client_price - this.totalSurge;
                console.log(this.totalSurge);
                this.info.Client_Surcharges[index].price;
            }
        this.surcharges.forEach(function (element) {
            //if rider
            if (element.owner == 1) {
                //if percent
                if (element.bone == 1) {
                    var fo = (element.price / 100) * _this.info.Client_realPrice;
                    console.log(element.price);
                    o.push(fo);
                    var add1 = function (a, b) { return a + b; };
                    var result1 = o.reduce(add1);
                    _this.percentRider = result1;
                    console.log(_this.percentRider);
                }
                if (element.bone == 0) {
                    g.push(parseFloat(element.price));
                    var add = function (a, b) { return a + b; };
                    var result = g.reduce(add);
                    _this.flatRider = result;
                    console.log(result);
                    element.price;
                }
                _this.totalRiderSurge = _this.flatRider + _this.percentRider;
                console.log(_this.totalRiderSurge);
            }
            // this.riderpaid = (this.info.Client_price);
            _this.riderpaid = parseFloat(_this.info.Client_price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    var nb = element.price / 100;
                    console.log(nb * _this.riderpaid);
                    var fo = nb * _this.riderpaid;
                    n.push(fo);
                    var add2 = function (a, b) { return a + b; };
                    var result2 = n.reduce(add2);
                    _this.percentDriver = result2;
                    console.log((Math.floor(element.price) / 100) * _this.riderpaid);
                }
                //if flat fee
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    var add4 = function (a, b) { return a + b; };
                    var result4 = c.reduce(add4);
                    _this.flatDriver = result4;
                    console.log(result4);
                }
                _this.totalDriverSurge = _this.flatDriver + _this.percentDriver;
                console.log(_this.totalDriverSurge, _this.flatDriver, _this.percentDriver);
            }
        });
        // console.log(this.actual, g, this.currentEvent.surcharge[index].price);
        if (this.info.Client_toll)
            for (var index = 0; index < this.info.Client_toll.length; index++) {
                b.push(this.info.Client_toll[index].tagCost);
                var add = function (a, b) { return a + b; };
                var result = b.reduce(add);
                this.total = result;
                console.log(this.total, g);
            }
        this.driverMade = (this.riderpaid - this.totalDriverSurge).toFixed(2);
    };
    TripinfoPage.prototype.closeModal = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.dismiss(null)];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TripinfoPage.prototype.onChange = function (e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                // await this.modal.dismiss(e);
                this.presentAlertCheckbox(e);
                return [2 /*return*/];
            });
        });
    };
    TripinfoPage.prototype.ngOnInit = function () {
        console.log("INFORMATION ICOMING", this.info);
    };
    TripinfoPage.prototype.presentAlertCheckbox = function (e) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertCtl.create({
                            header: 'Reason for cancellation ',
                            inputs: [
                                {
                                    name: 'checkbox1',
                                    type: 'radio',
                                    label: 'Pickup is too far',
                                    value: 'pickup',
                                },
                                {
                                    name: 'checkbox2',
                                    type: 'radio',
                                    label: 'GPS problem',
                                    value: 'gps'
                                },
                                {
                                    name: 'checkbox3',
                                    type: 'radio',
                                    label: 'Rider asked to cancel',
                                    value: 'rider'
                                },
                                {
                                    name: 'checkbox4',
                                    type: 'radio',
                                    label: 'Maintenance problem',
                                    value: 'value4'
                                },
                                {
                                    name: 'checkbox5',
                                    type: 'text',
                                    label: 'Others',
                                },
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                        console.log('Confirm Cancel');
                                    }
                                }, {
                                    text: 'Ok',
                                    handler: function () {
                                        console.log('Confirm Ok');
                                        _this.modal.dismiss(e);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    TripinfoPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TripinfoPage.prototype, "info", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TripinfoPage.prototype, "accepted", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TripinfoPage.prototype, "arrived", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], TripinfoPage.prototype, "distance", void 0);
    TripinfoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: "app-tripinfo",
            template: __webpack_require__(/*! raw-loader!./tripinfo.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/tripinfo/tripinfo.page.html"),
            styles: [__webpack_require__(/*! ./tripinfo.page.scss */ "./src/app/pages/tripinfo/tripinfo.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_4__["ProfileService"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_6__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_5__["SettingsService"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_3__["EventService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_7__["ActivatedRoute"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], TripinfoPage);
    return TripinfoPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es5.js.map