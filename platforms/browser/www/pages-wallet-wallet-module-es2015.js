(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-wallet-wallet-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/wallet/wallet.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/wallet/wallet.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-border>\n  <ion-toolbar style=\"margin-top: 20px\">\n    <ion-button (click)=\"goBack()\" color=\"primary\" fill=\"clear\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n      <span style=\"margin-left: 30px; font-size: 1.4em\"\n        >Commission Payment</span\n      >\n    </ion-button>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n\n    <div class=\"test\" >\n\n        <ion-item  style=\"text-align: left;\">\n        <h4 style=\"text-align: left;\">\n      Your Earnings was:  <span style=\"color: red\">GHC {{mr}}</span>\n    </h4>\n    </ion-item>\n      <ion-item  style=\"text-align: left;\">\n     <h4 style=\"text-align: left;\">\n    *No Bonus Earned <span style='font-size:10px;'>&#128542;</span>. Ride more to unlock the bonus\n    </h4>\n    </ion-item>\n  <ion-item  style=\"text-align: left;\">\n  \n    <!-- <ion-input\n      type=\"number\"\n      placeholder=\"Enter Amount you want to pay\"\n      [(ngModel)]=\"amount\"\n    ></ion-input> -->\n\n       <h4 style=\"text-align: left;\">\n      Pay your commission of <span style=\"color: red\">GHC {{commission}}</span> with a single click\n    </h4>\n  </ion-item>\n\n\n  <div class=\"centerDiv2\" >\n    <angular4-paystack\n      [key]=\"'pk_live_caabde47a485606dc025e27220d3c03548aa40f2'\"\n      [email]=\"email\"\n      [amount]=\"commission * 100\"\n      [ref]=\"random_id\"\n      [channels]=\"channels\"\n      [currency]=\"'GHS'\"\n      [class]=\"'btn-primary2'\"\n      (close)=\"paymentCancel()\"\n      (callback)=\"paymentDone($event)\"\n      >Pay with Mobile Money/Card</angular4-paystack\n    >\n  </div>\n\n  </div>\n\n\n\n\n  <!-- <div *ngIf =\"has_subscribed == 'subscriptions'\">\n  <ion-item *ngIf=\"ph.funds\" style=\"text-align: center; margin-top: 20%\">\n    <h4>\n      You have successfully funded\n      <span style=\"color: #fbb91d\">GHC {{ph.funds}} </span>in your wallet. You\n      have 30 days to use it on Call4Ride. Enjoy driving!\n    </h4>\n  </ion-item>\n\n  <div class=\"centerDiv2\" *ngIf=\"ph.funds\">\n    <ion-button (click)=\"updateFundsO()\" slot=\"end\">\n      <ion-icon class=\"ion-text-center\" name=\"ios-add\"></ion-icon> Renew 30- day\n      Subscription\n    </ion-button>\n  </div>\n\n\n  <div style=\"text-align: center; margin-top: 20%\" *ngIf=\"!ph.funds\">\n    <h4>Subscribe to drive with call4ride!</h4>\n\n    <p>\n      Fund your wallet of <span style=\"color: #fbb91d\">GHC 100 </span>and have\n      30 days to use it on Call4Ride. Enjoy driving!\n    </p>\n\n    <div class=\"centerDiv2\">\n      <ion-button (click)=\"subscribe()\" slot=\"end\">\n        <ion-icon class=\"ion-text-center\" name=\"ios-add\"></ion-icon> Fund Wallet\n      </ion-button>\n    </div>\n  </div>\n\n  <div class=\"centerDiv2\" *ngIf=\"funds\">\n    <angular4-paystack\n      [key]=\"'pk_live_caabde47a485606dc025e27220d3c03548aa40f2'\"\n      [email]=\"'nana@Call4Ride.com'\"\n      [amount]=\"funds\"\n      [ref]=\"random_id\"\n      [channels]=\"channels\"\n      [currency]=\"'GHS'\"\n      (close)=\"paymentCancel()\"\n      (callback)=\"paymentDone($event)\"\n      >Pay GHC {{funds}} with Mobile Money/Card</angular4-paystack\n    >\n  </div>\n\n   <div class=\"centerDiv2\">\n      <ion-button (click)=\"chooseSubscriptiontype()\" slot=\"end\">\n        <ion-icon class=\"ion-text-center\" name=\"ios-add\"></ion-icon> Change Plan\n      </ion-button>\n    </div>\n  </div>\n\n  <div *ngIf =\"has_subscribed == 'commission'\">\n      <div style=\"text-align: center; margin-top: 20%\">\n    <h4>You are on the Commision Plan</h4>\n\n    <p>\n      This mean you pay  <span style=\"color: #fbb91d\">%15 </span> of every sale you make. Enjoy driving!\n    </p>\n\n    <div class=\"centerDiv2\">\n      <ion-button (click)=\"chooseSubscriptiontype()\" slot=\"end\">\n        <ion-icon class=\"ion-text-center\" name=\"ios-add\"></ion-icon> Change Plan\n      </ion-button>\n    </div>\n  </div>\n\n  </div>\n\n    <div *ngIf =\"has_subscribed == undefined\">\n      <div style=\"text-align: center; margin-top: 20%\">\n    <h4>You are currently not on any Subscription or Commission plan</h4>\n\n    <p>\n      This means you cannot go   <span style=\"color: #002311\">ONLINE </span>and make some money. Please choose a plan and enjoy driving!\n    </p>\n\n    <div class=\"centerDiv2\">\n      <ion-button (click)=\"chooseSubscriptiontype()\" slot=\"end\">\n        <ion-icon class=\"ion-text-center\" name=\"ios-add\"></ion-icon> Choose Subscription or Commission Plan\n      </ion-button>\n    </div>\n  </div>\n\n  </div> -->\n\n</ion-content>\n\n<ion-footer>\n  <img src=\"/assets/img/payment.png\" />\n</ion-footer>\n"

/***/ }),

/***/ "./src/app/pages/wallet/wallet.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/wallet/wallet.module.ts ***!
  \***********************************************/
/*! exports provided: WalletPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPageModule", function() { return WalletPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _wallet_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./wallet.page */ "./src/app/pages/wallet/wallet.page.ts");
/* harmony import */ var angular4_paystack__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! angular4-paystack */ "./node_modules/angular4-paystack/fesm2015/angular4-paystack.js");








const routes = [
    {
        path: "",
        component: _wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"],
    },
];
let WalletPageModule = class WalletPageModule {
};
WalletPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            angular4_paystack__WEBPACK_IMPORTED_MODULE_7__["Angular4PaystackModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_wallet_page__WEBPACK_IMPORTED_MODULE_6__["WalletPage"]],
    })
], WalletPageModule);



/***/ }),

/***/ "./src/app/pages/wallet/wallet.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/wallet/wallet.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".btn-primary {\n  border: 0;\n  border-radius: 6px;\n  font-family: inherit;\n  font-style: inherit;\n  font-variant: inherit;\n  line-height: 1;\n  text-transform: none;\n  cursor: pointer;\n  -webkit-appearance: button;\n  background-color: #000000 !important;\n  --background-color: #000000 !important;\n  color: #fbb91d !important;\n  height: 40px;\n}\n\n.test {\n  text-align: center;\n}\n\nion-content button.btn-primary2 {\n  border-radius: 17px;\n  --ion-background-color:#b72569 !important;\n  background-color: #fbb91d !important;\n  font-size: 17px !important;\n  line-height: 4;\n  padding-left: 20px;\n  padding-right: 20px;\n  color: black;\n}\n\n.btn-primary2 {\n  border-radius: 17px;\n  background-color: #fbb91d !important;\n  font-size: 17px !important;\n  line-height: 4;\n  padding-left: 20px;\n  padding-right: 20px;\n  color: black;\n}\n\nbtn-primary2 {\n  border-radius: 17px;\n  background-color: #fbb91d !important;\n  font-size: 17px !important;\n  line-height: 4;\n  padding-left: 20px;\n  padding-right: 20px;\n  color: black;\n}\n\nbutton {\n  border: 0;\n  border-radius: 6px;\n  font-family: inherit;\n  font-style: inherit;\n  font-variant: inherit;\n  line-height: 1;\n  text-transform: none;\n  cursor: pointer;\n  -webkit-appearance: button;\n  border-radius: 17px;\n  background-color: #fbb91d !important;\n  font-size: 17px !important;\n  line-height: 4;\n  padding-left: 20px;\n  padding-right: 20px;\n  color: black;\n}\n\n.centerDiv2 {\n  position: relative;\n  text-align: center;\n  margin-top: 5%;\n}\n\nion-footer {\n  background: transparent !important;\n}\n\n.footer-md:before {\n  left: 0;\n  top: -2px;\n  bottom: auto;\n  background-position: left 0 top 0;\n  position: absolute;\n  width: 100%;\n  height: 2px;\n  background-image: url();\n  background-repeat: repeat-x;\n  content: \"\" !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvd2FsbGV0L3dhbGxldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxvQ0FBQTtFQUNBLHNDQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FDQ0Y7O0FERUE7RUFDRSxrQkFBQTtBQ0NGOztBREdBO0VBQ0ksbUJBQUE7RUFDRCx5Q0FBQTtFQUNELG9DQUFBO0VBQ0UsMEJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUNBSjs7QURJQTtFQUNJLG1CQUFBO0VBRUYsb0NBQUE7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0ZKOztBRGlCRTtFQUNFLG1CQUFBO0VBRUYsb0NBQUE7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ2ZKOztBRGtCQTtFQUNFLFNBQUE7RUFDQSxrQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNFLG1CQUFBO0VBRUYsb0NBQUE7RUFDRSwwQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ2hCSjs7QURpQ0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtBQzlCRjs7QURpQ0E7RUFDRSxrQ0FBQTtBQzlCRjs7QURpQ0E7RUFDRSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSx1QkFBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7QUM5QkYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy93YWxsZXQvd2FsbGV0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idG4tcHJpbWFyeSB7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBmb250LWZhbWlseTogaW5oZXJpdDtcbiAgZm9udC1zdHlsZTogaW5oZXJpdDtcbiAgZm9udC12YXJpYW50OiBpbmhlcml0O1xuICBsaW5lLWhlaWdodDogMTtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLnRlc3R7XG4gIHRleHQtYWxpZ246IGNlbnRlclxufVxuXG5pb24tY29udGVudHtcbmJ1dHRvbi5idG4tcHJpbWFyeTIge1xuICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNiNzI1NjkgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjojZmJiOTFkICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxN3B4ICFpbXBvcnRhbnQ7XG4gICAgbGluZS1oZWlnaHQ6IDQ7XG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgY29sb3I6IHJnYigwLCAwLCAwKTtcbiAgfSBcbn1cblxuLmJ0bi1wcmltYXJ5MiB7XG4gICAgYm9yZGVyLXJhZGl1czogMTdweDtcbiAgICAvLyAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNiNzI1NjkgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjojZmJiOTFkICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAxN3B4ICFpbXBvcnRhbnQ7XG4gICAgbGluZS1oZWlnaHQ6IDQ7XG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gICAgY29sb3I6IHJnYigwLCAwLCAwKTtcbiAgfSBcblxuICAvLyBhbmd1bGFyNC1wYXlzdGFja3tcbiAgLy8gICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gIC8vICAgLy8gLS1pb24tYmFja2dyb3VuZC1jb2xvcjojYjcyNTY5ICFpbXBvcnRhbnQ7XG4gIC8vIGJhY2tncm91bmQtY29sb3I6I2ZiYjkxZCAhaW1wb3J0YW50O1xuICAvLyAgIGZvbnQtc2l6ZTogMTdweCAhaW1wb3J0YW50O1xuICAvLyAgIGxpbmUtaGVpZ2h0OiA0O1xuICAvLyAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgLy8gICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICAvLyAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XG4gIFxuICAvLyB9XG5cbiAgYnRuLXByaW1hcnkyIHtcbiAgICBib3JkZXItcmFkaXVzOiAxN3B4O1xuICAgIC8vIC0taW9uLWJhY2tncm91bmQtY29sb3I6I2I3MjU2OSAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiNmYmI5MWQgIWltcG9ydGFudDtcbiAgICBmb250LXNpemU6IDE3cHggIWltcG9ydGFudDtcbiAgICBsaW5lLWhlaWdodDogNDtcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICBjb2xvcjogcmdiKDAsIDAsIDApO1xuICB9IFxuXG5idXR0b24ge1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG4gIGZvbnQtc3R5bGU6IGluaGVyaXQ7XG4gIGZvbnQtdmFyaWFudDogaW5oZXJpdDtcbiAgbGluZS1oZWlnaHQ6IDE7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xuICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gICAgLy8gLS1pb24tYmFja2dyb3VuZC1jb2xvcjojYjcyNTY5ICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6I2ZiYjkxZCAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMTdweCAhaW1wb3J0YW50O1xuICAgIGxpbmUtaGVpZ2h0OiA0O1xuICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICAgIGNvbG9yOiByZ2IoMCwgMCwgMCk7XG59XG5cbi8vIGJ1dHRvbiB7XG4vLyAgIGJvcmRlcjogMDtcbi8vICAgYm9yZGVyLXJhZGl1czogMDtcbi8vICAgZm9udC1mYW1pbHk6IGluaGVyaXQ7XG4vLyAgIGZvbnQtc3R5bGU6IGluaGVyaXQ7XG4vLyAgIGZvbnQtdmFyaWFudDogaW5oZXJpdDtcbi8vICAgbGluZS1oZWlnaHQ6IDE7XG4vLyAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuLy8gICBjdXJzb3I6IHBvaW50ZXI7XG4vLyAgIC13ZWJraXQtYXBwZWFyYW5jZTogYnV0dG9uO1xuLy8gICAtLWJhY2tncm91bmQ6ICNmYmI5MWQgIWltcG9ydGFudDtcbi8vICAgbWFyZ2luOiAxNnB4O1xuLy8gfVxuXG4uY2VudGVyRGl2MiB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tdG9wOiA1JTtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59XG5cbi5mb290ZXItbWQ6YmVmb3JlIHtcbiAgbGVmdDogMDtcbiAgdG9wOiAtMnB4O1xuICBib3R0b206IGF1dG87XG4gIGJhY2tncm91bmQtcG9zaXRpb246IGxlZnQgMCB0b3AgMDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAycHg7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgpO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LXg7XG4gIGNvbnRlbnQ6IFwiXCIgIWltcG9ydGFudDtcbn1cbiIsIi5idG4tcHJpbWFyeSB7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBmb250LWZhbWlseTogaW5oZXJpdDtcbiAgZm9udC1zdHlsZTogaW5oZXJpdDtcbiAgZm9udC12YXJpYW50OiBpbmhlcml0O1xuICBsaW5lLWhlaWdodDogMTtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDAgIWltcG9ydGFudDtcbiAgLS1iYWNrZ3JvdW5kLWNvbG9yOiAjMDAwMDAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLnRlc3Qge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbmlvbi1jb250ZW50IGJ1dHRvbi5idG4tcHJpbWFyeTIge1xuICBib3JkZXItcmFkaXVzOiAxN3B4O1xuICAtLWlvbi1iYWNrZ3JvdW5kLWNvbG9yOiNiNzI1NjkgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZiYjkxZCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE3cHggIWltcG9ydGFudDtcbiAgbGluZS1oZWlnaHQ6IDQ7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG4uYnRuLXByaW1hcnkyIHtcbiAgYm9yZGVyLXJhZGl1czogMTdweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZiYjkxZCAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDE3cHggIWltcG9ydGFudDtcbiAgbGluZS1oZWlnaHQ6IDQ7XG4gIHBhZGRpbmctbGVmdDogMjBweDtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgY29sb3I6IGJsYWNrO1xufVxuXG5idG4tcHJpbWFyeTIge1xuICBib3JkZXItcmFkaXVzOiAxN3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiOTFkICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTdweCAhaW1wb3J0YW50O1xuICBsaW5lLWhlaWdodDogNDtcbiAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xuICBjb2xvcjogYmxhY2s7XG59XG5cbmJ1dHRvbiB7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBmb250LWZhbWlseTogaW5oZXJpdDtcbiAgZm9udC1zdHlsZTogaW5oZXJpdDtcbiAgZm9udC12YXJpYW50OiBpbmhlcml0O1xuICBsaW5lLWhlaWdodDogMTtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBidXR0b247XG4gIGJvcmRlci1yYWRpdXM6IDE3cHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYmI5MWQgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxN3B4ICFpbXBvcnRhbnQ7XG4gIGxpbmUtaGVpZ2h0OiA0O1xuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG4gIGNvbG9yOiBibGFjaztcbn1cblxuLmNlbnRlckRpdjIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogNSU7XG59XG5cbmlvbi1mb290ZXIge1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4uZm9vdGVyLW1kOmJlZm9yZSB7XG4gIGxlZnQ6IDA7XG4gIHRvcDogLTJweDtcbiAgYm90dG9tOiBhdXRvO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBsZWZ0IDAgdG9wIDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMnB4O1xuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IHJlcGVhdC14O1xuICBjb250ZW50OiBcIlwiICFpbXBvcnRhbnQ7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/wallet/wallet.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/wallet/wallet.page.ts ***!
  \*********************************************/
/*! exports provided: WalletPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalletPage", function() { return WalletPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");





let WalletPage = class WalletPage {
    constructor(navCtrl, alertCtrl, ph, eventProvider) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.ph = ph;
        this.eventProvider = eventProvider;
        this.ammount = 0;
        this.funds = 0;
        this.public_key = "pk_live_caabde47a485606dc025e27220d3c03548aa40f2"; //Put your paystack Test or Live Key here
        this.channels = ["bank", "card", "ussd", "qr", "mobile_money"]; //Paystack Payment Methods
        this.random_id = Math.floor(Date.now() / 1000); //Line to generate reference number
        this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
            // console.log("FUNDS-->" + userProfileSnapshot.val());
            this.ph.funds = userProfileSnapshot.val().funds;
            this.email = userProfileSnapshot.val().email;
            this.has_subscribed = userProfileSnapshot.val().subscribe;
            // console.log("SUBSCRIBEDD????:: " + this.has_subscribed);
        });
    }
    ionViewDidLoad() {
        this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
            // console.log("FUNDS-->" + userProfileSnapshot.val());
            this.ph.funds = userProfileSnapshot.val().funds;
            this.email = userProfileSnapshot.val().email;
            this.has_subscribed = userProfileSnapshot.val().subscribe;
            // console.log("SUBSCRIBEDD????:: " + this.has_subscribed);
        });
        this.eventProvider.getEventList().on("value", (snapshot) => {
            this.eventList = [];
            snapshot.forEach((snap) => {
                this.eventList.push({
                    id: snap.key,
                    realPrice: snap.val().realPrice,
                });
                console.log(this.eventList);
                return false;
            });
        });
        let n = [];
        for (let index = 0; index < this.eventList.length; index++) {
            const element = this.eventList[index];
            n.push(parseInt(this.eventList[index].realPrice));
            const add4 = (a, b) => a + b;
            const result4 = n.reduce(add4);
            this.mr = result4.toFixed(2);
            this.commission = Math.round(0.15 * this.mr);
            // this.commission = parseInt(commission)
        }
        console.log("TOAL COST OF EARNINGS" + this.mr);
        console.log("TOAL COST OF commission" + this.commission);
    }
    AddNow() {
        if (this.ph.card == null) {
            this.navCtrl.navigateRoot("card");
            this.ph.addedFunds = this.funds;
        }
        else {
            this.updateFunds();
        }
    }
    goBack() {
        this.navCtrl.navigateBack("home");
    }
    updateFunds() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                message: "Enter Ammount to Add To Your Funds",
                inputs: [
                    {
                        value: "Enter Here",
                    },
                ],
                buttons: [
                    {
                        text: "cancel",
                    },
                    {
                        text: "Accept",
                        handler: (data) => {
                            console.log(data);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    subscribe() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                message: "Enter Amount to Fund Your Wallet",
                inputs: [
                    {
                        value: "",
                    },
                ],
                buttons: [
                    {
                        text: "Cancel",
                    },
                    {
                        text: "Accept",
                        handler: (data) => {
                            this.funds = data[0];
                            console.log("FUNDS-->>" + this.funds * 100);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    updateFundsO() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                message: "Enter Amount to Fund Your Wallet",
                inputs: [
                    {
                        value: "",
                    },
                ],
                buttons: [
                    {
                        text: "Cancel",
                    },
                    {
                        text: "Accept",
                        handler: (data) => {
                            // tslint:disable-next-line: radix
                            this.funds = parseInt(data[0]) + parseInt(this.ph.funds);
                            console.log("FUNDS-->>" + this.funds * 100);
                            // this.ph.UpdatePaymentType(2, this.funds);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    ngOnInit() {
        console.log("INIT TRIGGERED", this.ph.id);
        this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
            this.ph.funds = userProfileSnapshot.val().funds;
            this.email = userProfileSnapshot.val().email;
        });
        this.eventProvider.getEventList().on("value", (snapshot) => {
            this.eventList = [];
            snapshot.forEach((snap) => {
                this.eventList.push({
                    id: snap.key,
                    realPrice: snap.val().realPrice,
                });
                console.log(this.eventList);
                return false;
            });
        });
        let n = [];
        for (let index = 0; index < this.eventList.length; index++) {
            const element = this.eventList[index];
            n.push(parseFloat(this.eventList[index].realPrice));
            const add4 = (a, b) => a + b;
            const result4 = n.reduce(add4);
            this.mr = result4.toFixed(2);
            this.commission = Math.round(0.15 * this.mr);
        }
        console.log("TOAL COST OF EARNINGS" + this.mr);
        console.log("TOAL COST OF commission" + this.commission);
    }
    paymentInit() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("Payment initialized");
        });
    }
    paymentDone(ref) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("RESPNSE AFTER PAYMENT" + JSON.stringify(ref));
            console.log("funding to update payment" + this.funds);
            this.ph.UpdatePaymentType(2, this.funds);
            let reference = ref.reference;
            let trans = ref.trans;
            let status = ref.status;
            let user_id = this.ph.id;
            console.log("TRans Ref" + reference);
            this.ph.newSubscription(this.funds, reference, trans, status, user_id);
        });
    }
    paymentCancel() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log("payment failed");
        });
    }
    chooseSubscriptiontype() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertCtrl.create({
                header: 'Either You pay Subscriptions or Commissions',
                // buttons: ['OK'],
                inputs: [
                    {
                        label: 'Pay Commission',
                        type: 'radio',
                        value: 'commission',
                    },
                    {
                        label: 'Pay Subscriptions',
                        type: 'radio',
                        value: 'subscriptions',
                    },
                ],
                buttons: [
                    {
                        text: "Cancel",
                        role: 'cancel',
                        handler: data => {
                        }
                    },
                    {
                        text: 'Ok',
                        handler: data => {
                            console.log("WHAT DATA COMING FROM ALERT::", data);
                            this.ph.UpdateSubscription(data).then((success) => {
                                console.log("SUBSCRIBED SUCCESSFULLY", success);
                            });
                        }
                    }
                ]
            });
            //   });
            yield alert.present();
        });
    }
};
WalletPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"] },
    { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_4__["EventService"] }
];
WalletPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-wallet",
        template: __webpack_require__(/*! raw-loader!./wallet.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/wallet/wallet.page.html"),
        styles: [__webpack_require__(/*! ./wallet.page.scss */ "./src/app/pages/wallet/wallet.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
        src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_3__["ProfileService"],
        src_app_services_event_service__WEBPACK_IMPORTED_MODULE_4__["EventService"]])
], WalletPage);



/***/ })

}]);
//# sourceMappingURL=pages-wallet-wallet-module-es2015.js.map