(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["campaign-campaign-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/campaign/campaign.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/campaign/campaign.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>Campaigns</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header no-border>\n  <ion-toolbar>\n    <ion-button fill=\"clear\" (click)=\"goBack()\" color=\"primary\">\n      <ion-icon style=\"font-size: 1.5em\" name=\"arrow-back\"></ion-icon>\n      <span style=\"margin-left: 30px; font-size: 1.4em\"\n        >Campaigns</span\n      >\n    </ion-button>\n  </ion-toolbar>\n</ion-header> \n\n<ion-content class=\"padding\" class=\"yes-scroll\">\n  <div class=\"followed-items\">\n    <ion-list class=\"ion-text-center\" class=\"padding\">\n     \n\n      <ion-card (click)=\"goto()\" *ngFor=\"let event of campaignss\">\n        <ion-item no-lines>\n          <div class=\"button_text\">\n            <p class=\"button_text\">{{todayDate | date:'mediumDate'}}</p>\n          </div>\n          <ion-badge color=\"danger\" slot=\"end\">Rush</ion-badge>\n        </ion-item>\n        <h4 class=\"dateText\"><span style=\"font-size: 34px;\n    color: black;\">{{event?.amount}}GHS</span> rush bonus</h4>\n        <ion-row>\n          <ion-list lines=\"none\">\n            <p class=\"dateText\">\n             {{event?.description}}\n            </p>\n            <div class=\"dateText\">\n            <ion-badge color=\"primary\" slot=\"end\">Trips: {{total_completedList}}/{{event?.no_trips}}</ion-badge> &nbsp;\n            <ion-badge color=\"secondary\" slot=\"end\">Hours: {{hours}}h/{{minute}}m</ion-badge>\n            </div>\n          </ion-list>\n        </ion-row>\n      </ion-card>\n      <br>\n\n      <!-- <ion-card (click)=\"goto()\">\n        <ion-item no-lines>\n          <div class=\"button_text\">\n            <p class=\"button_text\">{{todayDate | date:'mediumDate'}}</p>\n          </div>\n          <ion-badge color=\"danger\" slot=\"end\">Rush</ion-badge>\n        </ion-item>\n        <h4 class=\"dateText\"><span style=\"font-size: 34px;\n    color: black;\">10GHS</span> rush bonus</h4>\n        <ion-row>\n          <ion-list lines=\"none\">\n            <p class=\"dateText\">\n              Stay online for 2 hours, complete 3 rides, accept and complete 85%\n              of your orders.\n            </p>\n            <div class=\"dateText\">\n            <ion-badge color=\"primary\" slot=\"end\">Trips: {{total_completedList}}/{{total_trips}}</ion-badge> &nbsp;\n            <ion-badge color=\"secondary\" slot=\"end\">Hours: {{hours}}h/{{minute}}m</ion-badge>\n            </div>\n          </ion-list>\n        </ion-row>\n      </ion-card> -->\n    </ion-list>\n  </div>\n</ion-content>\n\n<ion-footer> </ion-footer>\n"

/***/ }),

/***/ "./src/app/campaign/campaign.module.ts":
/*!*********************************************!*\
  !*** ./src/app/campaign/campaign.module.ts ***!
  \*********************************************/
/*! exports provided: CampaignPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignPageModule", function() { return CampaignPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _campaign_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./campaign.page */ "./src/app/campaign/campaign.page.ts");







var routes = [
    {
        path: '',
        component: _campaign_page__WEBPACK_IMPORTED_MODULE_6__["CampaignPage"]
    }
];
var CampaignPageModule = /** @class */ (function () {
    function CampaignPageModule() {
    }
    CampaignPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_campaign_page__WEBPACK_IMPORTED_MODULE_6__["CampaignPage"]]
        })
    ], CampaignPageModule);
    return CampaignPageModule;
}());



/***/ }),

/***/ "./src/app/campaign/campaign.page.scss":
/*!*********************************************!*\
  !*** ./src/app/campaign/campaign.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-content {\n  border-bottom: 1px solid rgba(212, 212, 212, 0.93) !important;\n}\nion-content .drive {\n  color: #f7f7f7;\n  font-family: \"Franklin Gothic Medium\", \"Arial Narrow\", Arial, sans-serif;\n}\nion-content .price {\n  color: lime;\n}\nion-content .date {\n  color: #fbb91d;\n}\nion-content .dateText {\n  text-align: left;\n  padding-left: 5%;\n}\nion-content .destination {\n  color: #f6fcff;\n}\nion-content .location {\n  color: whitesmoke;\n}\nion-content ion-item {\n  margin-top: 24px;\n  box-shadow: none;\n}\n.hists {\n  border: 1px solid;\n  margin: 8px;\n  border-radius: 20px;\n  --background: rgb(28 31 70);\n  color: light;\n}\n.price {\n  color: #006e00;\n}\nion-footer {\n  border-top: 1.4px solid rgba(212, 212, 212, 0.93);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9jYW1wYWlnbi9jYW1wYWlnbi5wYWdlLnNjc3MiLCJzcmMvYXBwL2NhbXBhaWduL2NhbXBhaWduLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQWdDRSw2REFBQTtBQzlCRjtBRERFO0VBQ0UsY0FBQTtFQUNBLHdFQUFBO0FDR0o7QURBRTtFQUNFLFdBQUE7QUNFSjtBRENFO0VBQ0UsY0FBQTtBQ0NKO0FERUU7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FDQUo7QURHRTtFQUNFLGNBQUE7QUNESjtBRElFO0VBQ0UsaUJBQUE7QUNGSjtBREtFO0VBQ0UsZ0JBQUE7RUFFQSxnQkFBQTtBQ0pKO0FEU0E7RUFDQSxpQkFBQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0YsWUFBQTtBQ05GO0FEU0E7RUFDRSxjQUFBO0FDTkY7QURTQTtFQUNFLGlEQUFBO0FDTkYiLCJmaWxlIjoic3JjL2FwcC9jYW1wYWlnbi9jYW1wYWlnbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gIC5kcml2ZSB7XG4gICAgY29sb3I6ICNmN2Y3Zjc7XG4gICAgZm9udC1mYW1pbHk6IFwiRnJhbmtsaW4gR290aGljIE1lZGl1bVwiLCBcIkFyaWFsIE5hcnJvd1wiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgfVxuXG4gIC5wcmljZSB7XG4gICAgY29sb3I6IHJnYigwLCAyNTUsIDApO1xuICB9XG5cbiAgLmRhdGUge1xuICAgIGNvbG9yOiAjZmJiOTFkO1xuICB9XG5cbiAgLmRhdGVUZXh0e1xuICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgcGFkZGluZy1sZWZ0OiA1JTtcbiAgfVxuXG4gIC5kZXN0aW5hdGlvbiB7XG4gICAgY29sb3I6IHJnYigyNDYsIDI1MiwgMjU1KTtcbiAgfVxuXG4gIC5sb2NhdGlvbiB7XG4gICAgY29sb3I6IHdoaXRlc21va2U7XG4gIH1cblxuICBpb24taXRlbSB7XG4gICAgbWFyZ2luLXRvcDogMjRweDtcbiAgICAvLyBtYXJnaW4tYm90dG9tOiAxNXB4O1xuICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gIH1cbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45MykgIWltcG9ydGFudDtcbn1cblxuLmhpc3RzIHtcbmJvcmRlcjogMXB4IHNvbGlkO1xuICAgIG1hcmdpbjogOHB4O1xuICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiByZ2IoMjggMzEgNzApO1xuICBjb2xvcjogbGlnaHQ7XG59XG5cbi5wcmljZSB7XG4gIGNvbG9yOiByZ2IoMCwgMTEwLCAwKTtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJvcmRlci10b3A6IDEuNHB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG59XG4iLCJpb24tY29udGVudCB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2JhKDIxMiwgMjEyLCAyMTIsIDAuOTMpICFpbXBvcnRhbnQ7XG59XG5pb24tY29udGVudCAuZHJpdmUge1xuICBjb2xvcjogI2Y3ZjdmNztcbiAgZm9udC1mYW1pbHk6IFwiRnJhbmtsaW4gR290aGljIE1lZGl1bVwiLCBcIkFyaWFsIE5hcnJvd1wiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbn1cbmlvbi1jb250ZW50IC5wcmljZSB7XG4gIGNvbG9yOiBsaW1lO1xufVxuaW9uLWNvbnRlbnQgLmRhdGUge1xuICBjb2xvcjogI2ZiYjkxZDtcbn1cbmlvbi1jb250ZW50IC5kYXRlVGV4dCB7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHBhZGRpbmctbGVmdDogNSU7XG59XG5pb24tY29udGVudCAuZGVzdGluYXRpb24ge1xuICBjb2xvcjogI2Y2ZmNmZjtcbn1cbmlvbi1jb250ZW50IC5sb2NhdGlvbiB7XG4gIGNvbG9yOiB3aGl0ZXNtb2tlO1xufVxuaW9uLWNvbnRlbnQgaW9uLWl0ZW0ge1xuICBtYXJnaW4tdG9wOiAyNHB4O1xuICBib3gtc2hhZG93OiBub25lO1xufVxuXG4uaGlzdHMge1xuICBib3JkZXI6IDFweCBzb2xpZDtcbiAgbWFyZ2luOiA4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIC0tYmFja2dyb3VuZDogcmdiKDI4IDMxIDcwKTtcbiAgY29sb3I6IGxpZ2h0O1xufVxuXG4ucHJpY2Uge1xuICBjb2xvcjogIzAwNmUwMDtcbn1cblxuaW9uLWZvb3RlciB7XG4gIGJvcmRlci10b3A6IDEuNHB4IHNvbGlkIHJnYmEoMjEyLCAyMTIsIDIxMiwgMC45Myk7XG59Il19 */"

/***/ }),

/***/ "./src/app/campaign/campaign.page.ts":
/*!*******************************************!*\
  !*** ./src/app/campaign/campaign.page.ts ***!
  \*******************************************/
/*! exports provided: CampaignPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CampaignPage", function() { return CampaignPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/language.service */ "./src/app/services/language.service.ts");
/* harmony import */ var src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/pop-up.service */ "./src/app/services/pop-up.service.ts");
/* harmony import */ var src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/profile.service */ "./src/app/services/profile.service.ts");
/* harmony import */ var src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/event.service */ "./src/app/services/event.service.ts");
/* harmony import */ var src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/settings.service */ "./src/app/services/settings.service.ts");








var CampaignPage = /** @class */ (function () {
    function CampaignPage(navCtrl, lp, settings, pop, load, profile, alert, eventProvider) {
        this.navCtrl = navCtrl;
        this.lp = lp;
        this.settings = settings;
        this.pop = pop;
        this.load = load;
        this.profile = profile;
        this.alert = alert;
        this.eventProvider = eventProvider;
        this.math = Math;
        this.float = parseFloat;
        var d = new Date();
        this.today = d.getDay();
        this.todayDate = Date.now();
        this.retriveCampaign();
    }
    CampaignPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.retriveCampaign();
        var name = this.profile.name;
        var n = [];
        var sum = 0;
        this.eventProvider.getEventList().on("value", function (snapshot) {
            _this.eventList = [];
            snapshot.forEach(function (snap) {
                _this.eventList.push({
                    id: snap.key,
                    name: snap.val().name,
                    price: snap.val().price -
                        _this.check(snap.val().price, snap.val().surcharge || []),
                    date: snap.val().date,
                    location: snap.val().location,
                    destination: snap.val().destination,
                    tip: snap.val().tip -
                        _this.checkMe(snap.val().tip, snap.val().surcharge || []),
                    upvote: snap.val().upvote || 0,
                    downvote: snap.val().downvote || 0,
                    toll: snap.val().tolls,
                    surcharge: snap.val().surcharge,
                    realPrice: snap.val().realPrice,
                    osc: snap.val().osc,
                });
                _this.eventList.sort();
                _this.eventList.reverse();
                console.log(_this.eventList);
                _this.total_trips = _this.eventList.length;
                console.log("total Trips", _this.total_trips);
                return false;
            });
        });
        this.eventProvider.getCompleted().orderByChild("name").equalTo(this.profile.name).on("value", function (snapshot) {
            _this.completedList = [];
            snapshot.forEach(function (snap) {
                _this.completedList.push({
                    id: snap.key,
                    name: snap.val().name,
                });
                console.log(_this.completedList);
                _this.total_completedList = _this.completedList.length;
                console.log("total Completed", _this.total_completedList);
            });
        });
        // this.presentRouteLoader(this.lp.translate()[0].c4);
        this.profile.getUserProfile().on("value", function (userProfileSnapshot) {
            _this.earnings = Math.floor(userProfileSnapshot.val().earnings);
            _this.hours = userProfileSnapshot.val().hours;
            _this.minute = userProfileSnapshot.val().minutes;
            var secs = userProfileSnapshot.val().seconds;
            console.log(_this.hours + "h : " + _this.minute + "m : " +
                secs + "s");
        });
        for (var index = 0; index < this.eventList.length; index++) {
            var element = this.eventList[index];
            // console.log(element.price.replace(/[^\d\.]/g, ''));
            // n.push(parseFloat(this.eventList[index].price.replace(/[^\d\.]/g, '')));
            n.push(parseFloat(this.eventList[index].price));
            var add4 = function (a, b) { return a + b; };
            var result4 = n.reduce(add4);
            this.mr = result4.toFixed(2);
            // sum += this.eventList[index].price + element.tip || 0;
        }
        console.log(this.mr);
    };
    // calculateEarningsForTheDay() {
    //   var date_result = "";
    //   var d = new Date();
    //   date_result += d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
    //   date_result;
    //   console.log("DATE TODAY IS", date_result)
    //   this.eventProvider.getEventListToday(date_result).on("value", (snapshot) => {
    //     console.log("<<!!!--------- SNAPSHOTTT:: ------>>", snapshot)
    //     snapshot.forEach((snap) => {
    //       console.log("THE VALUSE FOR TODAY::", snap.val())
    //     })
    //   })
    // }
    CampaignPage.prototype.goBack = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                this.navCtrl.navigateRoot("home");
                return [2 /*return*/];
            });
        });
    };
    CampaignPage.prototype.checkMe = function (price, surcharge) {
        var _this = this;
        var g = [];
        var b = [];
        var k = [];
        var o = [];
        var c = [];
        var n = [];
        console.log(surcharge);
        surcharge.forEach(function (element) {
            _this.riderpaid = parseFloat(price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    var nb = element.price / 100;
                    console.log(nb * _this.riderpaid);
                    var fo = nb * _this.riderpaid;
                    n.push(fo);
                    var add2 = function (a, b) { return a + b; };
                    var result2 = n.reduce(add2);
                    _this.percentDriver = result2;
                    console.log((Math.floor(element.price) / 100) * _this.riderpaid);
                }
                //if flat fee
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    var add4 = function (a, b) { return a + b; };
                    var result4 = c.reduce(add4);
                    _this.flatDriver = result4;
                    console.log(result4);
                }
                _this.totalDriverSurge = _this.flatDriver + _this.percentDriver;
                console.log(_this.totalDriverSurge, _this.flatDriver, _this.percentDriver);
            }
        });
        return this.totalDriverSurge;
    };
    CampaignPage.prototype.check = function (price, surcharge) {
        var _this = this;
        var g = [];
        var b = [];
        var k = [];
        var o = [];
        var c = [];
        var n = [];
        console.log(surcharge);
        surcharge.forEach(function (element) {
            _this.wef = parseFloat(price).toFixed(2);
            //if driver
            if (element.owner == 0) {
                //if percent
                if (element.bone == 1) {
                    var nb = element.price / 100;
                    console.log(nb * _this.wef);
                    var fo = nb * _this.wef;
                    n.push(fo);
                    var add2 = function (a, b) { return a + b; };
                    var result2 = n.reduce(add2);
                    _this.percent2Driver = result2;
                    console.log((Math.floor(element.price) / 100) * _this.wef);
                }
                //if flat fee
                if (element.bone == 0) {
                    c.push(parseFloat(element.price));
                    var add4 = function (a, b) { return a + b; };
                    var result4 = c.reduce(add4);
                    _this.flat2Driver = result4;
                    console.log(result4);
                }
                _this.myDriverSurge = _this.flat2Driver + _this.percent2Driver;
                console.log(_this.myDriverSurge, _this.flatDriver, _this.percentDriver);
            }
        });
        return this.myDriverSurge;
    };
    // async presentRouteLoader(message) {
    //   const loading = await this.load.create({
    //     message: message,
    //   });
    //   await loading.present();
    //   let myInterval = setInterval(() => {
    //     if (this.eventList != null) {
    //       loading.dismiss();
    //       clearInterval(myInterval);
    //     }
    //   }, 1000);
    // }
    CampaignPage.prototype.goto = function () {
        this.navCtrl.navigateRoot(["wallet"]);
    };
    // Request(eventId){
    //   let alert = this.alert.create({
    //     title: this.lp.translate()[0].c5,
    //     buttons: [ {
    //       text: this.lp.translate()[0].c6,
    //       role: 'cancel',
    //       handler: () => {
    //       }
    //     },
    //     {
    //       text: this.lp.translate()[0].c7,
    //       handler: () => {
    //         this.eventProvider.getEventDetail(eventId).update({
    //           paid: 2,
    //         }).then((suc) =>{
    //           this.pop.alertMe('Your Request Has Been Recieved And Is Biegn Processed. This May Take Up To 30 minutes')
    //         });
    //     }
    //     },],
    //     enableBackdropDismiss: false
    //   });
    //   alert.present();
    // }
    // OpenCancelled() {
    //   this.navCtrl.navigateRoot("cancelled");
    // }
    // async Delete(eventId) {
    //   const alert = await this.alert.create({
    //     message: this.lp.translate()[0].c5,
    //     buttons: [
    //       {
    //         text: this.lp.translate()[0].c6,
    //         role: "cancel",
    //         handler: () => { },
    //       },
    //       {
    //         text: this.lp.translate()[0].c7,
    //         handler: () => {
    //           this.eventProvider
    //             .getEventDetail(eventId)
    //             .update({
    //               hidden: true,
    //             })
    //             .then((success) => { });
    //         },
    //       },
    //     ],
    //     backdropDismiss: false,
    //   });
    //   alert.present();
    // }
    CampaignPage.prototype.ngOnInit = function () {
        this.todayDate = Date.now();
        this.retriveCampaign();
    };
    CampaignPage.prototype.retriveCampaign = function () {
        var _this = this;
        this.profile.retrieve_campaigns().on("value", function (snapshot) {
            _this.campaignss = [];
            snapshot.forEach(function (snap) {
                _this.campaignss.push({
                    id: snap.key,
                    description: snap.val().description,
                    amount: snap.val().amount,
                    hours: snap.val().hours,
                    date: snap.val().date,
                    no_trips: snap.val().no_trips,
                });
                _this.campaignss.sort();
                _this.campaignss.reverse();
                console.log(_this.campaignss);
                _this.total_trips = _this.campaignss.length;
                console.log("Campaings Trips", _this.total_trips);
                return false;
            });
        });
    };
    CampaignPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"] },
        { type: src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_7__["SettingsService"] },
        { type: src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__["PopUpService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] },
        { type: src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
        { type: src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__["EventService"] }
    ]; };
    CampaignPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-campaign',
            template: __webpack_require__(/*! raw-loader!./campaign.page.html */ "./node_modules/raw-loader/index.js!./src/app/campaign/campaign.page.html"),
            styles: [__webpack_require__(/*! ./campaign.page.scss */ "./src/app/campaign/campaign.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_language_service__WEBPACK_IMPORTED_MODULE_3__["LanguageService"],
            src_app_services_settings_service__WEBPACK_IMPORTED_MODULE_7__["SettingsService"],
            src_app_services_pop_up_service__WEBPACK_IMPORTED_MODULE_4__["PopUpService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"],
            src_app_services_profile_service__WEBPACK_IMPORTED_MODULE_5__["ProfileService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"],
            src_app_services_event_service__WEBPACK_IMPORTED_MODULE_6__["EventService"]])
    ], CampaignPage);
    return CampaignPage;
}());



/***/ })

}]);
//# sourceMappingURL=campaign-campaign-module-es5.js.map