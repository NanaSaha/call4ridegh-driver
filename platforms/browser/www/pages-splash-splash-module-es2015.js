(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-splash-splash-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/splash/splash.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/splash/splash.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-content>\n  <div class=\"container ion-text-center\">\n    <div class=\"vertical-center\">\n      <img src=\"/assets/img/eklogo.png\" class=\"logoSize\" />\n    </div>\n  </div>\n</ion-content> -->\n\n<ion-content>\n  <div class=\"back\">\n    <ion-row id=\"test\">\n      <ion-col size=\"6\"> <div class=\"logoSize\">\n          <img src=\"/assets/img/1.png\"  />\n      </div></ion-col>\n      <ion-col size=\"6\"> <div class=\"logoSize2\">\n          <img src=\"/assets/img/3.png\"  />\n      </div></ion-col>\n         <ion-col size=\"12\">   <div class=\"logoSize5\">\n          <img src=\"/assets/img/5.png\"  />\n      </div></ion-col>\n       <ion-col size=\"6\">   <div class=\"logoSize3\">\n          <img src=\"/assets/img/4.png\"  />\n      </div></ion-col>\n      <ion-col size=\"6\">  <div class=\"logoSize4\">\n          <img src=\"/assets/img/6.png\"  />\n      </div></ion-col>\n    </ion-row>\n\n      <div class=\"pad\" id=\"move-down2\">\n        <h1 class=\"font-style2\">Earn without hassle</h1>\n        <h4 class=\"font-style\">Enjoy earning when driving with Call4Ride.</h4>\n      </div>\n        <div class=\"move-down\">\n        <p class=\"pad\" style=\"text-align: center\">\n          <ion-button color=\"dark\" class=\"button_style\" (click)=\"login()\">\n            Get Started\n            <ion-icon color=\"light\" name=\"arrow-forward\" slot=\"end\"></ion-icon>\n          </ion-button>\n        </p>\n      </div>\n  \n     \n     \n  </div> \n\n  <!-- <div class=\"system\">\n\t<div class=\"system__orbit system__orbit--sun\">\n\t\t<img src=\"https://www.dropbox.com/s/g02pto204mz1ywi/sun.png?raw=1\" alt=\"Sun\" class=\"system__icon system__icon--sun\">\n\t</div>\n\t\n\t<div class=\"system__orbit system__orbit--mercury\">\n\t\t<div class=\"system__planet system__planet--mercury\">\n\t\t\t<img src=\"https://www.dropbox.com/s/2o38602cmwhhdi1/mercury.png?raw=1\" >\n\t\t</div>\n\t</div>\n\t<div class=\"system__orbit system__orbit--venus\">\n\t\t<div class=\"system__planet system__planet--venus\">\n\t\t\t<img src=\"https://www.dropbox.com/s/wvictuysutiirho/venus.png?raw=1\" >\n\t\t</div>\n\t</div>\n\n</div> -->\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/splash/splash.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/splash/splash.module.ts ***!
  \***********************************************/
/*! exports provided: SplashPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashPageModule", function() { return SplashPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _splash_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./splash.page */ "./src/app/pages/splash/splash.page.ts");







const routes = [
    {
        path: '',
        component: _splash_page__WEBPACK_IMPORTED_MODULE_6__["SplashPage"]
    }
];
let SplashPageModule = class SplashPageModule {
};
SplashPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_splash_page__WEBPACK_IMPORTED_MODULE_6__["SplashPage"]]
    })
], SplashPageModule);



/***/ }),

/***/ "./src/app/pages/splash/splash.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/splash/splash.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".back {\n  background-image: url(/assets/img/circle.png);\n  background-color: white;\n  height: 100%;\n  background-position: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  align-content: center;\n  text-align: center;\n}\n\n.pad {\n  text-align: center;\n}\n\n.font-style {\n  color: #050505;\n  font-size: 19px;\n  text-align: center;\n  font-weight: 100;\n  line-height: 1.5;\n  letter-spacing: 1px;\n}\n\n.font-style2 {\n  color: #090909;\n  font-size: 32px;\n  text-align: center;\n  font-weight: 900;\n  letter-spacing: 1px;\n}\n\n.move-down {\n  padding-top: 15%;\n}\n\n#move-down2 {\n  padding-top: 0%;\n}\n\n.button_style {\n  text-align: center;\n  border-radius: 60px;\n  height: 40px;\n  width: 90%;\n}\n\n.logoSize {\n  text-align: center;\n  width: 70%;\n  align-content: center;\n  margin-left: 3%;\n  padding-top: 5%;\n  margin-bottom: 0%;\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 2s;\n          animation-duration: 2s;\n}\n\n.logoSize2 {\n  text-align: center;\n  width: 70%;\n  align-content: center;\n  margin-left: 27%;\n  padding-top: 3%;\n  margin-bottom: 0%;\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 3s;\n          animation-duration: 3s;\n}\n\n.logoSize3 {\n  text-align: center;\n  width: 90%;\n  align-content: center;\n  margin-left: 1%;\n  margin-top: -15%;\n  margin-bottom: 5%;\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 4s;\n          animation-duration: 4s;\n}\n\n.logoSize4 {\n  text-align: center;\n  width: 90%;\n  align-content: center;\n  margin-left: 0%;\n  margin-top: 5%;\n  margin-bottom: 5%;\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 5s;\n          animation-duration: 5s;\n}\n\n.logoSize5 {\n  text-align: center;\n  width: 50%;\n  align-content: center;\n  margin-left: 26%;\n  margin-top: -16%;\n  /* margin-bottom: 5%; */\n  position: relative;\n  -webkit-animation: spin 2s ease-in-out alternate infinite;\n  -webkit-animation-duration: 6s;\n          animation-duration: 6s;\n}\n\n@-webkit-keyframes spin {\n  from {\n    left: 0px;\n    top: 0px;\n  }\n  to {\n    left: 0px;\n    top: 20px;\n  }\n}\n\n@-webkit-keyframes drawline {\n  0% {\n    height: 0;\n  }\n  50% {\n    border-left: 1px solid red;\n  }\n  100% {\n    height: 5vh;\n    border-left: 1px solid #324c77;\n  }\n}\n\n@keyframes drawline {\n  0% {\n    height: 0;\n  }\n  50% {\n    border-left: 1px solid red;\n  }\n  100% {\n    height: 5vh;\n    border-left: 1px solid #324c77;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9zYWhhL0lvbmljL2M0ci1naC9Ecml2ZXIvc3JjL2FwcC9wYWdlcy9zcGxhc2gvc3BsYXNoLnBhZ2Uuc2NzcyIsInNyYy9hcHAvcGFnZXMvc3BsYXNoL3NwbGFzaC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUNBO0VBQ0UsNkNBQUE7RUFDQSx1QkFBQTtFQUVBLFlBQUE7RUFDQSwyQkFBQTtFQUNBLDRCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0FDekNGOztBRDZDQTtFQUNFLGtCQUFBO0FDMUNGOztBRDZDQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUVBLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQ0Y7O0FEOENBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUMzQ0Y7O0FEOENBO0VBQ0UsZ0JBQUE7QUMzQ0Y7O0FEOENBO0VBQ0UsZUFBQTtBQzNDRjs7QUQ2Q0E7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUMxQ0Y7O0FENkNBO0VBQ0Esa0JBQUE7RUFDSSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFFRyx5REFBQTtFQUNGLDhCQUFBO1VBQUEsc0JBQUE7QUMzQ0w7O0FEOENBO0VBQ0ssa0JBQUE7RUFDRCxVQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0UseURBQUE7RUFDRCw4QkFBQTtVQUFBLHNCQUFBO0FDM0NMOztBRGdEQTtFQUNDLGtCQUFBO0VBQ0csVUFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNFLHlEQUFBO0VBQ0QsOEJBQUE7VUFBQSxzQkFBQTtBQzdDTDs7QURpREE7RUFDQyxrQkFBQTtFQUNHLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNFLHlEQUFBO0VBQ0QsOEJBQUE7VUFBQSxzQkFBQTtBQzlDTDs7QURtREE7RUFDSyxrQkFBQTtFQUNELFVBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLGtCQUFBO0VBQ0UseURBQUE7RUFDRCw4QkFBQTtVQUFBLHNCQUFBO0FDaERMOztBRHNGQTtFQUNHO0lBQ0csU0FBQTtJQUNBLFFBQUE7RUNuRko7RURzRkM7SUFDRyxTQUFBO0lBQ0EsU0FBQTtFQ3BGSjtBQUNGOztBRHlGQTtFQUVJO0lBQ0UsU0FBQTtFQ3hGSjtFRDBGRztJQUNBLDBCQUFBO0VDeEZIO0VEMEZFO0lBQ0ssV0FBQTtJQUNKLDhCQUFBO0VDeEZIO0FBQ0Y7O0FENkVBO0VBRUk7SUFDRSxTQUFBO0VDeEZKO0VEMEZHO0lBQ0EsMEJBQUE7RUN4Rkg7RUQwRkU7SUFDSyxXQUFBO0lBQ0osOEJBQUE7RUN4Rkg7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NwbGFzaC9zcGxhc2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gaW9uLWNvbnRlbnQge1xuLy8gICAtLWJhY2tncm91bmQ6ICNmYmI5MWQ7XG4vLyB9XG4vLyAuY29udGFpbmVyIHtcbi8vICAgLXdlYmtpdC1hbmltYXRpb246IHJvdGF0ZS1jZW50ZXIgMXMgZWFzZS1pbi1vdXQgYm90aDtcbi8vICAgYW5pbWF0aW9uOiByb3RhdGUtY2VudGVyIDFzIGVhc2UtaW4tb3V0IGJvdGg7XG4vLyAgIC8vIHBvc2l0aW9uOiByZWxhdGl2ZTtcbi8vICAgaW1nIHtcbi8vICAgICBoZWlnaHQ6IDEwMHB4O1xuLy8gICAgIG1hcmdpbi10b3A6IDYwJTtcbi8vICAgfVxuLy8gfVxuXG4vLyBALXdlYmtpdC1rZXlmcmFtZXMgcm90YXRlLWNlbnRlciB7XG4vLyAgIDAlIHtcbi8vICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDApO1xuLy8gICAgIHRyYW5zZm9ybTogcm90YXRlKDApO1xuLy8gICB9XG4vLyAgIDEwMCUge1xuLy8gICAgIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbi8vICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgzNjBkZWcpO1xuLy8gICB9XG4vLyB9XG4vLyBAa2V5ZnJhbWVzIHJvdGF0ZS1jZW50ZXIge1xuLy8gICAwJSB7XG4vLyAgICAgLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSgwKTtcbi8vICAgICB0cmFuc2Zvcm06IHJvdGF0ZSgwKTtcbi8vICAgfVxuLy8gICAxMDAlIHtcbi8vICAgICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4vLyAgICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbi8vICAgfVxuLy8gfVxuLy8gLnZlcnRpY2FsLWNlbnRlciB7XG4vLyAgIG1hcmdpbjogMDtcbi8vICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICB0b3A6IDUwJTtcbi8vICAgLW1zLXRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbi8vICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuLy8gfVxuXG4uYmFjayB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL2ltZy9jaXJjbGUucG5nKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gICAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZiYjkxZDtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuXG59XG5cbi5wYWQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5mb250LXN0eWxlIHtcbiAgY29sb3I6IHJnYig1LCA1LCA1KTtcbiAgZm9udC1zaXplOiAxOXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIC8vIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICBsaW5lLWhlaWdodDogMS41O1xuICBsZXR0ZXItc3BhY2luZzogMXB4O1xufVxuXG4uZm9udC1zdHlsZTIge1xuICBjb2xvcjogcmdiKDksIDksIDkpO1xuICBmb250LXNpemU6IDMycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cblxuLm1vdmUtZG93biB7XG4gIHBhZGRpbmctdG9wOiAxNSU7XG59XG5cbiNtb3ZlLWRvd24yIHtcbiAgcGFkZGluZy10b3A6IDAlO1xufVxuLmJ1dHRvbl9zdHlsZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbiAgaGVpZ2h0OiA0MHB4O1xuICB3aWR0aDogOTAlO1xufVxuXG4ubG9nb1NpemUge1xudGV4dC1hbGlnbjogY2VudGVyO1xuICAgIHdpZHRoOiA3MCU7XG4gICAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICAgIG1hcmdpbi1sZWZ0OiAzJTtcbiAgICBwYWRkaW5nLXRvcDogNSU7XG4gICAgbWFyZ2luLWJvdHRvbTogMCU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLy8gYW5pbWF0aW9uOiBzcGluIGluZmluaXRlIGxpbmVhciBib3RoO1xuICAgICAgIC13ZWJraXQtYW5pbWF0aW9uOnNwaW4gMnMgZWFzZS1pbi1vdXQgYWx0ZXJuYXRlIGluZmluaXRlO1xuICAgICBhbmltYXRpb24tZHVyYXRpb246IDIuMHM7ICBcbn1cblxuLmxvZ29TaXplMiB7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogNzAlO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogMjclO1xuICAgIHBhZGRpbmctdG9wOiAzJTtcbiAgICBtYXJnaW4tYm90dG9tOiAwJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAtd2Via2l0LWFuaW1hdGlvbjpzcGluIDJzIGVhc2UtaW4tb3V0IGFsdGVybmF0ZSBpbmZpbml0ZTtcbiAgICAgYW5pbWF0aW9uLWR1cmF0aW9uOiAzLjBzOyAgXG4gICAgLy8gIGFuaW1hdGlvbjogZHJhd2xpbmUgaW5maW5pdGUgbGluZWFyIGJvdGg7XG4gICAgLy8gIGFuaW1hdGlvbi1kdXJhdGlvbjogMS41cztcbiAgICAvLyAgYW5pbWF0aW9uLWZpbGwtbW9kZTogZm9yd2FyZHM7XG59XG4ubG9nb1NpemUzIHtcbiB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gICAgbWFyZ2luLWxlZnQ6IDElO1xuICAgIG1hcmdpbi10b3A6IC0xNSU7XG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLXdlYmtpdC1hbmltYXRpb246c3BpbiAycyBlYXNlLWluLW91dCBhbHRlcm5hdGUgaW5maW5pdGU7XG4gICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogNC4wczsgIFxuICAgIC8vICBhbmltYXRpb246IHNwaW4gaW5maW5pdGUgbGluZWFyIGJvdGg7XG4gICAgLy8gIGFuaW1hdGlvbi1kdXJhdGlvbjogMy41cztcbn1cbi5sb2dvU2l6ZTQge1xuIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogOTAlO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogMCU7XG4gICAgbWFyZ2luLXRvcDogNSU7XG4gICAgbWFyZ2luLWJvdHRvbTogNSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLXdlYmtpdC1hbmltYXRpb246c3BpbiAycyBlYXNlLWluLW91dCBhbHRlcm5hdGUgaW5maW5pdGU7XG4gICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogNS4wczsgIFxuICAgIC8vICBhbmltYXRpb246IHNwaW4gaW5maW5pdGUgbGluZWFyIGJvdGg7XG4gICAgLy8gIGFuaW1hdGlvbi1kdXJhdGlvbjogMy41cztcbn1cblxuLmxvZ29TaXplNSB7XG4gICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICB3aWR0aDogNTAlO1xuICAgIGFsaWduLWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBtYXJnaW4tbGVmdDogMjYlO1xuICAgIG1hcmdpbi10b3A6IC0xNiU7XG4gICAgLyogbWFyZ2luLWJvdHRvbTogNSU7ICovXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgICAgLXdlYmtpdC1hbmltYXRpb246c3BpbiAycyBlYXNlLWluLW91dCBhbHRlcm5hdGUgaW5maW5pdGU7XG4gICAgIGFuaW1hdGlvbi1kdXJhdGlvbjogNi4wczsgIFxuICAgIC8vICBhbmltYXRpb246IHNwaW4gaW5maW5pdGUgbGluZWFyIGJvdGg7XG4gICAgLy8gIGFuaW1hdGlvbi1kdXJhdGlvbjogMy41cztcbn1cblxuI292ZXJsYXkge1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gXG4gIC8vIHdpZHRoOiAxMDAlO1xuICAvLyBoZWlnaHQ6IDEwMCU7XG4gIC8vIHRvcDogMDtcbiAgLy8gbGVmdDogMDtcbiAgLy8gcmlnaHQ6IDA7XG4gIC8vIGJvdHRvbTogMDtcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMGI4O1xuICAvLyB6LWluZGV4OiAxMDtcbiAgLy8gY3Vyc29yOiBwb2ludGVyO1xufVxuXG5cblxuXG5cblxuLy8gQGtleWZyYW1lcyBzcGluIHtcblxuLy8gICAgIDAlIHtcbi8vICAgICAgIG1hcmdpbi10b3A6IDBweDtcbi8vICAgICB9XG4vLyAgICAgIDUwJSB7XG4vLyAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xuLy8gICAgIH1cbi8vICAgICA5MCUge1xuLy8gICAgICAgbWFyZ2luLXRvcDogMjBweDtcbi8vICAgICB9XG4gXG4vLyB9XG5cbkAtd2Via2l0LWtleWZyYW1lcyBzcGluICB7XG4gICBmcm9tIHtcbiAgICAgIGxlZnQ6MHB4O1xuICAgICAgdG9wOjBweDtcbiAgIH1cbiAgIFxuICAgdG8ge1xuICAgICAgbGVmdDowcHg7XG4gICAgICB0b3A6MjBweDtcbiAgIH1cblxufVxuXG5cbkBrZXlmcmFtZXMgZHJhd2xpbmUge1xuXG4gICAgMCUge1xuICAgICAgaGVpZ2h0OiAwO1xuICAgIH1cbiAgICAgNTAlIHtcbiAgICAgYm9yZGVyLWxlZnQ6IDFweCBzb2xpZCByZWQ7XG4gICAgfVxuICAgIDEwMCUge1xuICAgICAgICAgaGVpZ2h0OiA1dmg7XG4gICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmdiKDUwLCA3NiwgMTE5KTtcbiAgICB9XG4gXG59XG4iLCIuYmFjayB7XG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgvYXNzZXRzL2ltZy9jaXJjbGUucG5nKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnBhZCB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmZvbnQtc3R5bGUge1xuICBjb2xvcjogIzA1MDUwNTtcbiAgZm9udC1zaXplOiAxOXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiAxMDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjU7XG4gIGxldHRlci1zcGFjaW5nOiAxcHg7XG59XG5cbi5mb250LXN0eWxlMiB7XG4gIGNvbG9yOiAjMDkwOTA5O1xuICBmb250LXNpemU6IDMycHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDFweDtcbn1cblxuLm1vdmUtZG93biB7XG4gIHBhZGRpbmctdG9wOiAxNSU7XG59XG5cbiNtb3ZlLWRvd24yIHtcbiAgcGFkZGluZy10b3A6IDAlO1xufVxuXG4uYnV0dG9uX3N0eWxlIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBib3JkZXItcmFkaXVzOiA2MHB4O1xuICBoZWlnaHQ6IDQwcHg7XG4gIHdpZHRoOiA5MCU7XG59XG5cbi5sb2dvU2l6ZSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDcwJTtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMyU7XG4gIHBhZGRpbmctdG9wOiA1JTtcbiAgbWFyZ2luLWJvdHRvbTogMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgZWFzZS1pbi1vdXQgYWx0ZXJuYXRlIGluZmluaXRlO1xuICBhbmltYXRpb24tZHVyYXRpb246IDJzO1xufVxuXG4ubG9nb1NpemUyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogNzAlO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAyNyU7XG4gIHBhZGRpbmctdG9wOiAzJTtcbiAgbWFyZ2luLWJvdHRvbTogMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgZWFzZS1pbi1vdXQgYWx0ZXJuYXRlIGluZmluaXRlO1xuICBhbmltYXRpb24tZHVyYXRpb246IDNzO1xufVxuXG4ubG9nb1NpemUzIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogOTAlO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxJTtcbiAgbWFyZ2luLXRvcDogLTE1JTtcbiAgbWFyZ2luLWJvdHRvbTogNSU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLXdlYmtpdC1hbmltYXRpb246IHNwaW4gMnMgZWFzZS1pbi1vdXQgYWx0ZXJuYXRlIGluZmluaXRlO1xuICBhbmltYXRpb24tZHVyYXRpb246IDRzO1xufVxuXG4ubG9nb1NpemU0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB3aWR0aDogOTAlO1xuICBhbGlnbi1jb250ZW50OiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAwJTtcbiAgbWFyZ2luLXRvcDogNSU7XG4gIG1hcmdpbi1ib3R0b206IDUlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzcGluIDJzIGVhc2UtaW4tb3V0IGFsdGVybmF0ZSBpbmZpbml0ZTtcbiAgYW5pbWF0aW9uLWR1cmF0aW9uOiA1cztcbn1cblxuLmxvZ29TaXplNSB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgd2lkdGg6IDUwJTtcbiAgYWxpZ24tY29udGVudDogY2VudGVyO1xuICBtYXJnaW4tbGVmdDogMjYlO1xuICBtYXJnaW4tdG9wOiAtMTYlO1xuICAvKiBtYXJnaW4tYm90dG9tOiA1JTsgKi9cbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAtd2Via2l0LWFuaW1hdGlvbjogc3BpbiAycyBlYXNlLWluLW91dCBhbHRlcm5hdGUgaW5maW5pdGU7XG4gIGFuaW1hdGlvbi1kdXJhdGlvbjogNnM7XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBzcGluIHtcbiAgZnJvbSB7XG4gICAgbGVmdDogMHB4O1xuICAgIHRvcDogMHB4O1xuICB9XG4gIHRvIHtcbiAgICBsZWZ0OiAwcHg7XG4gICAgdG9wOiAyMHB4O1xuICB9XG59XG5Aa2V5ZnJhbWVzIGRyYXdsaW5lIHtcbiAgMCUge1xuICAgIGhlaWdodDogMDtcbiAgfVxuICA1MCUge1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgcmVkO1xuICB9XG4gIDEwMCUge1xuICAgIGhlaWdodDogNXZoO1xuICAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgIzMyNGM3NztcbiAgfVxufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/splash/splash.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/splash/splash.page.ts ***!
  \*********************************************/
/*! exports provided: SplashPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashPage", function() { return SplashPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let SplashPage = class SplashPage {
    constructor(navCtrl) {
        this.navCtrl = navCtrl;
    }
    ngOnInit() { }
    login() {
        this.navCtrl.navigateRoot("login");
    }
};
SplashPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
SplashPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-splash",
        template: __webpack_require__(/*! raw-loader!./splash.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/splash/splash.page.html"),
        styles: [__webpack_require__(/*! ./splash.page.scss */ "./src/app/pages/splash/splash.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]])
], SplashPage);



/***/ })

}]);
//# sourceMappingURL=pages-splash-splash-module-es2015.js.map