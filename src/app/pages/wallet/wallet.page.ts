import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { NavController, AlertController, NavParams } from "@ionic/angular";
import { ProfileService } from "src/app/services/profile.service";
import { EventService } from "src/app/services/event.service";

@Component({
  selector: "app-wallet",
  templateUrl: "./wallet.page.html",
  styleUrls: ["./wallet.page.scss"],
  // encapsulation: ViewEncapsulation.None,
})
export class WalletPage implements OnInit {
  ammount: any = 0;
  funds: any = 0;
  public public_key = "pk_live_caabde47a485606dc025e27220d3c03548aa40f2"; //Put your paystack Test or Live Key here
  public channels = ["bank", "card", "ussd", "qr", "mobile_money"]; //Paystack Payment Methods
  public random_id = Math.floor(Date.now() / 1000); //Line to generate reference number

  commission: number;

  amount: any;
  mr: any;

  has_subscribed;
  email;
  // commission
  public eventList: Array<any>;

  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,

    public ph: ProfileService,
    public eventProvider: EventService
  ) {
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
      // console.log("FUNDS-->" + userProfileSnapshot.val());
      this.ph.funds = userProfileSnapshot.val().funds;
      this.email = userProfileSnapshot.val().email;
      this.has_subscribed = userProfileSnapshot.val().subscribe;
      // console.log("SUBSCRIBEDD????:: " + this.has_subscribed);
    });
  }

  ionViewDidLoad() {
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
      // console.log("FUNDS-->" + userProfileSnapshot.val());
      this.ph.funds = userProfileSnapshot.val().funds;
      this.email = userProfileSnapshot.val().email;
      this.has_subscribed = userProfileSnapshot.val().subscribe;
      // console.log("SUBSCRIBEDD????:: " + this.has_subscribed);
    });



    this.eventProvider.getEventList().on("value", (snapshot) => {
      this.eventList = [];

      snapshot.forEach((snap) => {
        this.eventList.push({
          id: snap.key,
         
          realPrice: snap.val().realPrice,
      
        });
 

        console.log(this.eventList);

        return false;
      });
    });

 
    let n = [];

    for (let index = 0; index < this.eventList.length; index++) {
      const element = this.eventList[index];
         n.push(parseInt(this.eventList[index].realPrice));
      const add4 = (a, b) => a + b;
      const result4 = n.reduce(add4);
      this.mr = result4.toFixed(2);

      this.commission = Math.round(0.15 * this.mr)

      // this.commission = parseInt(commission)

    }

    console.log("TOAL COST OF EARNINGS" + this.mr);
    console.log("TOAL COST OF commission" + this.commission);
  }


  AddNow() {
    if (this.ph.card == null) {
      this.navCtrl.navigateRoot("card");
      this.ph.addedFunds = this.funds;
    } else {
      this.updateFunds();
    }
  }

  goBack() {
    this.navCtrl.navigateBack("home");
  }

  async updateFunds() {
    const alert = await this.alertCtrl.create({
      message: "Enter Ammount to Add To Your Funds",
      inputs: [
        {
          value: "Enter Here",
        },
      ],
      buttons: [
        {
          text: "cancel",
        },
        {
          text: "Accept",
          handler: (data) => {
            console.log(data);
          },
        },
      ],
    });
    alert.present();
  }

  async subscribe() {
    const alert = await this.alertCtrl.create({
      message: "Enter Amount to Fund Your Wallet",
      inputs: [
        {
          value: "",
        },
      ],
      buttons: [
        {
          text: "Cancel",
        },
        {
          text: "Accept",
          handler: (data) => {
            this.funds = data[0];
            console.log("FUNDS-->>" + this.funds * 100);
          },
        },
      ],
    });
    alert.present();
  }

  async updateFundsO() {
    const alert = await this.alertCtrl.create({
      message: "Enter Amount to Fund Your Wallet",
      inputs: [
        {
          value: "",
        },
      ],
      buttons: [
        {
          text: "Cancel",
        },
        {
          text: "Accept",
          handler: (data) => {
            // tslint:disable-next-line: radix
            this.funds = parseInt(data[0]) + parseInt(this.ph.funds);
            console.log("FUNDS-->>" + this.funds * 100);

            // this.ph.UpdatePaymentType(2, this.funds);
          },
        },
      ],
    });
    alert.present();
  }

  ngOnInit() {
    console.log("INIT TRIGGERED", this.ph.id);
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
  
      this.ph.funds = userProfileSnapshot.val().funds;
      this.email = userProfileSnapshot.val().email;
    });


    this.eventProvider.getEventList().on("value", (snapshot) => {
      this.eventList = [];

      snapshot.forEach((snap) => {
        this.eventList.push({
          id: snap.key,

          realPrice: snap.val().realPrice,

        });


        console.log(this.eventList);

        return false;
      });
    });


    let n = [];

    for (let index = 0; index < this.eventList.length; index++) {
      const element = this.eventList[index];
      n.push(parseFloat(this.eventList[index].realPrice));
      const add4 = (a, b) => a + b;
      const result4 = n.reduce(add4);
      this.mr = result4.toFixed(2);

      this.commission = Math.round(0.15 * this.mr)
    }

    console.log("TOAL COST OF EARNINGS" + this.mr);
    console.log("TOAL COST OF commission" + this.commission);
  }
  async paymentInit() {
    console.log("Payment initialized");
  }

  async paymentDone(ref: any) {
    console.log("RESPNSE AFTER PAYMENT" + JSON.stringify(ref));
    console.log("funding to update payment" + this.funds);
    this.ph.UpdatePaymentType(2, this.funds);

    let reference = ref.reference;
    let trans = ref.trans;
    let status = ref.status;
    let user_id = this.ph.id;

    console.log("TRans Ref" + reference);

    this.ph.newSubscription(this.funds, reference, trans, status, user_id);
  }

  async paymentCancel() {
    console.log("payment failed");
  }


  async chooseSubscriptiontype() {
    const alert = await this.alertCtrl.create({
      header: 'Either You pay Subscriptions or Commissions',
      // buttons: ['OK'],
      inputs: [
        {
          label: 'Pay Commission',
          type: 'radio',
          value: 'commission',
        },
        {
          label: 'Pay Subscriptions',
          type: 'radio',
          value: 'subscriptions',
        },

      ],
      buttons: [
        {
          text: "Cancel",
          role: 'cancel',
          handler: data => {
          }
        },
        {
          text: 'Ok',
          handler: data => {
            console.log("WHAT DATA COMING FROM ALERT::", data)
            this.ph.UpdateSubscription(data).then((success) => {
              console.log("SUBSCRIBED SUCCESSFULLY", success);

            })


          }
        }
      ]
    });

    //   });

    await alert.present();
  }
}
