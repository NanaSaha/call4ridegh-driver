import { Component, NgZone } from "@angular/core";

import {
  Platform,
  ModalController,
  LoadingController,
  NavController,
  AlertController,
  ToastController
} from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { SettingsService } from "./services/settings.service";
import { LanguageService } from "./services/language.service";
import { AuthService } from "./services/auth.service";
import { ProfileService } from "./services/profile.service";
import { timer } from "rxjs";
import firebase from "firebase";
import { PopUpService } from "./services/pop-up.service";
import { DirectionserviceService } from "./services/directionservice.service";
import { Router } from "@angular/router";
import { Geolocation, PositionError, Geoposition } from "@ionic-native/geolocation/ngx";
@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"],
})
export class AppComponent {
  watchPositionSubscription: Geolocation;
  mapTracker: any;
  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "ios-home",
    },
    {
      title: "Profile",
      url: "/profile",
      icon: "ios-clock",
    },
    {
      title: "Earnings",
      url: "/history",
      icon: "ios-card",
    },

    {
      title: "Payments",
      url: "/wallet",
      icon: "ios-briefcase",
    },
    {
      title: "Campaign",
      url: "/campaign",
      icon: "flower",
    },
    {
      title: "Referal Code",
      url: "/refcode",
      icon: "pricetags",
    },
    {
      title: "Promo Code",
      url: "/promo",
      icon: "ribbon",
    },
    {
      title: "Documents",
      url: "/documents",
      icon: "ios-book",
    },
    {
      title: "Support",
      url: "/support",
      icon: "ios-chatbubbles",
    },
    {
      title: "Call4Ride information",
      url: "/about",
      icon: "information-circle-outline",
    },
    // {
    //   title: "Mapping",
    //   url: "/mapping",
    //   icon: "map",
    // },

    // {
    //   title: "Select Zone",
    //   url: "/zone",
    //   icon: "ios-map",
    // },
    // {
    //   title: "Profile",
    //   url: "/profile",
    //   icon: "ios-clock",
    // },

    // {
    //   title: "Settings",
    //   url: "/settings",
    //   icon: "ios-settings",
    // },

    ,
  ];
  public user: any;
  showSplash = true;
  public userProfile: any;
  public phone: any;
  hasHome: any;
  connect: any;
  last_name;
  first_name;
  picture;
  phonenum;
  unique;
  watch

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private dProvider: DirectionserviceService,
    public pop: PopUpService,
    public navCtrl: NavController,
    public zone: NgZone,
    public set: SettingsService,
    public lp: LanguageService,
    public modalCtrl: ModalController,
    public loadingCtrl: LoadingController,
    private oneSignal: OneSignal,
    public ph: ProfileService,
    public auth: AuthService,
    public alertCtrl: AlertController,
    public router: Router,
    private toastCtrl: ToastController,
    private geo: Geolocation
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.allowLocation();
      // this.oneSignal.startInit(this.set.OnesignalAppID, this.set.CloudID);
      // this.oneSignal.inFocusDisplaying(
      //   this.oneSignal.OSInFocusDisplayOption.Notification
      // );
      // "GH" + Math.floor(Date.now() / 1000) + "D";
      let currentYear = new Date().getFullYear();
      let unique1 = "GH" + currentYear + "D" + Math.floor(Date.now() / 1000);
      console.log("unq" + unique1);
      this.setupPush();
      this.getNotificationPlayerIds()
        .then((ids) => {
          console.log("first get ids: " + JSON.stringify(ids));
        })
        .catch((e) => {
          console.log("first get ids error: " + e);
        });

      this.oneSignal.setSubscription(true);

      this.splashScreen.hide();
      this.startUp();
      // this.router.navigateByUrl("splash");
      // setTimeout(() => {
      //   this.startUp();
      // }, 5000);
    });
  }

  setupPush() {
    // I recommend to put these into your environment.ts
    this.oneSignal.startInit(
      "8113f114-14de-4ee6-8ab2-4ec3cfb5a90a",
      "516821551729"
    );

    this.oneSignal.inFocusDisplaying(
      this.oneSignal.OSInFocusDisplayOption.None
    );

    // Notifcation was received in general
    this.oneSignal.handleNotificationReceived().subscribe((data) => {
      console.log("DATA FROM PUSH NOFTICA---->>" + data);
      let msg = data.payload.body;
      let title = data.payload.title;
      let additionalData = data.payload.additionalData;
      this.showAlert(title, msg, additionalData.task);
    });

    // Notification was really clicked/opened
    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      console.log("DATA FROM PUSH NOFTICA WHEN OPENDE---->>" + data);
      // Just a note that the data is a different place here!
      let additionalData = data.notification.payload.additionalData;

      this.showAlert(
        "Notification opened",
        "You already read this before",
        additionalData.task
      );
    });

    this.oneSignal.endInit();
  }

  getNotificationPlayerIds() {
    return new Promise((resolve, reject) => {
      if (this.platform.is("cordova")) {
        this.oneSignal
          .getIds()
          .then((ids) => {
            console.log("GETTING IDS RESOLVED-->>" + JSON.stringify(ids));
            resolve(ids);
          })
          .catch((e) => {
            reject(e);
          });
      }
    });
  }

  startUp() {
    console.log("profileServ PROFILE SERVICE", this.ph);

    const unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      // Check of this is a user
      if (!user) {
        this.zone.run(() => {
          this.navCtrl.navigateRoot("splash");
        });

        this.ph.loadingState = true;
        console.log(this.ph.loadingState);
        unsubscribe();
      } else {
        this.ph
          .getUserProfil()
          .child(this.ph.id)
          .on("value", (userProfileSnapshot) => {
            console.log(userProfileSnapshot.val(), this.ph.id);
            this.first_name = userProfileSnapshot.val().userInfo.first_name;
            this.last_name = userProfileSnapshot.val().userInfo.last_name;
            this.picture = userProfileSnapshot.val().userInfo.picture;
            this.phonenum = userProfileSnapshot.val().userInfo.phonenumber;
            this.unique = userProfileSnapshot.val().userInfo.unique_number;

            console.log("FRIST NAME" + this.first_name);
            console.log("unique NUMBER" + this.unique);
          });

        unsubscribe();
        let phone;
        let picture;
        let licence_pic;
        let active_User;
        let license;

        this.ph
          .getUserProfil()
          .child(this.ph.id)
          .on("value", (userProfileSnapshot) => {
            console.log(userProfileSnapshot.val(), this.ph.id);

            if (userProfileSnapshot.val() == null) {
              this.auth.logoutUser().then(() => {
                this.zone.run(() => { });
              });
            } else {
              phone = userProfileSnapshot.val().userInfo.phonenumber;
              picture = userProfileSnapshot.val().userInfo.picture;
              active_User = userProfileSnapshot.val().userInfo.active_state;
              license = userProfileSnapshot.val().userInfo.license;
              licence_pic = userProfileSnapshot.val().userInfo.license_picture;

              console.log("phone here is " + phone);
              console.log("License PIC here is " + licence_pic);
              console.log("License number here is " + license);
              console.log("Picture here is " + picture);
              console.log("Active State here is " + active_User);

              if (phone) {
                console.log("Phone Number Exist");
                if (license) {
                  console.log("license EXist");
                  if (picture != null && licence_pic != null) {
                    console.log("pic and lic pic exist");
                    if (active_User) {
                      this.ph.loadingState = true;
                      console.log("user is active");

                      this.ph
                        .getUserProfil()
                        .child(this.ph.id)
                        .child("userInfo")
                        .on("value", (userProfileSnapshot) => {
                          this.ph.earnings = userProfileSnapshot.val().earnings;
                        });

                      this.zone.run(() => {
                        this.navCtrl.navigateRoot("home");
                      });
                    } else {
                      this.zone.run(() => {
                        //this.navCtrl.navigateRoot("waiting"); Will bring it back to waiting after test
                        this.navCtrl.navigateRoot("home");
                      });
                      this.ph.loadingState = true;
                      console.log(this.ph.loadingState);
                      console.log("user is not active");
                    }
                  } else {
                    this.zone.run(() => {
                      console.log("pic and lic pic DOES NOT exist");
                      this.navCtrl.navigateRoot("addphotoinfo");
                    });
                    console.log(this.ph.loadingState);
                    this.ph.loadingState = true;
                  }
                } else {
                  this.zone.run(() => {
                    this.navCtrl.navigateRoot("more-info");
                    
                    console.log("license number DOESNT EXist");
                  });
                  console.log(this.ph.loadingState);
                  this.ph.loadingState = true;
                  console.log("license is not there");
                }
              } else {
                this.zone.run(() => {
                  console.log("Phone Number DOESNT Exist");
                  this.navCtrl.navigateRoot("more-info");
                });
                this.ph.loadingState = true;
                console.log(this.ph.loadingState);
              }
            }
            this.ph.getUserProfil().child(this.ph.id).off("value");
          });
      }
    });
  }

  menuOpened() {
    console.log("---- WHEN MENU IS OPENED----");
    this.ph
      .getUserProfil()
      .child(this.ph.id)
      .on("value", (userProfileSnapshot) => {
        console.log(userProfileSnapshot.val(), this.ph.id);
        this.first_name = userProfileSnapshot.val().userInfo.first_name;
        this.last_name = userProfileSnapshot.val().userInfo.last_name;
        this.picture = userProfileSnapshot.val().userInfo.picture;
        this.phonenum = userProfileSnapshot.val().userInfo.phonenumber;
        this.unique = userProfileSnapshot.val().userInfo.unique_number;

        console.log("FRIST NAME" + this.first_name);
        console.log("unique NUMBER" + this.unique);
      });
  }

  menuClosed() { }

  async showAlert(title, msg, task) {
    const alert = await this.alertCtrl.create({
      header: title,
      subHeader: msg,
      buttons: [
        {
          text: `Action: ${task}`,
          handler: () => {
            // E.g: Navigate to a specific screen
          },
        },
      ],
    });
    alert.present();
  }

  goProfile() {
    this.router.navigate(["profile"]);
  }

  allowLocation() {
    // this.watchPositionSubscription = navigator.geolocation;
    // this.mapTracker = this.watchPositionSubscription.watchPosition((position) => {
    //   console.log("position callback", position);


    // },
    //   (error) => {

    //     this.presentToast("Your GPS is off.Please turn it on");
    //     console.log(error);

    //   }, {
    //   enableHighAccuracy: true,
    // });


    this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);
        console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);

        console.log("Location is ready!--location ");


      } else {
        var positionError = (position as PositionError);
        console.log('Error ' + positionError.code + ': ' + positionError.message);

        // this.presentToast("Enable your location. Finding it difficult to get your current location. Please wait..");

      }
    });
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 3000,
      position: "bottom",
    });

    toast.onDidDismiss();

    toast.present();
  }
}
