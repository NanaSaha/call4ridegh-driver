import { Component, OnInit } from "@angular/core";
import {
  NavController,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { LanguageService } from "src/app/services/language.service";
import { PopUpService } from "src/app/services/pop-up.service";
import { ProfileService } from "src/app/services/profile.service";
import { EventService } from "src/app/services/event.service";
import { SettingsService } from "src/app/services/settings.service";

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.page.html',
  styleUrls: ['./campaign.page.scss'],
})
export class CampaignPage implements OnInit {

  public eventList: Array<any>;
  public earnings: any;
  today: number;
  math: any = Math;
  public float: any = parseFloat;
  totalDriverSurge: any;
  flatDriver: any;
  percentDriver: any;
  riderpaid: any;
  wef: any;
  myDriverSurge: any;
  flat2Driver: any;
  percent2Driver: any;
  mr: any;
  todayDate;
  hours;
  minute;
  total_trips;
  total_completedList;
  completedList;
  campaignss
  constructor(
    public navCtrl: NavController,
    public lp: LanguageService,
    public settings: SettingsService,
    public pop: PopUpService,
    public load: LoadingController,
    public profile: ProfileService,
    public alert: AlertController,
    public eventProvider: EventService
  ) {
    let d = new Date();
    this.today = d.getDay();

    this.todayDate = Date.now()
    this.retriveCampaign()
  }

  ionViewDidEnter() {

    this.retriveCampaign()

    let name = this.profile.name
  
    let n = [];
    var sum = 0;

  


    this.eventProvider.getEventList().on("value", (snapshot) => {
      this.eventList = [];

      snapshot.forEach((snap) => {
        this.eventList.push({
          id: snap.key,
          name: snap.val().name,
          price:
            snap.val().price -
            this.check(snap.val().price, snap.val().surcharge || []),
          date: snap.val().date,
          location: snap.val().location,
          destination: snap.val().destination,
          tip:
            snap.val().tip -
            this.checkMe(snap.val().tip, snap.val().surcharge || []),
          upvote: snap.val().upvote || 0,
          downvote: snap.val().downvote || 0,
          toll: snap.val().tolls,
          surcharge: snap.val().surcharge,
          realPrice: snap.val().realPrice,
          osc: snap.val().osc,
        });
        this.eventList.sort();
        this.eventList.reverse();

        console.log(this.eventList);
        this.total_trips = this.eventList.length
        console.log("total Trips", this.total_trips);

        return false;
      });
    });

    this.eventProvider.getCompleted().orderByChild("name").equalTo(this.profile.name).on("value", (snapshot) => {
      this.completedList = [];

      snapshot.forEach((snap) => {
        this.completedList.push({
          id: snap.key,
          name: snap.val().name,
         
        });
    
        

        console.log(this.completedList);
        this.total_completedList = this.completedList.length
        console.log("total Completed", this.total_completedList);

      });
    });

    // this.presentRouteLoader(this.lp.translate()[0].c4);
    
    this.profile.getUserProfile().on("value", (userProfileSnapshot) => {
      this.earnings = Math.floor(userProfileSnapshot.val().earnings);
      this.hours = userProfileSnapshot.val().hours
      this.minute = userProfileSnapshot.val().minutes
      let secs = userProfileSnapshot.val().seconds

      console.log(this.hours + "h : " + this.minute + "m : " +
            secs + "s" )
    });



    for (let index = 0; index < this.eventList.length; index++) {
      const element = this.eventList[index];
      // console.log(element.price.replace(/[^\d\.]/g, ''));

      // n.push(parseFloat(this.eventList[index].price.replace(/[^\d\.]/g, '')));
      n.push(parseFloat(this.eventList[index].price));
      const add4 = (a, b) => a + b;
      const result4 = n.reduce(add4);
      this.mr = result4.toFixed(2);


      // sum += this.eventList[index].price + element.tip || 0;
    }

    console.log(this.mr);
  }

  // calculateEarningsForTheDay() {
  //   var date_result = "";
  //   var d = new Date();
  //   date_result += d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();
  //   date_result;
  //   console.log("DATE TODAY IS", date_result)

  //   this.eventProvider.getEventListToday(date_result).on("value", (snapshot) => {

  //     console.log("<<!!!--------- SNAPSHOTTT:: ------>>", snapshot)

  //     snapshot.forEach((snap) => {

  //       console.log("THE VALUSE FOR TODAY::", snap.val())
  //     })

  //   })

  // }

  async goBack() {
    this.navCtrl.navigateRoot("home");
  }
  checkMe(price, surcharge) {
    let g = [];
    let b = [];
    let k = [];
    let o = [];
    let c = [];
    let n = [];

    console.log(surcharge);

    surcharge.forEach((element) => {
      this.riderpaid = parseFloat(price).toFixed(2);

      //if driver
      if (element.owner == 0) {
        //if percent
        if (element.bone == 1) {
          let nb = element.price / 100;
          console.log(nb * this.riderpaid);
          let fo = nb * this.riderpaid;
          n.push(fo);
          const add2 = (a, b) => a + b;
          const result2 = n.reduce(add2);
          this.percentDriver = result2;
          console.log((Math.floor(element.price) / 100) * this.riderpaid);
        }
        //if flat fee
        if (element.bone == 0) {
          c.push(parseFloat(element.price));
          const add4 = (a, b) => a + b;
          const result4 = c.reduce(add4);
          this.flatDriver = result4;
          console.log(result4);
        }

        this.totalDriverSurge = this.flatDriver + this.percentDriver;
        console.log(this.totalDriverSurge, this.flatDriver, this.percentDriver);
      }
    });

    return this.totalDriverSurge;
  }

  check(price, surcharge) {
    let g = [];
    let b = [];
    let k = [];
    let o = [];
    let c = [];
    let n = [];

    console.log(surcharge);

    surcharge.forEach((element) => {
      this.wef = parseFloat(price).toFixed(2);

      //if driver
      if (element.owner == 0) {
        //if percent
        if (element.bone == 1) {
          let nb = element.price / 100;
          console.log(nb * this.wef);
          let fo = nb * this.wef;
          n.push(fo);
          const add2 = (a, b) => a + b;
          const result2 = n.reduce(add2);
          this.percent2Driver = result2;
          console.log((Math.floor(element.price) / 100) * this.wef);
        }
        //if flat fee
        if (element.bone == 0) {
          c.push(parseFloat(element.price));
          const add4 = (a, b) => a + b;
          const result4 = c.reduce(add4);
          this.flat2Driver = result4;
          console.log(result4);
        }

        this.myDriverSurge = this.flat2Driver + this.percent2Driver;
        console.log(this.myDriverSurge, this.flatDriver, this.percentDriver);
      }
    });

    return this.myDriverSurge;
  }

  // async presentRouteLoader(message) {
  //   const loading = await this.load.create({
  //     message: message,
  //   });

  //   await loading.present();

  //   let myInterval = setInterval(() => {
  //     if (this.eventList != null) {
  //       loading.dismiss();
  //       clearInterval(myInterval);
  //     }
  //   }, 1000);
  // }

  goto() {
    this.navCtrl.navigateRoot(["wallet"]);
  }

  // Request(eventId){
  //   let alert = this.alert.create({
  //     title: this.lp.translate()[0].c5,
  //     buttons: [ {
  //       text: this.lp.translate()[0].c6,
  //       role: 'cancel',
  //       handler: () => {

  //       }
  //     },
  //     {
  //       text: this.lp.translate()[0].c7,
  //       handler: () => {
  //         this.eventProvider.getEventDetail(eventId).update({
  //           paid: 2,
  //         }).then((suc) =>{
  //           this.pop.alertMe('Your Request Has Been Recieved And Is Biegn Processed. This May Take Up To 30 minutes')
  //         });

  //     }
  //     },],
  //     enableBackdropDismiss: false
  //   });
  //   alert.present();
  // }

  // OpenCancelled() {
  //   this.navCtrl.navigateRoot("cancelled");
  // }

  // async Delete(eventId) {
  //   const alert = await this.alert.create({
  //     message: this.lp.translate()[0].c5,
  //     buttons: [
  //       {
  //         text: this.lp.translate()[0].c6,
  //         role: "cancel",
  //         handler: () => { },
  //       },
  //       {
  //         text: this.lp.translate()[0].c7,
  //         handler: () => {
  //           this.eventProvider
  //             .getEventDetail(eventId)
  //             .update({
  //               hidden: true,
  //             })
  //             .then((success) => { });
  //         },
  //       },
  //     ],
  //     backdropDismiss: false,
  //   });
  //   alert.present();
  // }

  ngOnInit() {
  
    this.todayDate = Date.now()
    this.retriveCampaign()
  }

  retriveCampaign() {

    this.profile.retrieve_campaigns().on("value", (snapshot) => {
      this.campaignss = [];

      snapshot.forEach((snap) => {
        this.campaignss.push({
          id: snap.key,
          description: snap.val().description,
          amount: snap.val().amount,
          hours: snap.val().hours,
          date: snap.val().date,
          no_trips: snap.val().no_trips,

        });
        this.campaignss.sort();
        this.campaignss.reverse();

        console.log(this.campaignss);
        this.total_trips = this.campaignss.length
        console.log("Campaings Trips", this.total_trips);

        return false;
      });
    });
  }
}
