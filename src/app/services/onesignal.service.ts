import { Injectable } from "@angular/core";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { EventService } from "./event.service";
import { Platform } from "@ionic/angular";
import { LanguageService } from "./language.service";
import { ProfileService } from "./profile.service";
import { PopUpService } from "./pop-up.service";


@Injectable()
export class OnesignalService {
  // tslint:disable-next-line: variable-name
  public notif_id: any;
  // tslint:disable-next-line: variable-name
  public isInDeepSh_t = true;
  // tslint:disable-next-line: variable-name
  public isInDeepSh_t2 = true;
  public ID: any;
  phone: any;
  picture: any;
  rating: any;
  carname: any;
  carImage: any;
  carprice: any;
  earnings: any;
  hours: any;
  price: any;
  constructor(
    public One: OneSignal,
    public ph: ProfileService,
    public lp: LanguageService,
    public eprov: EventService,
    public platform: Platform
  ) {

    this.subscribeToOnesignal();
    console.log("checked onesignal");
  }


  subscribeToOnesignal() {
    if (!this.platform.is("cordova")) {
      this.notif_id = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
      console.log("NOTIFY ID IN BROSWER::--" + this.notif_id);
    }
    else if (this.platform.is("desktop")) {
      this.notif_id = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
      console.log("NOTFICA IN desktop BROWSER-->>" + this.notif_id);
    }

    else if (this.platform.is("mobileweb")) {
      this.notif_id = "43cd6829-4651-4039-bbc3-aace7fbe7d72";
      console.log("NOTFICA IN mobileweb BROWSER-->>" + this.notif_id);
    }
    else {
      this.One.getIds().then((success) => {
        this.notif_id = success.userId;
        console.log("NOTIFY ID IN CORDOVA:: --" + this.notif_id);
      });
    }
  }

  ionViewLoaded() {
    this.subscribeToOnesignal();
  }

  sendPush(id) {
    this.One.getIds().then((success) => {
      console.log(success.userId);

      const notificationObj: any = {
        include_player_ids: [id],
        contents: { en: this.lp.translate()[0].f6 },
      };


      this.One.postNotification(notificationObj).then(
        (success) => {
          console.log("Notification Post Success:", success);
        },
        (error) => {
          console.log("ERROR SENDING NOTIFICATIONS " + error);
          //this.pop.presentToast("Push notification failed");
        }
      );
    });
  }
  sendMessage(id, message) {
    this.One.getIds().then((success) => {
      console.log(success.userId);

      const notificationObj: any = {
        include_player_ids: [id],
        contents: { en: message },
      };

      // tslint:disable-next-line: no-shadowed-variable
      this.One.postNotification(notificationObj).then(
        (success) => {
          console.log("Notification Post Success:", success);
        },
        (error) => {
          console.log(error);
          // alert("Notification Post Failed:\n" + JSON.stringify(error));
        }
      );
    });
  }

  UpdateInfo(lat, lng, car) {
    console.log("Push Initial Location Info to DB");
    this.ph.getUserProfile().on("value", (userProfileSnapshot) => {
      console.log("SHOW DETAILS OF USERS/DRIVERS", userProfileSnapshot.val());

      this.phone = userProfileSnapshot.val().phonenumber;
      this.picture = userProfileSnapshot.val().picture;
      this.rating = userProfileSnapshot.val().rating;
      this.carname = userProfileSnapshot.val().carName;
      this.earnings = userProfileSnapshot.val().earnings;
      this.price = userProfileSnapshot.val().price;

      if (this.platform.is("cordova")) {
        console.log(
          "updated In Cordova",
          lat,
          lng,
          this.notif_id,
          this.phone,
          this.picture,
          this.rating,
          this.earnings,
          this.price
        );

        if (lat !== 0) {
          this.eprov.PushCurrentLocation(
            lat,
            lng,
            this.notif_id,
            this.carname,
            this.phone,
            this.picture,
            this.rating,
            this.earnings,
            this.price
          );
        }
        else {
          console.log("couldnt find lattide for Driver")
        }
      } else {
        console.log(
          "updated In Broswer",
          lat,
          lng,
          this.notif_id,
          this.carname,

          this.phone,
          this.picture,
          this.rating,
          this.earnings,
          this.price
        );

        if (lat !== 0) {
          this.eprov.PushCurrentLocation(
            lat,
            lng,
            this.notif_id,
            this.carname,
            this.phone,
            this.picture,
            this.rating,
            this.earnings,
            this.price
          );
        }
        else {
          console.log("couldnt find lattide for Driver")
        }
      }
      this.ph.getUserProfile().off("value");
    });
  }

  UpdateCurrentLocation(lat, lng) {
    if (this.isInDeepSh_t) {
      this.One.getIds().then((success) => {
        this.notif_id = success.userId;
        if (lat !== 0) {
          this.eprov.UpdateLocation(lat, lng, success.userId);
        }
      });
    }
  }
}
