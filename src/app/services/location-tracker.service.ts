import { Injectable, NgZone } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Platform, AlertController, ToastController } from "@ionic/angular";
import { timer } from "rxjs";

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  LatLngBounds,
  LatLng,
  Marker
} from "@ionic-native/google-maps/ngx";
import { LanguageService } from "./language.service";
import { OnesignalService } from "./onesignal.service";
import { EventService } from "./event.service";
import { ProfileService } from "./profile.service";
import { GeocoderService } from "./geocoder.service";
import { interval } from "rxjs";
import { Geolocation, PositionError, Geoposition } from "@ionic-native/geolocation/ngx";

declare var google: any;
declare var plugin: any;

@Injectable()
export class LocationTrackerService {
  public onLocationbarHide = true;
  public onDestinatiobarHide = false;
  public driver_lat: any = 0;
  public driver_lng: any = 0;
  public poly: any;
  // tslint:disable-next-line: variable-name
  public tracking_timeout: any;
  public hasTransactionEnded = true;
  // tslint:disable-next-line: variable-name
  public tracking_timeout2: any;
  public hasTransactionEnded2 = true;
  public startPos: any;
  public client: any;
  public driver: any;
  public speed = 50; // km/h
  // public marker: any;
  marker: Marker;
  public cars: any = [];
  // tslint:disable-next-line: variable-name
  public car_location: any = [];
  // tslint:disable-next-line: variable-name
  public car_notificationIds: any = [];
  public delay = 100;
  public hasRequested = false;
  public isCarAvailable = false;
  public uid: any;
  public onGpsEnabled = false;
  isNavigate = false;
  locations: any;
  location: any;
  public ClearDetection = false;
  timer1: any;
  map1: GoogleMap;
  public onbar = false;
  public onbar1 = false;
  public onbar2 = false;
  public onbar3 = false;
  public toggleBtn = false;
  public onPointerHide = false;
  mapElement: HTMLElement;
  mapLoadComplete = false;
  public pan = 0;
  public detectCarChange: any;
  public hasDone = false;
  public hasStart = false;
  public hasShown = false;
  // tslint:disable-next-line: variable-name
  public D_lat: any;
  // tslint:disable-next-line: variable-name
  public D_lng: any;
  // tslint:disable-next-line: variable-name
  public canTrack_ = false;
  // tslint:disable-next-line: variable-name
  public canTrack_2 = false;
  public isDestination = false;
  public userLat: any;
  public userLng: any;
  notifID: any;
  watchPositionSubscription: Geolocation;
  // tslint:disable-next-line: variable-name
  goto_passenger_watcher: number;
  hasCompleted = false;
  Line: any;
  isComingToUser: any;
  flag: any;
  // tslint:disable-next-line: variable-name
  goto_destination_watcher: number;
  // tslint:disable-next-line: variable-name
  hasCompleted_Destination: any;
  destination: any;
  // tslint:disable-next-line: variable-name
  rider_lat: any;
  // tslint:disable-next-line: variable-name
  rider_lng: any;
  watchPositionSubscription2: Geolocation;
  isDropped = false;
  isPickedUp = true;
  route: any = [];
  directionsService: any = new google.maps.DirectionsService();
  Line2: any;
  myLat: number;
  myLng: number;
  routeNumber: any;
  location_tracker_loop: any;
  destination_tracker_loop: any;
  myVal: any;
  mapTracker: any;
  watch;
  circle;

  yellow_markersArray = [];
  driver_markersArray = [];
  client_markersArray = [];
  flag_markersArray = [];
  constructor(
    public lp: LanguageService,
    public alert: AlertController,
    public oneS: OnesignalService,
    public ev: EventService,
    public zone: NgZone,
    public myProf: ProfileService,
    public gcode: GeocoderService,
    public platform: Platform,
    private toastCtrl: ToastController,
    private geo: Geolocation
  ) { }




  loadMap() {
    console.log("map called");
    const mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 5.614818,
          lng: -0.205874,
        },
        zoom: 19,
        tilt: 0,
      },
    };



    this.map1 = GoogleMaps.create("map-canvas", mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map1.one(GoogleMapsEvent.MAP_READY).then(
      () => {
        console.log("Map is ready!");
        this.map1.setMyLocationEnabled(true)
        this.map1.setAllGesturesEnabled(true)
        this.map1.setClickable(true)
        this.map1.setCompassEnabled(true)

        this.map1.getMyLocation().then(location => {
          console.log(location.latLng);

          this.AnimateToLoc(location.latLng.lat, location.latLng.lng);
          this.location = location;

          this.driver_lat = location.latLng.lat,
            this.driver_lng = location.latLng.lng;


        }, (error) => {
          //this.presentToast("Finding it difficult to get your current location. Please wait..");
          this.loadMap();

        })



        // this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
        //   if ((position as Geoposition).coords != undefined) {
        //     var geoposition = (position as Geoposition);
        //     console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);

        //     console.log("Location is ready!--location ");
        //     this.location = geoposition;

        //     this.driver_lat = geoposition.coords.latitude,
        //       this.driver_lng = geoposition.coords.longitude;


        //     this.AnimateToLoc(this.driver_lat, this.driver_lng);



        //   } else {
        //     var positionError = (position as PositionError);
        //     console.log('Error ' + positionError.code + ': ' + positionError.message);

        //     this.presentToast("Finding it difficult to get your current location. Please wait..");
        //     this.loadMap();
        //   }
        // });




      })
  }

  async presentToast(message) {
    const toast = await this.toastCtrl.create({
      message,
      duration: 3000,
      position: "bottom",
    });

    toast.onDidDismiss();

    toast.present();
  }

  ResetMe() {
    console.log("RESETING MAP...")
    this.map1.clear();
    this.AnimateToLoc(this.driver_lat, this.driver_lng);
  }

  refreshForChangesRemove() {
    console.log("RESETING MAP...")
    this.isDropped = false;
    this.hasCompleted = true;

    this.watch.unsubscribe();
    this.map1.clear().then(() => {
      console.log("RESETING MAP...")
      this.map1.getMyLocation().then(location => {
        console.log(location.latLng);

        this.AnimateToLoc(location.latLng.lat, location.latLng.lng);
        this.location = location;


      }, (error) => {
      //  this.presentToast("Finding it difficult to get your current location. Please wait..");
        this.loadMap();

      })


      // this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
      //   if ((position as Geoposition).coords != undefined) {
      //     var geoposition = (position as Geoposition);
      //     console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);

      //     console.log("Map is ready Resetiin!--location ");
      //     this.location = geoposition;

      //     this.driver_lat = geoposition.coords.latitude,
      //       this.driver_lng = geoposition.coords.longitude;


      //     this.AnimateToLoc(this.driver_lat, this.driver_lng);



      //   } else {
      //     var positionError = (position as PositionError);
      //     console.log('Error ' + positionError.code + ': ' + positionError.message);

      //     this.presentToast("Finding it difficult to get your current location. Please wait..");
      //     this.loadMap();
      //   }
      // });

    }, (error) => {
      console.log(error);
      console.log("ERROR CLERIN MAP")
    });
  }

  async showError(title) {
    const alert = await this.alert.create({
      header: title,
      buttons: [
        {
          text: "Okay",
          role: "cancel",
          handler: () => {
            this.refreshForChangesRemove();
          },
        },
      ],
      backdropDismiss: false,
    });
    alert.present();
  }

  /// Animate to user location
  AnimateToLoc(lat, lng) {

    this.isPickedUp = true;
    console.log("ani to loc");

    this.map1
      .animateCamera({
        target: {
          lat,
          lng,
        },
        zoom: 19,
        tilt: 0,
        bearing: 0,
        duration: 500,
      })
      .then((suc) => {
        console.log("camera done");
        this.hasShown = true;
        this.map1.setClickable(true);



        const image_icon = {
          url: "assets/icon/pin.png",

          size: new google.maps.Size(40, 40),

        };

        console.log("YELLOW MARKERS ARRAY LENTHG", this.yellow_markersArray.length);

        //CHeck if yellow marker exist
        if (this.yellow_markersArray.length >= 1) {
          console.log("----MARKER EXIST SO MOVE MARKER----", this.marker);
          // this.circle = null;
         // this.circle.remove();
          // this.marker.setPosition({
          //   lat: lat,
          //   lng: lng
          // });
          this.mapLoadComplete = true;

        }
        else if (this.yellow_markersArray.length < 1) {
          console.log("----CREATING NEW MARKER----");
          this.map1
            .addMarker({
              title: "pinIcon",

              icon: image_icon,
              position: {
                lat,
                lng,
              },
            })
            .then((marker) => {
              this.mapLoadComplete = true;
              this.marker = marker;
              console.log("marker added", marker);
              this.yellow_markersArray.push(this.marker);
              console.log("Yellow MArker Array", this.yellow_markersArray);


              // this.map1
              //   .addCircle({
              //     center: {
              //       lat,
              //       lng,
              //     },
              //     radius: 50,
              //     strokeColor: "#fbb91d",
              //     strokeWidth: 2,
              //     visible: true,
              //     fillColor: "#737373",
              //     fillOpacity: 0.2,
              //   })
              //   .then((circle) => {
              //     this.circle = circle;
              //     setTimeout(() => {
              //       circle.setRadius(50);
              //     }, 500);
              //   });

            });
        }
      });
    // });
  }

  //   fadeCityCircles() {
  //  cityCircle =  this.map1.addCircle({
  //     center: {
  //       lat,
  //       lng,
  //     },
  //     radius: 500,
  //     strokeColor: "#0000",
  //     strokeWidth: 2,
  //     visible: true,
  //     fillColor: "#fbb91d",
  //     fillOpacity: 0.6,
  //   });
  //     var fillOpacity = citymap[city].cityCircle.get("fillOpacity");
  //     fillOpacity -= 0.02;
  //     if (fillOpacity < 0) fillOpacity =0.0;
  //     var strokeOpacity = citymap[city].cityCircle.get("strokeOpacity");
  //     strokeOpacity -= 0.05;
  //     if (strokeOpacity < 0) strokeOpacity =0.0;
  //     citymap[city].cityCircle.setOptions({fillOpacity:fillOpacity, strokeOpacity:strokeOpacity});

  // }

  async showAlertNormal() {
    const alert = await this.alert.create({
      header: "GPS Related Error",
      subHeader: "Your GPS location seem to be unavailable",
      buttons: [
        {
          text: "Make sure your GPS location is turned on",
          role: "cancel",
          handler: () => {
            this.refreshForChangesRemove();
          },
        },
      ],
      backdropDismiss: false,
    });
    alert.present();
  }

  // Show distance between driver and User in the map
  setMarkers(uid, lat, lng) {

    if (this.watch) {

      this.watch.unsubscribe();
    }

    this.rider_lat = lat;
    this.rider_lng = lng;

    this.destination = new LatLng(lat, lng);
    let location = new LatLng(lat, lng);
    console.log("RIDER LAT AND LNG", location);



    this.DrawPoly(uid);

    this.myProf.getUserAsClientInfo(uid).on("child_changed", (driverSnap) => {
      console.log(driverSnap.val());

      if (this.isPickedUp) {
        this.myProf.getUserAsClientInfo(uid).once("value", (dggd) => {
          console.log(driverSnap.val());
          if (driverSnap.val()) {
            console.log("There is now a value");
            this.rider_lat = driverSnap.val().Client_location[0];
            this.rider_lng = driverSnap.val().Client_location[1];

            let location = new LatLng(this.rider_lat, this.rider_lng);
            console.log("RIDER LAT AND LNG", location);

            //animate camera to driver
            this.map1
              .moveCamera({
                target: location,
                zoom: 19,
                tilt: 0,
                bearing: 0,
              })
          }
          this.destination = new LatLng(
            driverSnap.val().Client_location[0],
            driverSnap.val().Client_location[1]
          );
          this.myProf.getUserAsClientInfo(uid).off("value");
        });
      }
    });
  }

  MoveDriver(lat, lng) {
    console.log("ADD DRIVER CAR MARKERR----")

    // if (this.driver_markersArray.length >= 1) {
    //   this.driver.setPosition({
    //     lat: lat,
    //     lng: lng
    //   });
    // }
    if (this.driver) {
      this.driver.remove();
    }

    // else if (this.driver_markersArray.length < 1) {

    const myIcon = {
      url: "assets/icon/map-taxi.png",
      size: new google.maps.Size(40, 40),

    };

    this.map1
      .addMarker({
        title: "",
        icon: myIcon,
        position: {
          lat,
          lng,
        },
      })
      .then((marker) => {
        this.driver = marker;
        this.driver_markersArray.push(this.driver);
        console.log("this.driver MARKER::", this.driver);
      });
    // }
  }

  MoveClient(lat, lng) {
    // if (this.client_markersArray.length >= 1) {
    //   this.client.setPosition({
    //     lat: lat,
    //     lng: lng
    //   });
    // }
    // else if (this.client_markersArray.length < 1) {
    if (this.client) {
      this.client.remove();
    }

    const image_icon2 = {
      url: "assets/icon/rider.png",
      size: new google.maps.Size(40, 40),

    };

    this.map1
      .addMarker({
        title: "",

        icon: image_icon2,
        position: {
          lat,
          lng,
        },
      })
      .then((markdfder) => {
        this.client = markdfder
        this.client_markersArray.push(this.client);
        console.log(this.client);
      });
    // }
  }

  MoveFlag(lat, lng) {
    // if (this.flag_markersArray.length >= 1) {
    //   this.flag.setPosition({
    //     lat: lat,
    //     lng: lng
    //   });
    // }
    // else if (this.flag_markersArray.length < 1) {

    if (this.flag) {
      this.flag.remove();
    }

    const image_icon3 = {
      url: "assets/icon/flag2.png",
      size: new google.maps.Size(40, 40),

    };

    this.map1
      .addMarker({
        title: "",

        icon: image_icon3,
        position: {
          lat,
          lng,
        },
      })
      .then((markdfder) => {
        this.flag = markdfder;
        this.flag_markersArray.push(this.flag);
        console.log(this.flag);
      });
    // }
  }

  // Show distance between driver and User in the map
  setMarkersDestination(lat, lng, uid) {

    console.log(lat, lng);
    this.destination = new LatLng(lat, lng);
    this.isPickedUp = false;
    this.isDropped = true;
    this.rider_lat = lat;
    this.rider_lng = lng;

    this.watch.unsubscribe();

    this.DrawPoly(uid);

  }

  DrawPoly(uid) {
    console.log("DrawPoly Called.............");

    this.watch = this.geo.watchPosition({ enableHighAccuracy: true, timeout: 10000 }).subscribe(position => {
      if ((position as Geoposition).coords != undefined) {
        var geoposition = (position as Geoposition);
        console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude);


        this.location = geoposition;

        this.driver_lat = geoposition.coords.latitude,
          this.driver_lng = geoposition.coords.longitude;

        console.log("DRIVER position callback", geoposition);
        console.log("DESTINATION", this.destination);

        this.route = [];
        this.directionsService.route(
          {
            origin: new LatLng(
              geoposition.coords.latitude,
              geoposition.coords.longitude
            ),
            destination: this.destination,
            travelMode: google.maps.TravelMode.DRIVING,
            provideRouteAlternatives: true,
          },
          (res, status) => {
            console.log("DIRECTION STATUS", status);
            if (status === google.maps.DirectionsStatus.OK) {
              // Loop through each direction route point and add it to be plotted
              for (
                let i = 0, len = res.routes[0].overview_path.length;
                i < len;
                i++
              ) {
                const point = res.routes[0].overview_path[i];

                this.route[i] = {
                  lat: Number(point.lat()),
                  lng: Number(point.lng()),
                };
              }

              if (!this.hasCompleted) {
                if (this.rider_lat) {


                  console.log("IS PICKUPED" + this.isPickedUp);
                  if (this.isPickedUp) {
                    this.AddPolyLine();

                    this.MoveClient(
                      this.rider_lat,
                      this.rider_lng,
                    );

                    this.MoveDriver(this.driver_lat, this.driver_lng);


                  }

                  if (!this.isPickedUp) {

                    this.MoveDriver(this.driver_lat, this.driver_lng);
                  }
                  //MOVE DRIVER ONGOING AND UPDATE LOCATION

                  console.log("driver location updated in the DB");
                  this.myProf.UpdateDriverLocation(
                    this.driver_lat,
                    this.driver_lng,
                    uid
                  );


                  this.Measure(
                    this.rider_lat,
                    this.rider_lng,
                    this.driver_lat,
                    this.driver_lng,
                  );
                  if (this.isDropped) {
                    this.myLat = this.driver_lat;
                    this.myLng = this.driver_lng;

                    this.AddPolyLine2();
                    console.log("now moving the driver ");
                    this.MoveFlag(this.rider_lat, this.rider_lng);
                  }
                }
              } else {
                if (this.Line) {
                  this.Line.remove();
                }

                if (this.Line2) {
                  this.Line2.remove();
                }
                if (this.driver) {
                  this.driver.remove();
                }

                if (this.client) {
                  this.client.remove();
                }
                this.route = [];

                this.watch.unsubscribe();


                console.log("TRIP HAS COMPLETED");



              }
            } else {
              console.log("google.maps.DirectionsStatus.ERROR", status);
              if (this.Line) {
                this.Line.remove();
              }

              if (this.Line2) {
                this.Line2.remove();
              }
              if (this.driver) {
                this.driver.remove();
              }

              if (this.client) {
                this.client.remove();
              }
              this.route = [];

              this.watch.unsubscribe();
            }
          }
        );

      } else {
        var positionError = (position as PositionError);
        console.log('Error ' + positionError.code + ': ' + positionError.message);


        this.presentToast("Finding it difficult to get your current location. Please wait..");

      }
    });


  }
  AddPolyLine() {
    console.log("ADDING POLYLINE")

    if (this.Line) {
      this.Line.remove();
    }

    this.map1
      .addPolyline({
        color: "#fbb91d",
        visible: true,
        width: 7,
        points: this.route,
      })
      .then((ff) => {
        this.Line = ff;
        console.log("LINE ADDED", this.Line)
      });
  }

  AddPolyLine2() {

    if (this.Line) {
      this.Line.remove();
    }

    if (this.Line2) {
      this.Line2.remove();
    }

    this.map1
      .addPolyline({
        color: "#fbb91d",
        visible: true,
        width: 7,
        points: this.route,
      })
      .then((ff) => {
        this.Line2 = ff;
      });
  }

  Measure(lat, lng, lat2, lng2) {
    // this.myVal.unsubscribe()
    console.log("MEASURED");
    const arrayOfLatLng = [new LatLng(lat, lng), new LatLng(lat2, lng2)];

    const bounds = new LatLngBounds(arrayOfLatLng);

    console.log("Calling Bounds-- " + bounds);
    const center = bounds.getCenter();
    console.log("Calling GETcenter-- " + center);

    // const mapElement = document.getElementById("map");
    const mapElement = document.getElementById("map-canvas");
    console.log("MAP ELEMENTS:::: " + mapElement);
    const mapDimensions = {
      height: mapElement.offsetHeight,
      width: mapElement.offsetWidth,
    };

    const zoom = this.getBoundsZoomLevel(bounds, mapDimensions);

    // this.map1
    //   .animateCamera({
    //     target: center,
    //     zoom,
    //     duration: 500,
    //   })
    //   .then(() => {
    //     this.isNavigate = true;
    //   });
  }

  getBoundsZoomLevel(bounds, mapDim) {
    const WORLD_DIM = { height: 96 / 2.05, width: 96 / 2.05 };
    const ZOOM_MAX = 15;

    const ne = bounds.northeast;
    const sw = bounds.southwest;

    const latFraction = (this.latRad(ne.lat) - this.latRad(sw.lat)) / Math.PI;

    const lngDiff = ne.lng - sw.lng;
    const lngFraction =
      ((lngDiff < 0 ? lngDiff + 360 * 1.16 : lngDiff) / 360) * 1.16;

    const latZoom = this.zoom(mapDim.height, WORLD_DIM.height, latFraction);
    const lngZoom = this.zoom(mapDim.width, WORLD_DIM.width, lngFraction);

    return Math.min(latZoom, lngZoom, ZOOM_MAX);
  }

  latRad(lat) {
    const sin = Math.sin((lat * Math.PI) / 180);
    const radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
    return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
  }

  zoom(mapPx, worldPx, fraction) {
    return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
  }
}
