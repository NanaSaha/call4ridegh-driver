import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouteReuseStrategy } from "@angular/router";
import { HttpClientModule } from "@angular/common/http";
import { IonicStorageModule } from "@ionic/storage";
import { IonicRatingModule } from "ionic4-rating";
import * as firebase from "firebase";
import { Firebase } from "@ionic-native/firebase/ngx";
import { FilePath } from "@ionic-native/file-path/ngx";
import { FileUploadModule } from "ng2-file-upload";
import { MediaCapture } from "@ionic-native/media-capture/ngx";
import { Base64 } from "@ionic-native/base64/ngx";


import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule, IonicRouteStrategy } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { Camera, CameraOptions } from "@ionic-native/camera/ngx";
import { BackgroundMode } from "@ionic-native/background-mode/ngx";
import { OneSignal } from "@ionic-native/onesignal/ngx";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { CallNumber } from "@ionic-native/call-number/ngx";
import { HTTP } from "@ionic-native/http/ngx";
import { File } from "@ionic-native/file/ngx";
import { FileChooser } from "@ionic-native/file-chooser/ngx";
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { Vibration } from "@ionic-native/vibration/ngx";
import { GoogleMaps } from "@ionic-native/google-maps";
import { LocationTrackerService } from "./services/location-tracker.service";
import { OnesignalService } from "./services/onesignal.service";
import { PopUpService } from "./services/pop-up.service";

import { ProfileService } from "./services/profile.service";
import { AcceptPage } from "./pages/accept/accept.page";
import { AcceptPageModule } from "./pages/accept/accept.module";
import { RatePage } from "./pages/rate/rate.page";
import { RatePageModule } from "./pages/rate/rate.module";
import { NewsPage } from "./pages/news/news.page";
import { NewsPageModule } from "./pages/news/news.module";
import { ChatPage } from "./pages/chat/chat.page";
import { ChatPageModule } from "./pages/chat/chat.module";
import { TripinfoPage } from "./pages/tripinfo/tripinfo.page";
import { TripinfoPageModule } from "./pages/tripinfo/tripinfo.module";
import { PaymentpagePage } from "./pages/paymentpage/paymentpage.page";
import { PaymentpagePageModule } from "./pages/paymentpage/paymentpage.module";
import { NgOtpInputModule } from "ng-otp-input";
import { Angular4PaystackModule } from "angular4-paystack";
import { IonicSelectableModule } from "ionic-selectable";
import { Geolocation } from "@ionic-native/geolocation/ngx";


// export const firebaseConfig = {
//   apiKey: "AIzaSyCmo-vLV-qO0TaIQaslBwJ2BHWy5JPBVlg",
//   authDomain: "call4ride-851d9.firebaseapp.com",
//   databaseURL: "https://call4ride-851d9-default-rtdb.firebaseio.com",
//   projectId: "call4ride-851d9",
//   storageBucket: "call4ride-851d9.appspot.com",
//   messagingSenderId: "252622798292",
//   appId: "1:252622798292:web:03eccba2572a82f4b26a41",
//   measurementId: "G-L5YTTJ0Z43",
// };



export const firebaseConfig = {
  // apiKey: "AIzaSyApjpwWIXFsAn6WWgauNG93bHdAY126eVw",
  // authDomain: "ghana-c4r.firebaseapp.com",
  // projectId: "ghana-c4r",
  // storageBucket: "ghana-c4r.appspot.com",
  // databaseURL: "https://ghana-c4r-default-rtdb.firebaseio.com",
  // messagingSenderId: "516821551729",
  // appId: "1:516821551729:web:04f1a30ca01b8acc9d29d0",
  // measurementId: "G-2Y04YP8P7Q"

  apiKey: "AIzaSyDtQGdYGuIQ7f-r9JqWBBj7q6RJJ9595nI",
  authDomain: "call4ride-2fe35.firebaseapp.com",
  projectId: "call4ride-2fe35",
  storageBucket: "call4ride-2fe35.appspot.com",
  databaseURL: "https://call4ride-2fe35-default-rtdb.firebaseio.com",
  messagingSenderId: "1092796986980",
  appId: "1:1092796986980:web:24d071186167016803d171",
  measurementId: "G-EPD48CQC9J"
};

firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [AppComponent],

  entryComponents: [
    // AcceptPage,
    // RatePage,
    // NewsPage,
    // ChatPage,
    // TripinfoPage,
    // PaymentpagePage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicRatingModule,
    IonicStorageModule.forRoot(),
    FileUploadModule,
    FormsModule,
    ReactiveFormsModule,
    // AcceptPageModule,
    // RatePageModule,
    // NewsPageModule,
    // ChatPageModule,
    // TripinfoPageModule,
    // PaymentpagePageModule,
    NgOtpInputModule,
    Angular4PaystackModule.forRoot("pk_test_xxxxxxxxxxxxxxxxxxxxxxxx"),
    IonicSelectableModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Camera,
    BackgroundMode,
    OneSignal,
    InAppBrowser,
    CallNumber,
    HTTP,
    File,
    FileChooser,
    GoogleMaps,
    Firebase,
    FilePath,
    SocialSharing,
    Vibration,
    LocationTrackerService,
    OnesignalService,
    PopUpService,
    Geolocation,

    ProfileService,
    MediaCapture,
    Base64,
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
